#include "qtquickcontrolsapplication.h"
#include "documenthandler.h"
#include <QtQml/QQmlApplicationEngine>
#include <QWindow>

int main(int argc, char *argv[])
{
    QtQuickControlsApplication app(argc, argv);
    qmlRegisterType<DocumentHandler>("org.qtproject.example", 1, 0, "DocumentHandler");
    QQmlApplicationEngine engine(QUrl("qrc:/qml/main.qml"));
    if (engine.rootObjects().isEmpty())
        return -1;

    //freopen( "clientLogs.txt", "w", stderr );
    app.setWindowIcon(QIcon("qrc:qml/images/myIco.ico"));

    return app.exec();
}
