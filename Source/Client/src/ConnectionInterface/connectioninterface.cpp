#include "connectioninterface.h"
#include "../Controller/controller.h"
#include "../QT_Library/MessageBox/messageBox.h"
#include <unistd.h>

#define mSECONDS 1000

static EditMessage tmpEdit(-1);

void ConnectionInterface::trueSend() {
    if(tmpEdit.clientId == -1) return;


    QJsonDocument json = tmpEdit.toJsonMessage();
    QByteArray finalMessage = buildFinalMessage(json);
    //Send of message transformed in Json representation
    writeSocket(finalMessage);

    tmpEdit.document = "";
    tmpEdit.clientsCursors = {};
    tmpEdit.externalActions = {};
    tmpEdit.clientId = -1;
}

ConnectionInterface::ConnectionInterface(QObject *parent, Controller *controller) :
    QObject(parent),
    controller(controller){


    QString path = QStandardPaths::standardLocations(QStandardPaths::AppDataLocation)[0];
    path.append("/config.txt");

    std::ifstream config;
    config.open(path.toStdString());

    if(!config.is_open()) {
        serverAddr = QHostAddress("127.0.0.1");
        port = 4444;
    } else {
        std::string tmp;
        config >> tmp;
        serverAddr = QHostAddress(QString::fromStdString(tmp));
        config >> port;
        config.close();
    }

    socket = new QTcpSocket(nullptr);
    connectToServer();
    connect(socket, SIGNAL(disconnected()), this, SLOT(disconnected()));
}

void ConnectionInterface::disconnected(){
    controller->manageSocketError();
}

void ConnectionInterface::connectToServer(){

    while(true){
        qDebug() << "Trying connection on " << serverAddr << ":" << port;
        socket->connectToHost(serverAddr, port, QIODevice::ReadWrite);
        if(socket->waitForConnected(4000)){
            connect(socket, SIGNAL(readyRead()), this, SLOT(receiveMessage()));
            connect(this, SIGNAL(readyRead()), this, SLOT(receiveMessage()));
            connect(&timer, SIGNAL(timeout()), this, SLOT(trueSend()));

            timer.setSingleShot(true);
            timer.setInterval(mSECONDS);

            break;
        } else{
            CMessageBox msgBox;
            msgBox.exec();
            QMessageBox::StandardButton ret = msgBox.getRet();
            switch (ret) {
            case QMessageBox::Retry:
                break;
            case QMessageBox::No:
                exit(1);

            default:
                // should never be reached
                break;
            }
        }
    }
}

/*
 * Method to read data from socket when they are available.
 *  receive() is connected to readyRead() signal, whenever
 *  a data is available it is read and then converted
 *  in QJsonObject from Json representation
 */
void ConnectionInterface::receiveMessage(){
    static int bytesToRead = 0;
    static QByteArray data  =  {};

    if(socket->bytesAvailable() < 8) {
        emit readyRead();
        return;
    }
    if(data.isEmpty()){//nuova lettura
        bytesToRead = socket->read(8).toInt();
        data = socket->read(bytesToRead);
        bytesToRead -= data.size();
        if(bytesToRead > 0){
            return;
        }
    }else{ //lettura sucessiva
        QByteArray buff = socket->read(bytesToRead);
        bytesToRead -= buff.size();
        data.append(buff);
        if(bytesToRead > 0){
            return;
        }
    }

    QJsonDocument json = QJsonDocument::fromJson(data);
    if(json.isObject()){
        QJsonObject obj = json.object();

        if(obj.find("messageType").value().toInt() == T_ManageFileMessage){
            ManageFileMessage manageFileMessage(obj);
            controller->manageServerMessage(manageFileMessage);
        }
        else if(obj.find("messageType").value().toInt() == T_ListDocumentMessage){
            ListDocumentMessage listDocumentMessage(obj);
            controller->manageServerMessage(listDocumentMessage);
        }
        else if(obj.find("messageType").value().toInt() == T_ErrorMessage){
            ErrorMessage errorMessage(obj);
            controller->manageServerMessage(errorMessage);
        }
        else if(obj.find("messageType").value().toInt() == T_ActiveClientMessage){
            ActiveClientMessage msg(obj);
            controller->manageServerMessage(msg);
        }
        else if(obj.find("messageType").value().toInt() == T_AccountMessage){
            AccountMessage accountMessage(obj);
            controller->manageServerMessage(accountMessage);
        }
        else if(obj.find("messageType").value().toInt() == T_EditMessage){
            EditMessage editMessage(obj);
            controller->manageServerMessage(editMessage);
        }
        else if(obj.find("messageType").value().toInt() == T_InfoMessage){
            InfoMessage infoMessage(obj);
            controller->manageServerMessage(infoMessage);
        }
    }

    data = {};
    bytesToRead = 0;

    if(socket->bytesAvailable()>0){
        emit readyRead();
    }
}

void ConnectionInterface::sendAccountMessage(AccountMessage msg){
    this->trueSend(); //preserve the order of unsent message;
    QJsonDocument json = msg.toJsonMessage();
    QByteArray finalMessage = buildFinalMessage(json);
    //Send of message transformed in Json representation
    writeSocket(finalMessage);
}

void ConnectionInterface::sendListDocumentMessage(ListDocumentMessage& msg) {
    this->trueSend(); //preserve the order of unsent message;
    QJsonDocument json = msg.toJsonMessage();
    QByteArray finalMessage = buildFinalMessage(json);
    //Send of message transformed in Json representation
    writeSocket(finalMessage);
}

void ConnectionInterface::sendManageFileMessage(ManageFileMessage& msg) {
    this->trueSend(); //preserve the order of unsent message;
    QJsonDocument json = msg.toJsonMessage();
    QByteArray finalMessage = buildFinalMessage(json);
    //Send of message transformed in Json representation
    writeSocket(finalMessage);
}

void ConnectionInterface::sendErrorMessage(ErrorMessage& msg) {
    this->trueSend(); //preserve the order of unsent message;
    QJsonDocument json = msg.toJsonMessage();
    QByteArray finalMessage = buildFinalMessage(json);
    //Send of message transformed in Json representation
    writeSocket(finalMessage);
}

void ConnectionInterface::sendInfoMessage(InfoMessage& msg){
    this->trueSend(); //preserve the order of unsent message;
    QJsonDocument json = msg.toJsonMessage();
    QByteArray finalMessage = buildFinalMessage(json);
    //Send of message transformed in Json representation
    writeSocket(finalMessage);
}

void ConnectionInterface::sendEditMessage(EditMessage& msg) {
    //Update the message to send each timer's timeout signal
    tmpEdit.clientsCursors = msg.clientsCursors;
    tmpEdit.externalActions.insert(tmpEdit.externalActions.end(), msg.externalActions.begin(), msg.externalActions.end());
    tmpEdit.document = msg.document;
    tmpEdit.clientId = msg.clientId;
    if(!timer.isActive()){
        timer.start();
    }
}

void ConnectionInterface::writeSocket(QByteArray& finalMessage){
    int bytesToSend = finalMessage.size();
    while(bytesToSend > 0){
        qint64 sent = socket->write(finalMessage);
        if(sent == -1)
            controller->manageSocketError();
        bytesToSend -= sent;
        finalMessage = finalMessage.remove(0, static_cast<int>(sent));
    }
}

QByteArray ConnectionInterface::buildFinalMessage(QJsonDocument json){ //It builds the message to be sent through socket : [ [dimension of the message] + [Json message] ]
    QByteArray messageDim;
    QByteArray zeros;
    int dimMax = 8;

    messageDim.setNum(json.toJson().size());
    for(int i=0;i<dimMax - messageDim.size();i++)
        zeros.push_back("0");
    zeros.append(messageDim);
    return zeros.append(json.toJson());
}

void ConnectionInterface::closeConnection(){
    socket->close();
    socket->deleteLater();
}

