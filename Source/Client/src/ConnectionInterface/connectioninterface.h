#ifndef CONNECTIONINTERFACE_H
#define CONNECTIONINTERFACE_H

#include "../Controller/controller.h"

#include <QApplication>
#include <QObject>
#include <QTcpSocket>
#include <QHostAddress>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <queue>
#include <fstream>

class ConnectionInterface : public QObject
{
    Q_OBJECT
public:
    ConnectionInterface(QObject *parent, Controller *controller);

    void sendListDocumentMessage(ListDocumentMessage&);
    void sendManageFileMessage(ManageFileMessage&);
    void sendErrorMessage(ErrorMessage&);
    void sendAccountMessage(AccountMessage);
    void sendInfoMessage(InfoMessage&);
    void sendEditMessage(EditMessage&);

private:
    QTcpSocket *socket;
    Controller *controller;
    QHostAddress serverAddr;
    quint16 port;
    QByteArray buildFinalMessage(QJsonDocument jsonMessage);
    void closeConnection();
    void writeSocket(QByteArray&);
    QTimer timer;



public slots:
    void disconnected();
    void receiveMessage();
    void trueSend();
    void connectToServer();

signals:
    void sendSignal();
    void readyRead();

};

#endif // CONNECTIONINTERFACE_H
