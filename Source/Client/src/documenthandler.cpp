#include "documenthandler.h"

DocumentHandler::DocumentHandler()
    : m_target(nullptr)
    , m_doc(nullptr)
    , m_cursorPosition(-1)
    , m_selectionStart(0)
    , m_selectionEnd(0),
      c(this){
}

void DocumentHandler::setTarget(QQuickItem *target)
{
    m_doc = nullptr;
    m_target = target;
    if (!m_target)
        return;

    QVariant doc = m_target->property("textDocument");
    if (doc.canConvert<QQuickTextDocument*>()) {
        QQuickTextDocument *qqdoc = doc.value<QQuickTextDocument*>();
        if (qqdoc)
            m_doc = qqdoc->textDocument();
    }

    emit targetChanged();
}

void DocumentHandler::setFileUrl(const QUrl &arg)
{
    if (m_fileUrl != arg) {
        m_fileUrl = arg;
        QString fileName = QQmlFile::urlToLocalFileOrQrc(arg);
        if (QFile::exists(fileName)) {
            QFile file(fileName);
            if (file.open(QFile::ReadOnly)) {
                QByteArray data = file.readAll();
                QTextCodec *codec = QTextCodec::codecForHtml(data);
                setText(codec->toUnicode(data));
                if (m_doc)
                    m_doc->setModified(false);
                if (fileName.isEmpty())
                    m_documentTitle = QStringLiteral("untitled.txt");
                else
                    m_documentTitle = QFileInfo(fileName).fileName();

                emit textChanged();
                emit documentTitleChanged();

                reset();
            }
        }
        emit fileUrlChanged();
    }
}

QString DocumentHandler::documentTitle() const
{
    return m_documentTitle;
}


void DocumentHandler::setDocumentTitle(QString arg)
{
    if (m_documentTitle != arg) {
        m_documentTitle = arg;
        emit documentTitleChanged();
    }
}

QString DocumentHandler::state() const{
    return m_state;

}

void DocumentHandler::setState(QString newState){
    m_state = newState;
    emit stateChanged();
}

void DocumentHandler::setText(const QString &arg)
{
    if (m_text != arg) {
        m_text = arg;
        emit textChanged();
    }
}

void DocumentHandler::saveAs(const QUrl &arg, const QString &fileType)
{
    bool isHtml = fileType.contains(QLatin1String("htm"));
    QLatin1String ext(isHtml ? ".html" : ".txt");
    QString localPath = arg.toLocalFile();
    if (!localPath.endsWith(ext))
        localPath += ext;
    QFile f(localPath);
    if (!f.open(QFile::WriteOnly | QFile::Truncate | (isHtml ? QFile::NotOpen : QFile::Text))) {
        emit error(tr("Cannot save: ") + f.errorString());
        return;
    }
    f.write((isHtml ? m_doc->toHtml() : m_doc->toPlainText()).toLocal8Bit());
    f.close();
    setFileUrl(QUrl::fromLocalFile(localPath));
}

QUrl DocumentHandler::fileUrl() const
{
    return m_fileUrl;
}

QString DocumentHandler::text() const
{
    return m_text;
}

void DocumentHandler::setCursorPosition(int position)
{
    if (position == m_cursorPosition)
        return;
    m_cursorPosition = position;
    reset();
}


void DocumentHandler::reset()
{
    if(mutex == false){
        return;
    }
    emit fontFamilyChanged();
    emit alignmentChanged();
    emit boldChanged();
    emit italicChanged();
    emit underlineChanged();
    emit fontSizeChanged();
    emit textColorChanged();
}

QTextCursor DocumentHandler::textCursor() const
{
    if (!m_doc)
        return QTextCursor();

    QTextCursor cursor = QTextCursor(m_doc);
    if (m_selectionStart != m_selectionEnd) {
        cursor.setPosition(m_selectionStart);
        cursor.setPosition(m_selectionEnd, QTextCursor::KeepAnchor);
    } else {
        cursor.setPosition(m_cursorPosition);
    }
    return cursor;
}

void DocumentHandler::mergeFormatOnWordOrSelection(const QTextCharFormat &format)
{
    QTextCursor cursor = textCursor();
    if (!cursor.hasSelection())
        cursor.select(QTextCursor::WordUnderCursor);
    cursor.mergeCharFormat(format);
}

void DocumentHandler::setSelectionStart(int position)
{
    m_selectionStart = position;
}

void DocumentHandler::setSelectionEnd(int position)
{
    m_selectionEnd = position;
}

void DocumentHandler::setAlignment(Qt::Alignment a)
{
    if (!m_doc)
        return;

    QTextBlockFormat fmt;
    fmt.setAlignment(a);
    QTextCursor cursor = QTextCursor(m_doc);
    cursor.setPosition(m_selectionStart, QTextCursor::MoveAnchor);
    cursor.setPosition(m_selectionEnd, QTextCursor::KeepAnchor);
    cursor.mergeBlockFormat(fmt);
    emit alignmentChanged();
}

Qt::Alignment DocumentHandler::alignment() const
{
    QTextCursor cursor = textCursor();
    if (cursor.isNull())
        return Qt::AlignLeft;
    return textCursor().blockFormat().alignment();
}

bool DocumentHandler::showCursor() const
{
    return this->externCursorEnabled;
}

bool DocumentHandler::showClientColor() const
{
    return this->showClientColorEnabled;
}

bool DocumentHandler::readOnly() const
{
    return this->readOnlyEnabled;
}

bool DocumentHandler::isFileOpened() const
{
    return this->isFileOpenedEnabled;
}

bool DocumentHandler::bold() const
{
    QTextCursor cursor = textCursor();
    if (cursor.isNull())
        return false;
    return textCursor().charFormat().fontWeight() == QFont::Bold;
}

bool DocumentHandler::italic() const
{
    QTextCursor cursor = textCursor();
    if (cursor.isNull())
        return false;
    return textCursor().charFormat().fontItalic();
}

bool DocumentHandler::underline() const
{
    QTextCursor cursor = textCursor();
    if (cursor.isNull())
        return false;
    return textCursor().charFormat().fontUnderline();
}

void DocumentHandler::setBold(bool arg)
{
    QTextCharFormat fmt;
    fmt.setFontWeight(arg ? QFont::Bold : QFont::Normal);
    mergeFormatOnWordOrSelection(fmt);
    emit boldChanged();
}

void DocumentHandler::setShowCursor(bool arg){
    if(c.isLogged() && c.fileIsOpen()){
        int cursor, start, end;
        getCursors(cursor, start, end);
        this->externCursorEnabled = arg;
        setModelText();
        setReadOnly(showClientColorEnabled);
        setCursorPosition(cursor, start, end);
    }
}

void DocumentHandler::setShowClientColor(bool arg){
    if(c.isLogged() && c.fileIsOpen()){
        int cursor, start, end;
        getCursors(cursor, start, end);
        this->showClientColorEnabled = arg;
        setModelText();
        setReadOnly(showClientColorEnabled);
        setCursorPosition(cursor, start, end);
    }
}

void DocumentHandler::setReadOnly(bool arg){
    this->readOnlyEnabled = arg;
    emit readOnlyChanged();
}

void DocumentHandler::setIsFileOpened(bool arg) {
    this->isFileOpenedEnabled = arg;
    emit isFileOpenedChanged();
}

void DocumentHandler::setItalic(bool arg)
{
    QTextCharFormat fmt;
    fmt.setFontItalic(arg);
    mergeFormatOnWordOrSelection(fmt);
    emit italicChanged();

}

void DocumentHandler::setUnderline(bool arg)
{
    QTextCharFormat fmt;
    fmt.setFontUnderline(arg);
    mergeFormatOnWordOrSelection(fmt);
    emit underlineChanged();
}

int DocumentHandler::fontSize() const
{
    QTextCursor cursor = textCursor();
    if (cursor.isNull())
        return 0;
    QTextCharFormat format = cursor.charFormat();
    return format.font().pointSize();
}

void DocumentHandler::setFontSize(int arg)
{
    QTextCursor cursor = textCursor();
    if (cursor.isNull())
        return;
    QTextCharFormat format;
    format.setFontPointSize(arg);
    mergeFormatOnWordOrSelection(format);
    emit fontSizeChanged();
}

QColor DocumentHandler::textColor() const
{
    QTextCursor cursor = textCursor();
    if (cursor.isNull())
        return QColor(Qt::black);
    QTextCharFormat format = cursor.charFormat();
    return format.foreground().color();
}

void DocumentHandler::setTextColor(const QColor &c)
{
    QTextCursor cursor = textCursor();
    if (cursor.isNull())
        return;
    QTextCharFormat format;
    format.setForeground(QBrush(c));
    mergeFormatOnWordOrSelection(format);
    emit textColorChanged();
}

QString DocumentHandler::fontFamily() const
{
    QTextCursor cursor = textCursor();
    if (cursor.isNull())
        return QString();
    QTextCharFormat format = cursor.charFormat();
    return format.font().family();
}

void DocumentHandler::setFontFamily(const QString &arg)
{
    QTextCursor cursor = textCursor();
    if (cursor.isNull())
        return;
    QTextCharFormat format;
    QFont f;
    f.setFamily(arg);
    format.setFont(f);
    format.setFontPointSize(fontSize());
    mergeFormatOnWordOrSelection(format);
    emit fontFamilyChanged();
}

QStringList DocumentHandler::defaultFontSizes() const
{
    // uhm... this is quite ugly
    QStringList sizes;
    QFontDatabase db;
    foreach (int size, db.standardSizes())
        sizes.append(QString::number(size));
    return sizes;
}


/**
 * @brief DocumentHandler::getSymbols get the symbols in the document
 * @return Symbols are internal without fractional position and clientId
 */
vector<InternalSymbol> DocumentHandler::getSymbols(){
    mutex = false;
    vector<InternalSymbol> retu;
    for (auto block = m_doc->begin(); block != m_doc->end(); block = block.next()) {

        if(block != m_doc->begin())
            retu.push_back(InternalSymbol('\n', false, false, false, static_cast<int>(block.charFormat().fontPointSize()), block.charFormat().fontFamily(), "#000000", block.blockFormat().alignment()));

        Qt::Alignment align = block.blockFormat().alignment();
        for(auto it = block.begin(); it != block.end(); it++) {

            QTextFragment fragment = it.fragment();
            QString text = fragment.text();
            QFont f = fragment.charFormat().font();
            QBrush color = fragment.charFormat().foreground();

            for(QChar c : text) {
                if(c != QChar::SpecialCharacter::ObjectReplacementCharacter)
                retu.push_back(InternalSymbol(c, f.bold(), f.italic(), f.underline(), f.pointSize(), f.family(), color.color().name(), align));
            }

        }
    }
    mutex = true;
    return retu;
}

void DocumentHandler::setModelText(){
    mutex = false;
    vector<pair<FracPos,QString>> positions= c.getPositionColor();
    vector<Symbol> text = c.getSymbols();

    QTextDocument m;
    QTextCursor cursor(&m);

    if(externCursorEnabled){
        FracPos zero;
        zero.fracPos.push_back(INT_MIN);
        for(auto pos : positions){
            if(zero == pos.first){
                cursor.insertHtml(pos.second);
            }
        }
    }

    if(!text.empty()){
        QTextBlockFormat b;
        QTextCharFormat format;
        QColor color;
        QFont qf;

        b.setAlignment(text.begin()->getAlignment());
        cursor.setBlockFormat(b);

        for(auto sIt = text.begin(); sIt != text.end();  sIt++){

            if(sIt->getC() == '\n' ){
                if(showClientColorEnabled){
                    color.setNamedColor(COLORS[0]);
                    format.setBackground(QBrush(color));
                }
                b.setAlignment(sIt->getAlignment());
                cursor.insertBlock(b, format);
            }else{
                qf.setBold(sIt->getBold());
                qf.setItalic(sIt->getItalic());
                qf.setUnderline(sIt->getUnderline());
                qf.setPointSize(sIt->getSize());
                qf.setFamily(sIt->getFamily());
                format.setFont(qf);

                color.setNamedColor(sIt->getColor());
                format.setForeground(QBrush(color));

                if(showClientColorEnabled){
                    color.setNamedColor(sIt->getBackGroundColor());
                    format.setBackground(QBrush(color));
                }
                cursor.insertText(QString(sIt->getC()), format);
            }

            //Draw clients cursors
            if(externCursorEnabled){
                for(auto pos : positions){
                    if(sIt->getFracPos().fracPos == pos.first.fracPos){
                        QString sizehtml(" height=\"");
                        int size = (sIt->getSize()>20)? sIt->getSize() : 20;
                        sizehtml.append(QString::number(size)).append("\" ");
                        pos.second.insert(5, sizehtml);
                        cursor.insertHtml(pos.second);
                    }
                }
            }
        }
    }
    m_doc->setHtml(m.toHtml());
    mutex = true;
    setReadOnly(showClientColorEnabled);
}

