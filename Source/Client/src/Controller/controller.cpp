#include "controller.h"
#include "../documenthandler.h"
#include "../ConnectionInterface/connectioninterface.h"
#include "../QT_Library/InputTextDialog/inputtextdialog.h"
#include "../Library/Constants/constants.h"
using namespace COMMON_MESSAGES;

/**
 * @brief openInputTextDialog support function to open a inputFileDialog
 * @param title
 * @param message
 * @param placeholder
 * @param ok = 1 if the user press ok.
 * @return the string placed by the user
 */
QString openInputTextDialog(QString title, QString message, QString placeholder, bool &ok){
    InputTextDialog itd(title, message, placeholder, nullptr);
    itd.exec();
    ok = itd.getOk();
    return itd.getText();
}


void Controller::closeDocument(){
    modello = Model();
    oldStr.clear();
    documentHandler->closeDocument();
    documentHandler->hdConnectedClientCount();
}

bool Controller::checkFuture(){
    if(futureRunnig && future.wait_for(chrono::seconds(0))==future_status::ready){
        futureRunnig = false;
        timer.stop();
        QString backcolor = COLORS[profileClient.getBackgroundColor()];
        std::list<InternalAction> actions = future.get();
        vector<ExternalAction> ex = modello.makeAction(actions, backcolor);

        EditMessage edMessage(profileClient.getClientId(), modello.getDocumentName(), ex, {pair<int, FracPos>(profileClient.getClientId(), profileClient.getCursorPosition())});

        connectionInterface->sendEditMessage(edMessage);
        oldStr = modello.getInternalSymbols();
    }
    return futureRunnig;
}

void Controller::manageSocketError(){
    this->profileClient = Client();
    this->closeDocument();
    documentHandler->setState(G_NO_LOGGED);
    delete connectionInterface;

    connectionInterface = new ConnectionInterface(nullptr, this);

    guiLoginAction();
}

Controller::Controller(DocumentHandler *d): modello("","", -1){
    documentHandler = d;
    closeDocument();
    connectionInterface = new ConnectionInterface(nullptr, this);
    this->guiLoginAction();
}

void Controller::textHasChanged(vector<InternalSymbol> newStr){

    if(!isLogged() || !fileIsOpen() || oldStr == newStr){
        return;
    }

    if(checkFuture()){
        return;
    }

    QString backcolor = COLORS[profileClient.getBackgroundColor()];

    if(editDistanceTool.needNewThread(newStr, oldStr)){
        futureRunnig = true;
        timer.setInterval(1000);
        timer.start();
        connect(&timer, &QTimer::timeout, [this](){
            this->checkFuture();
        });

        this->future = std::async(std::launch::async, [newStr, this](){
            return editDistanceTool.getActionsRecursive(newStr, oldStr);
        });

    } else {
        list<InternalAction> ia = editDistanceTool.getActionsRecursive(newStr, oldStr);

        vector<ExternalAction> ex = modello.makeAction(ia, backcolor);
        EditMessage edMessage(profileClient.getClientId(), modello.getDocumentName(), ex, {pair<int, FracPos>(profileClient.getClientId(), profileClient.getCursorPosition())});

        connectionInterface->sendEditMessage(edMessage);
        oldStr = std::move(newStr);
    }
}

void Controller::guiCreateFile(){
    if(!isLogged()) return;

    bool ok;
    QString filename = openInputTextDialog(D_CREATE_FILE_TITLE, "Nome file", "", ok);
    if(ok && !filename.isEmpty()){
        ManageFileMessage m(profileClient.getClientId(), Create, filename, "");
        connectionInterface->sendManageFileMessage(m);
        documentHandler->shwConnectedClientCount();
    }

}

void Controller::guiDeleteFile(){
    if(!isLogged() || !fileIsOpen()) return;

    QMessageBox msgBox;
    msgBox.setWindowTitle(D_DELETE_FILE_TITLE);
    msgBox.setText(D_DELETE_FILE_MESSAGE);
    msgBox.setStandardButtons(QMessageBox::No | QMessageBox::Yes );
    msgBox.setDefaultButton(QMessageBox::No);
    int ret = msgBox.exec();
    if  (ret == QMessageBox::Yes) {
        ManageFileMessage manageFileMessage = ManageFileMessage(profileClient.getClientId(), Delete, modello.getDocumentName(), modello.getURI());
        connectionInterface->sendManageFileMessage(manageFileMessage);
        this->closeDocument();
    }
}

void Controller::guiLoginAction(){
    if(isLogged()) return;
    LoginDialog l;
    l.exec();

    if(l.isValid()){
        AccountMessage accountMessage = AccountMessage(-1, l.getUser(), l.getPass(), Login);
        //Account message sent to the server
        connectionInterface->sendAccountMessage(accountMessage);
    }
    if(l.isCreate()){
        guiCreateAccount();
    }
}

void Controller::guiModifyProfile(){
    if(!isLogged()) return;


    ProfileDialog p(nullptr, profileClient.getIconNumber());
    p.setNickname(profileClient.getNickname());
    p.exec();

    if(p.isValid()){
        connectionInterface->sendAccountMessage(
                    AccountMessage(profileClient.getClientId(),
                                   profileClient.getUsername(),
                                   p.getNickname(),
                                   p.getPassword(),
                                   p.getIconNumber(),
                                   profileClient.getBackgroundColor(),
                                   UpdateAccount));
    }
}

void Controller::guiLogoutAction(){
    if(!isLogged()) return;

    AccountMessage m(profileClient.getClientId(), "", "", Logout);
    connectionInterface->sendAccountMessage(m);
}

void Controller::guiDeleteAccount(){
    if(!isLogged()) return;

    bool ok_user;
    QString user = openInputTextDialog(D_DELETE_ACCOUNT_TITLE, D_DELETE_ACCOUNT_MESSAGE_USER, "", ok_user);

    if(ok_user){
        bool ok_pass;
        QString pass = openInputTextDialog(D_DELETE_ACCOUNT_TITLE, D_DELETE_ACCOUNT_MESSAGE_PASS, "", ok_pass);

        if(ok_pass){
            AccountMessage accountMessage = AccountMessage(profileClient.getClientId(), user, pass, DeleteAccount);
            //Account message sent to the server
            connectionInterface->sendAccountMessage(accountMessage);
        }
    }
}

void Controller::guiCreateAccount(){
    if(isLogged()) return;

    CreateAccDialog cd;
    cd.exec();

    if(cd.isValid()){
        AccountMessage accountMessage = AccountMessage(-1, cd.getUsername(), cd.getNickname(),
                                                       cd.getPassword(), cd.getIconNumber(), 0, NewAccount);
        //Account message sent to the server
        connectionInterface->sendAccountMessage(accountMessage);
    }


}

void Controller::guiExportPdf(QString htmlText){
    if(isLogged() && fileIsOpen())
        PDFConverter tool(htmlText);
}

void Controller::guiOpenFile(){
    if(!isLogged())    return;

    ListDocumentMessage l = ListDocumentMessage(profileClient.getClientId());
    connectionInterface->sendListDocumentMessage(l);
}

void Controller::guiShareFile(){
    if(!isLogged() || !fileIsOpen())    return;

    bool ok;
    QString URI = modello.getURI();
    openInputTextDialog(D_SHARE_FILE_TITLE, D_SHARE_FILE_TITLE, URI, ok);
}

void Controller::guiOpenURIFile(){
    if(!isLogged()) return;

    bool ok;
    QString URI = openInputTextDialog(D_OPEN_URI_FILE_TITLE, D_OPEN_URI_FILE_MESSAGE, "", ok);

    if(ok && !URI.isEmpty()){
        ManageFileMessage m(profileClient.getClientId(), OpenByURI, "", URI);
        connectionInterface->sendManageFileMessage(m);
    }
}

void Controller::cursorPositionChange(int cursor){
    if(!isLogged() || !fileIsOpen())    return;

    FracPos cursorPosition = getFracPosByCursorPosition(cursor);

    if(cursorPosition != profileClient.getCursorPosition()){
        profileClient.setCursorPosition(cursorPosition);

        EditMessage editMessage(profileClient.getClientId(),
                                modello.getDocumentName(),
        {},
        {pair<int, FracPos>(profileClient.getClientId(), profileClient.getCursorPosition())});

        connectionInterface->sendEditMessage(editMessage);
    }
}

vector<Symbol> Controller::getSymbols(){
    return modello.getSymbols();
}

void Controller::manageServerMessage(ListDocumentMessage m){
    QStringList Qfiles;
    for(QString filename : m.documents){
        Qfiles << filename;
    }
    bool ok;
    if(Qfiles.empty())
        return;

    QString selected = QInputDialog::getItem(nullptr ,D_OPEN_FILE_TITLE,
                                             D_OPEN_FILE_MESSAGE, Qfiles, 0,
                                             false, &ok, Qt::MSWindowsFixedSizeDialogHint | Qt::WindowCloseButtonHint);

    if(ok && !selected.isEmpty()){

        ManageFileMessage manageFileMessage(profileClient.getClientId(), Open, selected, "");
        connectionInterface->sendManageFileMessage(manageFileMessage); //Message sent to server
    }

}

void Controller::manageServerMessage(EditMessage m){

    if(modello.getDocumentName() != m.document) return;

    int cursor, start, end;
    documentHandler->getCursors(cursor, start, end);

    FracPos cursorf, startf, endf;
    getFracPosByCursorPosition(cursor, start, end, cursorf, startf, endf);

    modello.makeAction(m.externalActions);

    for(auto clientCursor = m.clientsCursors.begin(); clientCursor!= m.clientsCursors.end(); clientCursor++){
        modello.updateClient(clientCursor->first, clientCursor->second);
    }

    oldStr = modello.getInternalSymbols();
    documentHandler->setModelText();

    getCursorPositionByFracPos(cursorf, startf, endf, cursor, start, end);

    documentHandler->setCursorPosition(start, end, cursor);
}

void Controller::manageServerMessage(AccountMessage m){
    switch(m.accountOperationType){


    case AccountOperationType::Login:
        profileClient = Client(m.clientId, m.username, m.nickname, m.iconNumber, m.backgroundColor);
        closeDocument();
        documentHandler->setState("Logged as clientId = " + QString::number(profileClient.getClientId())
                                  + " nick = "  + profileClient.getNickname()
                                  + " username = " + profileClient.getUsername());
        documentHandler->lgnOwnerClient(profileClient.getBackgroundColor(), profileClient.getIconNumber());
        break;

    case AccountOperationType::Logout:
        profileClient = Client();
        closeDocument();
        documentHandler->setState(G_NO_LOGGED);
        documentHandler->lgtOwnerClient();
        guiLoginAction();
        break;

    case AccountOperationType::NewAccount:
        break;

    case AccountOperationType::DeleteAccount:
        profileClient = Client();
        closeDocument();
        documentHandler->setState(G_NO_LOGGED);
        documentHandler->lgtOwnerClient();
        guiLoginAction();
        break;

    case AccountOperationType::UpdateAccount:
        profileClient = Client(m.clientId, m.username, m.nickname, m.iconNumber, m.backgroundColor);
        documentHandler->setState("Logged as clientId = " + QString::number(profileClient.getClientId())
                                  + " nick = "  + profileClient.getNickname()
                                  + " username = " + profileClient.getUsername());
        documentHandler->lgnOwnerClient(profileClient.getBackgroundColor(), profileClient.getIconNumber());
        break;        
    }
}

void Controller::manageServerMessage(ErrorMessage m){
    QMessageBox msgBox;
    QString qstr = m.message;
    msgBox.setText(qstr);
    msgBox.exec();

    switch (m.errorType) {
    case NoAction:
        break;
    case NetworkError:
        break;
    }
}

void Controller::manageServerMessage(ManageFileMessage m){
    switch (m.operationType) {
    case Open:
        modello = Model(m.document, m.URI, profileClient.getClientId());
        oldStr.clear();
        documentHandler->clear();
        documentHandler->setModelText();
        documentHandler->setCursorPosition(0, 0, 0);
        documentHandler->setDocumentTitle(m.document);
        documentHandler->clrConnectedClientList();
        documentHandler->showConnectedClientCount();
        documentHandler->setIsFileOpened(true);
        break;

    case OpenByURI:
        modello = Model(m.document, m.URI, profileClient.getClientId());
        oldStr.clear();
        documentHandler->setModelText();
        documentHandler->setCursorPosition(0, 0, 0);
        documentHandler->setDocumentTitle(m.document);
        documentHandler->clrConnectedClientList();
        documentHandler->showConnectedClientCount();
        documentHandler->setIsFileOpened(true);
        break;

    case Close:
        closeDocument();
        break;

    case Create:
        break;

    case Delete:
        closeDocument();
        break;
    }
}

void Controller::manageServerMessage(InfoMessage m){
    QMessageBox msgBox;
    QString qstr = m.message;
    msgBox.setText(qstr);
    msgBox.exec();
}

void Controller::manageServerMessage(ActiveClientMessage m){
    int cursor, start, end;
    documentHandler->getCursors(cursor, start, end);

    FracPos cursorf, startf, endf;
    getFracPosByCursorPosition(cursor, start, end, cursorf, startf, endf);

    if(m.activeClientOperation == AddClient) {
        for(Client client : m.activeClients) {
            documentHandler->setConnectedClient(client.getNickname(), client.getBackgroundColor(), client.getIconNumber());
            modello.addActiveClient(client);
        }
    } else if(m.activeClientOperation == DeleteClient) {
        vector<Client> tmp = modello.getActiveClients();
        for(Client client : m.activeClients){
            auto it = std::find_if(tmp.begin(), tmp.end(), [&client](Client el){
                    return el.getClientId() == client.getClientId();
                    });
            documentHandler->delConnectedClient(static_cast<int>(std::distance(tmp.begin(), it)));
            modello.removeClient(client.getClientId());
        }
    } else if(m.activeClientOperation == UpdateClient) {
        vector<Client> tmp = modello.getActiveClients();
        for(Client client : m.activeClients) {
            auto it = std::find_if(tmp.begin(), tmp.end(), [&client](Client el){
                    return el.getClientId() == client.getClientId();
        });

            documentHandler->updConnectedClient(static_cast<int>(std::distance(tmp.begin(), it)), client.getNickname(), client.getBackgroundColor(), client.getIconNumber());
            modello.updateActiveClient(static_cast<uint>(std::distance(tmp.begin(), it)), client);
        }
    }

    getCursorPositionByFracPos(cursorf, startf, endf, cursor, start, end);

    documentHandler->setModelText();
    documentHandler->setCursorPosition(start, end, cursor);
}

vector<pair<FracPos,QString>> Controller::getPositionColor(){
    return modello.getPositionColor();
}

FracPos Controller::getFracPosByCursorPosition(int cursor){
    cursor--;
    bool showCursor = documentHandler->showCursor();

    FracPos cursorPosition;

    multimap<FracPos, bool> imageOrSymbol;

    for(auto s : modello.getSymbols()){
        imageOrSymbol.insert(pair<FracPos, bool>(s.getFracPos(), false));
    }

    if(showCursor){
        vector<FracPos> clientPositions = modello.getClientsPositions();
        for (auto clientpos: clientPositions){
            imageOrSymbol.insert(pair<FracPos, bool>(clientpos, true));
        }
    }

    if(cursor < 0){
        cursorPosition.fracPos.push_back(INT_MIN);
        return cursorPosition;
    }else if(cursor < static_cast<int>(imageOrSymbol.size())){
             auto it = imageOrSymbol.begin();
             for (int i = 0; i < cursor; i++){
                 it++;
             }
             if(it != imageOrSymbol.end()){
                 cursorPosition = it->first;
             }
    }else{
        if(!imageOrSymbol.empty()){
            cursorPosition = imageOrSymbol.rbegin()->first;
        }
    }

    return cursorPosition;
}

void Controller::getFracPosByCursorPosition(int cursor, int start, int end, FracPos &cursoPos, FracPos &startPos, FracPos &endPos){
    cursor--;
    start--;
    end--;
    vector<int> cursors = {cursor, start, end};
    vector<FracPos> cursorsPos = {FracPos(), FracPos(), FracPos()};

    bool showCursor = documentHandler->showCursor();



    multimap<FracPos, bool> imageOrSymbol;

    for(auto s : modello.getSymbols()){
        imageOrSymbol.insert(pair<FracPos, bool>(s.getFracPos(), false));
    }

    if(showCursor){
        vector<FracPos> clientPositions = modello.getClientsPositions();
        for (auto clientpos: clientPositions){
            imageOrSymbol.insert(pair<FracPos, bool>(clientpos, true));
        }
    }

    for(uint j = 0; j<3; j++){
        if(cursors[j] < 0){
            cursorsPos[j].fracPos.push_back(INT_MIN);

        }else if(cursors[j] < static_cast<int>(imageOrSymbol.size())){
                 auto it = imageOrSymbol.begin();
                 for (int i = 0; i < cursors[j]; i++){
                     it++;
                 }
                 if(it != imageOrSymbol.end()){
                     cursorsPos[j] = it->first;
                 }
        }else{
            if(!imageOrSymbol.empty()){
                cursorsPos[j] = imageOrSymbol.rbegin()->first;
            }
        }
    }

    cursoPos = cursorsPos[0];
    startPos = cursorsPos[1];
    endPos   = cursorsPos[2];
}

int Controller::getCursorPositionByFracPos(FracPos fracPosCursor){
    if(fracPosCursor.fracPos.size()==0 || fracPosCursor.fracPos.at(0) == INT_MIN){
        return 0;
    }

    bool showCursor = documentHandler->showCursor();
    int cursor = 0;

    vector<Symbol> text = modello.getSymbols();
    multimap<FracPos, bool> imageOrSymbol;
    for(auto s : text){
        imageOrSymbol.insert(pair<FracPos, bool>(s.getFracPos(), false));
    }

    if(showCursor){
        vector<FracPos> clientPositions = modello.getClientsPositions();
        for (auto clientpos: clientPositions){
            imageOrSymbol.insert(pair<FracPos, bool>(clientpos, true));
        }
    }

    for(auto s : imageOrSymbol){
        if(s.first < fracPosCursor || s.first == fracPosCursor){
            cursor++;
        }else{
            break;
        }
    }

    return cursor;
}

void Controller::getCursorPositionByFracPos(FracPos fracCursor, FracPos fracStart, FracPos fracEnd, int &cursorPos, int &start, int &end){
    vector<FracPos> cursorsFrac = {fracCursor, fracStart, fracEnd };
    vector<int> cursors = {0,0,0};

        bool showCursor = documentHandler->showCursor();


        vector<Symbol> text = modello.getSymbols();
        multimap<FracPos, bool> imageOrSymbol;
        for(auto s : text){
            imageOrSymbol.insert(pair<FracPos, bool>(s.getFracPos(), false));
        }

        if(showCursor){
            vector<FracPos> clientPositions = modello.getClientsPositions();
            for (auto clientpos: clientPositions){
                imageOrSymbol.insert(pair<FracPos, bool>(clientpos, true));
            }
        }

        for(uint j = 0; j < 3; j++){

            if(cursorsFrac[j].fracPos.size()==0 || cursorsFrac[j].fracPos.at(0) == INT_MIN){
                cursors[j] = 0;
            }

            for(auto s : imageOrSymbol){
                if(s.first < cursorsFrac[j] || s.first == cursorsFrac[j]){
                    cursors[j]++;
                }else{
                    break;
                }
            }
        }

        cursorPos = cursors[0];
        start = cursors[1];
        end = cursors[2];

}

Controller::~Controller(){
    if(connectionInterface != nullptr){
        delete connectionInterface;
    }
}
