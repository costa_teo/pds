#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <QMessageBox>
#include <future>
#include <QInputDialog>
#include <QTimer>

#include "../EditDistance/editdistance.h"
#include "../PDFConverter/pdfconverter.h"
#include "../Library/library.h"
#include "../QT_Library/LoginDialog/logindialog.h"
#include "../QT_Library/ProfileDialog/profiledialog.h"
#include "../QT_Library/CreateDialog/createdialog.h"

class DocumentHandler;
class ConnectionInterface;

using namespace std;

class Controller : public QObject
{

private:
    vector<InternalSymbol> oldStr;
    Client profileClient;
    DocumentHandler *documentHandler;
    Model modello;
    EditDistance editDistanceTool;

    std::future<std::list<InternalAction>> future;
    bool futureRunnig = false;
    ConnectionInterface *connectionInterface;
    QTimer timer;

    bool checkFuture();

    FracPos getFracPosByCursorPosition(int cursor);
    void getFracPosByCursorPosition(int cursor, int start, int end, FracPos &cursoPos, FracPos &startPos, FracPos &endPos);

    int getCursorPositionByFracPos(FracPos fracPosCursor);
    void getCursorPositionByFracPos(FracPos fracCursor, FracPos fracStart, FracPos fracEnd, int &cursorPos, int &start, int &end);


public:
    Controller(DocumentHandler *d);
    void closeDocument();
    void textHasChanged(vector<InternalSymbol> newStr);
    void cursorPositionChange(int position);

    void guiOpenFile();
    void guiCreateFile();
    void guiDeleteFile();
    void guiModifyProfile();
    void guiLoginAction();
    void guiLogoutAction();
    void guiDeleteAccount();
    void guiCreateAccount();
    void guiExportPdf(QString htmlText);
    void guiShareFile();
    void guiOpenURIFile();
    void guiInsertActiveClient(QString nickname, QString color, QString icon);
    void guiRemoveActiveClient(int position);
    void guiClearActiveClients();


    vector<Symbol> getSymbols();
    vector<pair<FracPos,QString>>getPositionColor();

    Client getClientId(){
        return profileClient;
    }

    bool isLogged(){
        return profileClient.getClientId() != -1;
    }

    bool fileIsOpen(){
        return  !modello.isValid();
    }

    void manageServerMessage(AccountMessage);
    void manageServerMessage(ErrorMessage);
    void manageServerMessage(ManageFileMessage);
    void manageServerMessage(EditMessage);
    void manageServerMessage(ListDocumentMessage);
    void manageServerMessage(InfoMessage);
    void manageServerMessage(ActiveClientMessage);

    void manageSocketError();
    ~Controller();
};

#endif // CONTROLLER_H



