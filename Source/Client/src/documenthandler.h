/****************************************************************************
**
** Copyright (C) 2016 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** BSD License Usage
** Alternatively, you may use this file under the terms of the BSD license
** as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef DOCUMENTHANDLER_H
#define DOCUMENTHANDLER_H

#include <QQuickTextDocument>
#include <QtGui/QTextCharFormat>
#include <QtCore/QTextCodec>
#include <qqmlfile.h>
#include <QtGui/QTextDocument>
#include <QtGui/QTextCursor>
#include <QtGui/QFontDatabase>
#include <QtCore/QFileInfo>
#include <QTextBlock>
#include <QImage>

#include "./Controller/controller.h"

QT_BEGIN_NAMESPACE
class QTextDocument;
QT_END_NAMESPACE

class DocumentHandler : public QObject
{
    Q_OBJECT

    Q_ENUMS(HAlignment)

    Q_PROPERTY(QQuickItem *target READ target WRITE setTarget NOTIFY targetChanged)
    Q_PROPERTY(int cursorPosition READ cursorPosition WRITE setCursorPosition NOTIFY cursorPositionChanged)
    Q_PROPERTY(int selectionStart READ selectionStart WRITE setSelectionStart NOTIFY selectionStartChanged)
    Q_PROPERTY(int selectionEnd READ selectionEnd WRITE setSelectionEnd NOTIFY selectionEndChanged)

    Q_PROPERTY(QColor textColor READ textColor WRITE setTextColor NOTIFY textColorChanged)
    Q_PROPERTY(QString fontFamily READ fontFamily WRITE setFontFamily NOTIFY fontFamilyChanged)
    Q_PROPERTY(Qt::Alignment alignment READ alignment WRITE setAlignment NOTIFY alignmentChanged)

    Q_PROPERTY(bool bold READ bold WRITE setBold NOTIFY boldChanged)
    Q_PROPERTY(bool italic READ italic WRITE setItalic NOTIFY italicChanged)
    Q_PROPERTY(bool underline READ underline WRITE setUnderline NOTIFY underlineChanged)

    Q_PROPERTY(bool showCursor READ showCursor WRITE setShowCursor NOTIFY showCursorChanged)
    Q_PROPERTY(bool showClientColor READ showClientColor WRITE setShowClientColor NOTIFY showClientColorChanged)
    Q_PROPERTY(bool readOnly READ readOnly WRITE setReadOnly NOTIFY readOnlyChanged)
    Q_PROPERTY(bool isFileOpened READ isFileOpened WRITE setIsFileOpened NOTIFY isFileOpenedChanged)
    Q_PROPERTY(int fontSize READ fontSize WRITE setFontSize NOTIFY fontSizeChanged)

    Q_PROPERTY(QStringList defaultFontSizes READ defaultFontSizes NOTIFY defaultFontSizesChanged)

    Q_PROPERTY(QUrl fileUrl READ fileUrl WRITE setFileUrl NOTIFY fileUrlChanged)
    Q_PROPERTY(QString text READ text WRITE setText NOTIFY textChanged)
    Q_PROPERTY(QString documentTitle READ documentTitle WRITE setDocumentTitle NOTIFY documentTitleChanged)
    Q_PROPERTY(QString state READ state WRITE setState NOTIFY stateChanged)

public:
    DocumentHandler();
    QQuickItem *target() { return m_target; }

    void setTarget(QQuickItem *target);

    void setCursorPosition(int start, int end, int position){
        m_selectionStart = start;
        m_selectionEnd = end;
        m_cursorPosition = position;
        emit cursorPositionMatte();
    }

    void getCursors(int &cursorPos, int &selectionStart, int &selectionEnd){
        cursorPos = m_cursorPosition;
        selectionStart = m_selectionStart;
        selectionEnd = m_selectionEnd;
    }

    void setCursorPosition(int position);
    void setSelectionStart(int position);
    void setSelectionEnd(int position);

    int cursorPosition() const { return m_cursorPosition; }
    int selectionStart() const { return m_selectionStart; }
    int selectionEnd() const { return m_selectionEnd; }

    QString fontFamily() const;

    QColor textColor() const;

    Qt::Alignment alignment() const;
    void setAlignment(Qt::Alignment a);

    bool showCursor() const;
    bool showClientColor() const;
    bool readOnly() const;
    bool isFileOpened() const;
    bool bold() const;
    bool italic() const;
    bool underline() const;
    int fontSize() const;

    QStringList defaultFontSizes() const;
    QUrl fileUrl() const;
    QString text() const;
    QString toPlainText(){
        return m_doc->toPlainText();
    }

    QString documentTitle() const;
    QString state() const;
    void setModelText();
    vector<InternalSymbol> getSymbols();

    Q_INVOKABLE void serverOpenFile(){
        timer->disconnect();
        timer->setSingleShot(true);
        timer->setInterval(10);
        connect(timer, &QTimer::timeout, [this](){
                c.guiOpenFile();
        });
        timer->start();
    }

    Q_INVOKABLE void serverCreateFile(){
        timer->disconnect();
        timer->setSingleShot(true);
        timer->setInterval(10);
        connect(timer, &QTimer::timeout, [this](){
                c.guiCreateFile();
        });
        timer->start();
    }

    Q_INVOKABLE void serverDeleteFile(){
        c.guiDeleteFile();
    }

    Q_INVOKABLE void serverModifyProfile(){
        timer->disconnect();
        timer->setSingleShot(true);
        timer->setInterval(10);
        connect(timer, &QTimer::timeout, [this](){
                c.guiModifyProfile();
        });
        timer->start();
    }

    Q_INVOKABLE void loginAction(){
        timer->disconnect();
        timer->setSingleShot(true);
        timer->setInterval(10);
        connect(timer, &QTimer::timeout, [this](){
                c.guiLoginAction();
        });
        timer->start();
    }

    Q_INVOKABLE void logoutAction(){
        c.guiLogoutAction();
    }

    Q_INVOKABLE void deleteAccount(){
        timer->disconnect();
        timer->setSingleShot(true);
        timer->setInterval(10);
        connect(timer, &QTimer::timeout, [this](){
                c.guiDeleteAccount();
        });
        timer->start();
    }

    Q_INVOKABLE void createAccount(){
        timer->disconnect();
        timer->setSingleShot(true);
        timer->setInterval(10);
        connect(timer, &QTimer::timeout, [this](){
                c.guiCreateAccount();
        });
        timer->start();
    }

    Q_INVOKABLE void exportPdf(){
        bool beforeCursor = externCursorEnabled;
        setShowCursor(false);
        c.guiExportPdf(m_doc->toHtml());
        setShowCursor(beforeCursor);
    }

    Q_INVOKABLE void serverOpenURIFile(){
        timer->disconnect();
        timer->setSingleShot(true);
        timer->setInterval(10);
        connect(timer, &QTimer::timeout, [this](){
                c.guiOpenURIFile();
        });
        timer->start();
    }

    Q_INVOKABLE void serverShareFile(){
        timer->disconnect();
        timer->setSingleShot(true);
        timer->setInterval(10);
        connect(timer, &QTimer::timeout, [this](){
                c.guiShareFile();
        });
        timer->start();
    }

    Q_INVOKABLE void controllerCursorPositionChanged(int cursor){
        if(mutex == false){
            return;
        }
        c.cursorPositionChange(cursor);
    }

    Q_INVOKABLE void controllerTextChanged(){
        if(mutex == false){
            return;
        }
        vector<InternalSymbol> symbols = getSymbols();
        c.textHasChanged(symbols);
        c.cursorPositionChange(m_cursorPosition);
    }

    void closeDocument(){
        m_text = COMMON_MESSAGES::G_WELCOME;
        emit textChanged();
        setCursorPosition(0, 0, 0);
        setDocumentTitle(COMMON_MESSAGES::G_NO_TITLE);
        clrConnectedClientList();
        hdConnectedClientCount();
        setReadOnly(true);
        setIsFileOpened(false);
    }

    void setDocumentTitle(string newTitle){
        m_documentTitle = QString::fromStdString(newTitle);
        emit documentTitleChanged();
    }

    void clear(){
        m_doc->clear();
    }

    void setConnectedClient(QString nickname, uint color, uint icon) {
        emit addConnectedClient(nickname,
                                COLORS[color],
                                ACCOUNT_ICON[icon].mid(6));
    }

    void delConnectedClient(int pos) {
        emit removeConnectedClient(pos);
    }

    void updConnectedClient(int pos, QString nickname, uint color, uint icon) {
        emit updateConnectedClient(pos,
                                   nickname,
                                   COLORS[color],
                                   ACCOUNT_ICON[icon].mid(6));
    }

    void clrConnectedClientList() {
        emit clearConnectedClientList();
    }

    void shwConnectedClientCount() {
        emit showConnectedClientCount();
    }

    void hdConnectedClientCount() {
        emit hideConnectedClientCount();
    }

    void lgnOwnerClient(uint color, uint icon) {
        emit loginOwnerClient(COLORS[color],
                              ACCOUNT_ICON[icon].mid(6));
    }

    void lgtOwnerClient() {
        emit logoutOwnerClient();
    }

    ~DocumentHandler(){
        if(timer)
        delete timer;
    }

public Q_SLOTS:
    void setBold(bool);
    void setShowCursor(bool);
    void setShowClientColor(bool);
    void setReadOnly(bool);
    void setIsFileOpened(bool);
    void setItalic(bool);
    void setUnderline(bool);
    void setFontSize(int);
    void setTextColor(const QColor &);
    void setFontFamily(const QString &);

    void setFileUrl(const QUrl &);
    void setText(const QString &);
    void saveAs(const QUrl &, const QString &);

    void setDocumentTitle(QString);
    void setState(QString);

Q_SIGNALS:
    void targetChanged();
    void cursorPositionChanged();
    void selectionStartChanged();
    void selectionEndChanged();

    void fontFamilyChanged();
    void textColorChanged();
    void alignmentChanged();

    void boldChanged();
    void showCursorChanged();
    void showClientColorChanged();
    void readOnlyChanged();
    void isFileOpenedChanged();
    void italicChanged();
    void underlineChanged();

    void fontSizeChanged();
    void defaultFontSizesChanged();

    void fileUrlChanged();

    void textChanged();
    void documentTitleChanged();
    void error(QString);
    void stateChanged();
    void cursorPositionMatte();

    /* Sidebar/Count/Profile management */
    void addConnectedClient(QString nickname, QString color, QString icon);
    void removeConnectedClient(int pos);
    void updateConnectedClient(int pos, QString nickname, QString color, QString icon);
    void clearConnectedClientList();
    void showConnectedClientCount();
    void hideConnectedClientCount();
    void loginOwnerClient(QString color, QString icon);
    void logoutOwnerClient();
    /* ***************** */

private:
    bool mutex = true;
    bool externCursorEnabled = true;
    bool showClientColorEnabled = false;
    bool readOnlyEnabled = true;
    bool isFileOpenedEnabled = false;
    void reset();
    void mergeFormatOnWordOrSelection(const QTextCharFormat &format);

    QQuickItem *m_target;
    QTextDocument *m_doc;
    QTextCursor textCursor() const;
    int m_cursorPosition;
    int m_selectionStart;
    int m_selectionEnd;

    QFont m_font;
    int m_fontSize;
    QUrl m_fileUrl;
    QString m_text;
    QString m_documentTitle = "Nessun Documento";
    QString m_state = "Not Logged";
    Controller c;
    QTimer *timer = new QTimer;

};

#endif
