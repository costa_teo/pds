#ifndef PDFCONVERTER_H
#define PDFCONVERTER_H

#include <QtWidgets>
#include <QPrinter>
#include <QString>

class PDFConverter{

public:
    PDFConverter(QString htmlText){
        QString fileName = QFileDialog::getSaveFileName(nullptr, "Export PDF", QString(), "*PDF (*.pdf)");
        if (QFileInfo(fileName).suffix().isEmpty()) { fileName.append("PDF (*.pdf)"); }

        QPrinter printer(QPrinter::PrinterResolution);
        printer.setResolution(96);
        printer.setOutputFormat(QPrinter::PdfFormat);
        printer.setPaperSize(QPrinter::A4);
        printer.setOutputFileName(fileName);

        QTextDocument doc;
        doc.setHtml(htmlText);
        doc.setPageSize(printer.pageRect().size()); // This is necessary if you want to hide the page number
        doc.print(&printer);
    }
};


#endif // PDFCONVERTER_H
