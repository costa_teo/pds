#ifndef EDITDISTANCE_H
#define EDITDISTANCE_H

#include "../Library/Action/action.h"
#include <string>
#include <list>
#include <iostream>

#define DEBUG 0
#define VERBOSE 0
#define maxSize 4900

using namespace std;

class EditDistance{
int tmp[maxSize][maxSize];
unsigned int nCol=0;
unsigned int nRow=0;



void initTool(unsigned int newNrow, unsigned int newNcol){
        nRow = newNrow+1;
        nCol = newNcol+1;
        return;
}


list<InternalAction> getActualEdits(vector<InternalSymbol> newStr, vector<InternalSymbol> oldStr, int offset) {
    unsigned int i = nRow - 1;
    unsigned int j = nCol - 1;
    list<InternalAction> actions;

    while(true) {
        if (i == 0 || j == 0) {
            break;
        }
        if (newStr[i-1] == oldStr[j-1]) {
            i = i-1;
            j = j-1;
        } else if (tmp[i][j] == tmp[i-1][j-1] + 1){

            actions.push_front(InternalAction(Change, j-1, newStr[i-1]));
#if VERBOSE
            cout<< "Edit " << oldStr[j-1] << " in old["<<j-1<<"] to " << newStr[i-1] << endl;
#endif
            i = i-1;
            j = j-1;
        } else if (tmp[i][j] == tmp[i-1][j] + 1) {
            actions.push_front(InternalAction(Insert, j, newStr[i-1]));
#if VERBOSE
            cout << "Insert in old["<<j<<"] " <<  newStr[i-1] << endl;
#endif
            i = i-1;
        } else if (tmp[i][j] == tmp[i][j-1] + 1){
            actions.push_front(InternalAction(Cancel, j-1 , oldStr[j-1]));
#if VERBOSE
            cout << "Delete in old["<< j-1 <<"] " << oldStr[j-1] << endl;
#endif
            j = j -1;
        } else {
            cout << "Some wrong with given data" << endl ;
        }

    }

    //delete and insert last charachter if (i!=0 || j!=0)
    for (unsigned int k = j; k>0 ; k--){
        actions.push_front(InternalAction(Cancel, k-1, oldStr[k-1]));
#if VERBOSE
        cout << "Delete old["<< k-1 <<"] " << oldStr[k-1] << endl;
#endif
    }

    for (unsigned int k = i; k>0 ; k--){
        InternalSymbol c = newStr[k-1];

        actions.push_front(InternalAction(Insert,j,c));
#if VERBOSE
        cout << "Insert in old["<<j<<"] " <<  newStr[k-1] <<  endl;
#endif
    }

#if VERBOSE
//    cout <<"\n\nadjusting the positionof chars based on previous insertion / elimination\n";
#endif

    for (auto a = actions.begin(); a != actions.end(); a++) {
        a->setPos(static_cast<int>(a->getPosition())+offset);
        if(a->getType() == Insert) offset ++;
        if(a->getType() == Cancel) offset --;
#if DEBUG
        cout << "DEBUG: " <<a->getStringType() << " " << a->getC() << " in position " << a->getPosition() << endl;
#endif
    }//end added
return actions;
}

list<InternalAction> getActionsRecursiveFunc(vector<InternalSymbol> newStr, vector<InternalSymbol> oldStr, unsigned int offset){
    if(newStr.size() >= maxSize || oldStr.size() >= maxSize){
        uint newStr_halfSize = static_cast<uint> (newStr.size()/2);
        std::vector<InternalSymbol> newStr_lo(newStr.begin(), newStr.begin() + newStr_halfSize);
        std::vector<InternalSymbol> newStr_hi(newStr.begin() + newStr_halfSize, newStr.end());

        uint oldStr_halfSize = static_cast<uint> (oldStr.size()/2);
        std::vector<InternalSymbol> oldStr_lo(oldStr.begin(), oldStr.begin() + oldStr_halfSize);
        std::vector<InternalSymbol> oldStr_hi(oldStr.begin() + oldStr_halfSize, oldStr.end());


        list<InternalAction> list1 = getActionsRecursiveFunc(newStr_lo, oldStr_lo, offset);
        list<InternalAction> list2 = getActionsRecursiveFunc( newStr_hi, oldStr_hi, offset+(static_cast<unsigned int>(newStr.size()/2)));
        for (auto l2 : list2){
            list1.push_back(l2);
        }
        return list1;
    }

    //ELSE
    initTool(static_cast<unsigned int>(newStr.size()), static_cast<unsigned int>(oldStr.size()));

    for(unsigned int i=0; i < nCol; i++){
        tmp[0][i] = static_cast<int>(i); //initialize first row
    }


    for(unsigned int i=0; i < nRow; i++){
        tmp[i][0] = static_cast<int>(i); // initialize first column
    }


    for(unsigned int i=1;i <=nRow-1; i++){
        for(unsigned int j=1; j <= nCol-1; j++){
            if(newStr[i-1] == oldStr[j-1]){
                tmp[i][j] = tmp[i-1][j-1];
            }else{
                tmp[i][j] = 1 + min(tmp[i-1][j-1], min(tmp[i-1][j], tmp[i][j-1]));
            }
        }
    }

return getActualEdits(newStr, oldStr, static_cast<int>(offset));

}



public:


    /**
     * @brief needNewThread return true if  the computation of getAction is very expensive
     * @param newStr
     * @param oldStr
     * @return
     */
    bool needNewThread(vector<InternalSymbol> newStr, vector<InternalSymbol> oldStr){
        int offset;
        for(offset=0; offset<static_cast<int>(min(newStr.size(), oldStr.size())); offset++){
            if(newStr[static_cast<unsigned int>(offset)] != oldStr[static_cast<unsigned int>(offset)])
                break;
        }

        int j = static_cast<int>(newStr.size())-1;
        int k = static_cast<int>(oldStr.size())-1;

        for ( ;j >= offset && k >= offset; j-- , k--) {
                if(newStr[static_cast<unsigned int>(j)] != oldStr[static_cast<unsigned int>(k)])
                    break;
        }

        return ((k-offset) * (j-offset)) > maxSize;
    }

    /**
     * @brief getActionsRecursive Recursive function to compute the differences among strings.
     * @param newStr the new string
     * @param oldStr the old string
     * @return the list of internal action to make old string equal to the new one.
     */
    list<InternalAction> getActionsRecursive(vector<InternalSymbol> newStr, vector<InternalSymbol> oldStr){
        //skip same characters
        int offset;
        for(offset=0; offset<static_cast<int>(min(newStr.size(), oldStr.size())); offset++){
            if(newStr[static_cast<unsigned int>(offset)] != oldStr[static_cast<unsigned int>(offset)])
                break;
        }

        int j = static_cast<int>(newStr.size())-1;
        int k = static_cast<int>(oldStr.size())-1;

        for ( ;j >= offset && k >= offset; j-- , k--) {
                if(newStr[static_cast<unsigned int>(j)] != oldStr[static_cast<unsigned int>(k)])
                    break;
        }

        vector<InternalSymbol> str1;
        vector<InternalSymbol> str2;
        for(int b = offset; b<=j; b++){
            str1.push_back(newStr[static_cast<unsigned int>(b)]);
        }

        for(int b = offset; b<=k; b++){
            str2.push_back(oldStr[static_cast<unsigned int>(b)]);
        }
        //END skip same characters

        return getActionsRecursiveFunc(str1, str2, static_cast<unsigned int>(offset));
    }



};

#endif // EDITDISTANCE_H
