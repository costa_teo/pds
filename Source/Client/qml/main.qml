import QtQuick 2.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.0
import QtQuick.Dialogs 1.1
import QtQuick.Window 2.1
import org.qtproject.example 1.0
import QtQuick.Controls.Styles 1.4

ApplicationWindow {
    id: applicationWindow
    visible: true
    width: 800
    height: 600
    minimumWidth: 650
    minimumHeight: 300

    title: "MMMP :  " + document.documentTitle

    Action {
        id: cutAction
        text: "Cut"
        shortcut: StandardKey.Cut
        iconSource: "images/ui/cut.png"
        onTriggered: textArea.cut()
        enabled : !document.readOnly
    }

    Action {
        id: copyAction
        text: "&Copy"
        shortcut: StandardKey.Copy
        iconSource: "images/ui/copy.png"
        onTriggered: textArea.copy()
        enabled : !document.readOnly
    }

    Action {
        id: pasteAction
        text: "&Paste"
        shortcut: StandardKey.Paste
        iconSource: "images/ui/paste.png"
        onTriggered: textArea.paste()
        enabled : !document.readOnly
    }

    Action {
        id: alignLeftAction
        text: "&Left"
        iconSource: "images/ui/align-left.png"
        onTriggered: document.alignment = Qt.AlignLeft
        checkable: true
        checked: document.alignment === Qt.AlignLeft
        enabled : !document.readOnly
    }

    Action {
        id: alignCenterAction
        text: "&Center"
        iconSource: "images/ui/align-center.png"
        iconName: "format-justify-center"
        onTriggered: document.alignment = Qt.AlignHCenter
        checkable: true
        checked: document.alignment === Qt.AlignHCenter
        enabled : !document.readOnly
    }

    Action {
        id: alignRightAction
        text: "&Right"
        iconSource: "images/ui/align-right.png"
        onTriggered: document.alignment = Qt.AlignRight
        checkable: true
        checked: document.alignment === Qt.AlignRight
        enabled : !document.readOnly
    }

    Action {
        id: boldAction
        text: "&Bold"
        shortcut: StandardKey.Bold
        iconSource: "images/ui/bold.png"
        onTriggered: document.bold = !document.bold
        checkable: true
        checked: document.bold
        enabled : !document.readOnly
    }

    Action {
        id: italicAction
        text: "&Italic"
        shortcut: StandardKey.Italic
        iconSource: "images/ui/italic.png"
        onTriggered: document.italic = !document.italic
        checkable: true
        checked: document.italic
        enabled : !document.readOnly
    }

    Action {
        id: underlineAction
        text: "&Underline"
        shortcut: StandardKey.Underline
        iconSource: "images/ui/underline.png"
        onTriggered: document.underline = !document.underline
        checkable: true
        checked: document.underline
        enabled : !document.readOnly
    }

    ColorDialog {
        id: colorDialog
        color: "black"
    }

    Action {
        id: fileServerOpenAction
        shortcut: StandardKey.Open
        iconSource: "images/ui/add-file.png"
        text: "Open File"
        onTriggered: {
            document.serverOpenFile()
        }
        enabled: false
    }

    Action {
        id: fileServerOpenURIAction
        shortcut: "Ctrl+Shift+O"
        iconSource: "images/ui/open-file.png"
        text: "Open URI   "
        onTriggered: {
            document.serverOpenURIFile()
        }
        enabled: false
    }

    Action {
        id: fileShareAction
        shortcut: StandardKey.SaveAs
        iconSource: "images/ui/share.png"
        text: "Share"
        onTriggered: document.serverShareFile()
        enabled: document.isFileOpened
    }

    Action {
        id: fileServerDeleteAction
        shortcut: "Ctrl+R"
        iconSource: "images/ui/trash.png"
        text: "Delete File"
        onTriggered: document.serverDeleteFile()
        enabled: document.isFileOpened
    }

    Action {
        id: fileServerCreateAction
        shortcut: StandardKey.New
        iconSource: "images/ui/new-file.png"
        text: "Create File"
        onTriggered: document.serverCreateFile()
        enabled: false
    }

    Action {
        id: loginAction
        shortcut: "Ctrl+Shift+J"
        iconSource: "images/ui/login.png"
        text: "Login"
        onTriggered: document.loginAction()
        enabled: true
    }

    Action {
        id: logoutAction
        shortcut: "Ctrl+Shift+K"
        iconSource: "images/ui/logout.png"
        text: "Logout"
        onTriggered: document.logoutAction()
        enabled: false
    }

    Action {
        id: createAccount
        shortcut: "Ctrl+Shift+N"
        iconSource: "images/ui/new-user.png"
        text: "Create"
        onTriggered: document.createAccount()
        enabled: true
    }

    Action {
        id: deleteAccount
        shortcut: "Ctrl+Shift+R"
        iconSource: "images/ui/remove-user.png"
        text: "Delete"
        onTriggered: document.deleteAccount()
        enabled: false
    }

    Action {
        id: fileExportPdf
        shortcut: StandardKey.Print
        iconSource: "images/ui/export-pdf.png"
        text: "Export PDF"
        onTriggered: document.exportPdf()
        enabled: document.isFileOpened
    }

    Action {
        id: showCursor
        shortcut: "Ctrl+G"
        text: "&Show Others Cursors"
        iconSource: "images/ui/magic.png"
        onToggled: {
            document.showCursor = !document.showCursor
            textArea.cursorPosition = document.cursorPosition
        }
        checkable: true
        checked: document.showCursor
        enabled: document.isFileOpened
    }

    Action {
        id: showClientColor
        shortcut: "Ctrl+Y"
        text: "&Show Clients Colors"
        iconSource: "images/ui/marker.png"
        onToggled: {
            document.showClientColor = !document.showClientColor
            textArea.cursorPosition = document.cursorPosition
        }
        checkable: true
        checked: document.showClientColor
        enabled: document.isFileOpened
    }

    Action {
        id: modifyAccount
        shortcut: "Ctrl+Shift+M"
        text: "&Modify"
        iconSource: "images/ui/modify-user.png"
        onTriggered: document.serverModifyProfile()
        enabled: false
    }

    menuBar: MenuBar {
        id: menuBar
        Menu {
            title: "&File"
            MenuItem { action: fileServerOpenAction }
            MenuItem { action: fileServerOpenURIAction }
            MenuItem { action: fileServerCreateAction }
            MenuItem { action: fileServerDeleteAction }
            MenuItem { action: fileExportPdf }
            MenuItem { text: "Quit"; onTriggered: Qt.quit(); iconSource: "images/ui/close.png" }
        }
        Menu {
            title: "&Account"
            MenuItem { action: loginAction }
            MenuItem { action: logoutAction }
            MenuItem { action: createAccount }
            MenuItem { action: deleteAccount }
            MenuItem { action: modifyAccount }
        }
        Menu {
            title: "&Edit"
            MenuItem { action: copyAction }
            MenuItem { action: cutAction }
            MenuItem { action: pasteAction }
        }
        Menu {
            title: "&Format"
            MenuItem { action: boldAction }
            MenuItem { action: italicAction }
            MenuItem { action: underlineAction }
            MenuSeparator {}
            MenuItem { action: alignLeftAction }
            MenuItem { action: alignCenterAction }
            MenuItem { action: alignRightAction }
            MenuSeparator {}
            MenuItem {
                id: colorMenu
                text: "&Color ..."
                onTriggered: {
                    colorDialog.color = document.textColor
                    colorDialog.open()
                }
                enabled : !document.readOnly
                iconSource: "images/ui/color.png"
            }
            MenuItem { action: showCursor }
            MenuItem { action: showClientColor }
        }
        Menu {
            title: "&Help"
            MenuItem { text: "Info" ; onTriggered: aboutBox.open(); iconSource: "images/ui/info.png"}
        }
    }

    MessageDialog {
        id: aboutBox
        title: "MMMP Text Editor"
        text: "Questo è un editor di testo condiviso creato con \nil framework QT durante il corso Programmazione di Sistema 2018/2019 \nAutori: Matteo Costa, Matteo Coscia, Matteo Accornero, Paride D'Angelo."
        icon: StandardIcon.Information
    }

    GridLayout {
        id: gridLayout
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 0
        rowSpacing: 0
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.top: parent.top
        rows: 20
        columns: 4

        ToolBar {
            style: ToolBarStyle {
                background: Rectangle {
                            border.color: "#999"
                            gradient: Gradient {
                                GradientStop { position: 0 ; color: "#fff" }
                                GradientStop { position: 1 ; color: "#eee" }
                            }
                        }
            }
            id: customToolbar
            Layout.minimumHeight: 50
            Layout.columnSpan: 4
            Layout.fillWidth: true
            //Layout.topMargin: textToolbar
            RowLayout {
                anchors.fill: parent
                transformOrigin: Item.Center
                spacing: 5
                ToolButton { Layout.bottomMargin: 3; Layout.topMargin: 3; action: fileServerCreateAction }
                ToolButton { Layout.bottomMargin: 3; Layout.topMargin: 3; action: fileServerOpenAction }
                ToolButton { Layout.bottomMargin: 3; Layout.topMargin: 3; action: fileServerOpenURIAction }
                ToolButton { Layout.bottomMargin: 3; Layout.topMargin: 3; action: fileExportPdf }
                ToolButton { Layout.bottomMargin: 3; Layout.topMargin: 3; action: fileShareAction }
                ToolButton { Layout.bottomMargin: 3; Layout.topMargin: 3; action: fileServerDeleteAction }
                Rectangle { width: 2; height: 25; color: "#AAAAAA"  }
                ToolButton { Layout.bottomMargin: 3; Layout.topMargin: 3; action: loginAction }
                ToolButton { Layout.bottomMargin: 3; Layout.topMargin: 3; action: logoutAction }
                Rectangle { width: 2; height: 25; color: "#AAAAAA"  }
                ToolButton { Layout.bottomMargin: 3; Layout.topMargin: 3; action: createAccount }
                ToolButton { Layout.bottomMargin: 3; Layout.topMargin: 3; action : modifyAccount }
                ToolButton { Layout.bottomMargin: 3; Layout.topMargin: 3; action: deleteAccount }
                Rectangle { width: 2; height: 25; color: "#AAAAAA"  }
                ToolButton { Layout.bottomMargin: 3; Layout.topMargin: 3; action: showCursor }
                ToolButton { Layout.bottomMargin: 3; Layout.topMargin: 3; action: showClientColor }
                Item { Layout.fillWidth: true }
                Rectangle {
                    id: ownerClient
                    color: color
                    visible: false
                    radius: 15
                    height: 35
                    width: 35
                    Image {
                        id: ownerClientImg
                        anchors.fill: parent
                        sourceSize.height: 25
                        sourceSize.width: 25
                        source: ""
                    }
                }
                Image { id: clientCountIcon; visible: false; Layout.bottomMargin: 3; Layout.topMargin: 3; source: "images/ui/group.png" }
                Text { id: clientCount; visible: false; text: "1" }
            }
        }

        ToolBar {
            style: ToolBarStyle {
                background: Rectangle {
                            border.color: "#999"
                            gradient: Gradient {
                                GradientStop { position: 0 ; color: "#fff" }
                                GradientStop { position: 1 ; color: "#eee" }
                            }
                        }
            }
            id: textToolbar
            Layout.minimumHeight: 50
            Layout.columnSpan: 4
            Layout.fillWidth: true
            RowLayout {
                anchors.topMargin: 0
                anchors.fill: parent
                spacing: 8
                ToolButton { Layout.bottomMargin: 3; Layout.topMargin: 3; action: copyAction }
                ToolButton { Layout.bottomMargin: 3; Layout.topMargin: 3; action: cutAction }
                ToolButton { Layout.bottomMargin: 3; Layout.topMargin: 3; action: pasteAction }
                Rectangle { width: 2; height: 25; color: "#AAAAAA"  }
                ToolButton { Layout.bottomMargin: 3; Layout.topMargin: 3; action: boldAction }
                ToolButton { Layout.bottomMargin: 3; Layout.topMargin: 3; action: italicAction }
                ToolButton { Layout.bottomMargin: 3; Layout.topMargin: 3; action: underlineAction }
                Rectangle { width: 2; height: 25; color: "#AAAAAA"  }
                ToolButton { Layout.bottomMargin: 3; Layout.topMargin: 3; action: alignLeftAction }
                ToolButton { Layout.bottomMargin: 3; Layout.topMargin: 3; action: alignCenterAction }
                ToolButton { Layout.bottomMargin: 3; Layout.topMargin: 3; action: alignRightAction }
                Rectangle { width: 2; height: 25; color: "#AAAAAA"  }
                ToolButton {
                    id: colorButton
                    property var color : document.textColor
                    Rectangle {
                        id: colorRect
                        anchors.fill: parent
                        anchors.margins: 8
                        color: Qt.darker(document.textColor, colorButton.pressed ? 1.4 : 1)
                        anchors.rightMargin: 10
                        anchors.leftMargin: 10
                        anchors.bottomMargin: 10
                        anchors.topMargin: 10
                        border.width: 1
                        border.color: Qt.darker(colorRect.color, 2)
                    }
                    onClicked: {
                        colorDialog.color = document.textColor
                        colorDialog.open()
                    }
                    enabled : !document.readOnly
                }
                RowLayout {
                    ComboBox {
                        currentIndex: 21
                        id: fontFamilyComboBox
                        implicitWidth: 150
                        model: Qt.fontFamilies()
                        property bool special : false
                        onActivated: {
                            if (special == false || index != 0) {
                                document.fontFamily = textAt(index)
                            }
                        }
                        enabled : !document.readOnly
                    }
                }
                SpinBox {
                    id: fontSizeSpinBox
                    activeFocusOnPress: false
                    implicitWidth: 50
                    value: 14
                    property bool valueGuard: true
                    font.pointSize: 12
                    onValueChanged: if (valueGuard) document.fontSize = value
                    enabled : !document.readOnly
                }
                Item { Layout.fillWidth: true }
            }
        }

        TextArea {
            id: textArea
            Accessible.name: "document"
            frameVisible: true
            Layout.columnSpan: 2
            Layout.fillWidth: true
            Layout.fillHeight: true
            wrapMode: Text.WrapAnywhere
            baseUrl: "qrc:/"
            text: document.text
            Layout.minimumHeight: 200
            Layout.minimumWidth: 300
            font.family: "Arial"
            font.pointSize: 14
            Layout.rowSpan: 18
            textFormat: Qt.RichText
            Component.onCompleted: forceActiveFocus()
            onTextChanged: document.controllerTextChanged()
            onCursorPositionChanged: document.controllerCursorPositionChanged(textArea.cursorPosition)
            antialiasing: true
            readOnly: document.readOnly
        }

        ListModel {
            id: connectedClients
        }

        Component {
            id: clientDelegate

            Rectangle {
                id: clientComponent
                color: bgcolor
                Layout.fillWidth: true
                Layout.leftMargin: 0
                Layout.rightMargin: 0
                radius: 5
                height: 32
                width: parent.width
                Image {
                    anchors.left: parent.left
                    anchors.leftMargin: 20
                    sourceSize.height: 32
                    sourceSize.width: 32
                    source: iconimg
                }
                Text {
                    anchors.centerIn: parent
                    font.pixelSize: 24
                    text: nickname
                }
            }
        }

        ListView {
            id: columnClients
            Layout.maximumWidth: 300
            Layout.minimumWidth: 150
            Layout.preferredWidth: 300
            Layout.columnSpan: 2
            Layout.rowSpan: 18
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.rightMargin: 5
            spacing: 5
            clip: true
            model: connectedClients
            delegate: clientDelegate
            add: Transition {
                NumberAnimation { property: "opacity"; from: 0; to: 1.0; duration: 200 }
                NumberAnimation { property: "scale"; from: 0; to: 1.0; duration: 200 }
            }
            remove: Transition {
                NumberAnimation { property: "opacity"; from: 1.0; to: 0; duration: 200 }
                NumberAnimation { property: "scale"; from: 1.0; to: 0; duration: 200 }
            }
        }

        ToolBar {
            id: debugToolBar
            Layout.preferredHeight: 20
            Layout.rowSpan: 1
            Layout.columnSpan: 4
            Layout.fillWidth: true
            Layout.bottomMargin: 0
            Text {
                id: element
                text: qsTr(document.state)
                clip: false
                anchors.verticalCenterOffset: 0
                anchors.verticalCenter: parent.verticalCenter
                font.pixelSize: 12
            }
        }

        DocumentHandler {
            id: document
            target: textArea
            cursorPosition: textArea.cursorPosition
            selectionStart: textArea.selectionStart
            selectionEnd: textArea.selectionEnd
            textColor: colorDialog.color
            Component.onCompleted: document.fileUrl = ""//"qrc:/example.html"
            onFontSizeChanged: {
                fontSizeSpinBox.valueGuard = false
                fontSizeSpinBox.value = document.fontSize
                fontSizeSpinBox.valueGuard = true
            }
            onFontFamilyChanged: {
                var index = Qt.fontFamilies().indexOf(document.fontFamily)
                if (index === -1) {
                    fontFamilyComboBox.currentIndex = 0
                    fontFamilyComboBox.special = true
                } else {
                    fontFamilyComboBox.currentIndex = index
                    fontFamilyComboBox.special = false
                }
            }
            onError: {
                errorDialog.text = message
                errorDialog.visible = true
            }
            onCursorPositionMatte: {
                if(document.selectionEnd != document.selectionStart) {
                    textArea.select(document.selectionStart, document.selectionEnd)
                } else {
                    textArea.cursorPosition = document.cursorPosition
                }
            }
            onAddConnectedClient: {
                connectedClients.append({"nickname": nickname, "bgcolor": color, "iconimg": icon})
                clientCount.text = parseInt(clientCount.text) + 1;
            }
            onRemoveConnectedClient: {
                connectedClients.remove(pos)
                clientCount.text = parseInt(clientCount.text) - 1;
            }
            onClearConnectedClientList: {
                connectedClients.clear()
                clientCount.text = 1
            }
            onUpdateConnectedClient: {
                connectedClients.remove(pos);
                connectedClients.insert(pos, {"nickname": nickname, "bgcolor": color, "iconimg": icon})
            }
            onShowConnectedClientCount: {
                clientCount.visible = true
                clientCountIcon.visible = true
            }
            onHideConnectedClientCount: {
                clientCount.visible = false
                clientCountIcon.visible = false
            }
            onLoginOwnerClient: {
                /* ownerClient Icon */
                ownerClient.color = color
                ownerClientImg.source = icon
                ownerClient.visible = true
                ownerClientImg.visible = true
                /* ********************* */
                /* customToolbar Actions */
                fileServerCreateAction.enabled = true
                fileServerOpenAction.enabled = true
                fileServerOpenURIAction.enabled = true
                createAccount.enabled = false
                modifyAccount.enabled = true
                deleteAccount.enabled = true
                loginAction.enabled = false
                logoutAction.enabled = true
            }
            onLogoutOwnerClient: {
                /* ownerClient Icon */
                ownerClient.visible = false
                ownerClientImg.visible = false
                /* ********************* */
                /* customToolbar Actions */
                fileServerCreateAction.enabled = false
                fileServerOpenAction.enabled = false
                fileServerOpenURIAction.enabled = false
                loginAction.enabled = true
                logoutAction.enabled = false
                createAccount.enabled = true
                modifyAccount.enabled = false
                deleteAccount.enabled = false
            }
        }
    }
}
