/****************************************************************************
**
** Copyright (C) 2016 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** BSD License Usage
** Alternatively, you may use this file under the terms of the BSD license
** as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

import QtQuick 2.2
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.0
import QtQuick.Dialogs 1.1
import QtQuick.Window 2.1
import org.qtproject.example 1.0

ApplicationWindow {
    id: applicationWindow
    visible: true
    width: 800
    height: 600
    minimumWidth: 400
    minimumHeight: 300

    title: "MMMP :  " + document.documentTitle

    MessageDialog {
        id: aboutBox
        title: "MMMP Text Editor"
        text: "Questo è un editor di testo condiviso creato con \nil framework QT durante il corso Programmazione di Sistema 2018/2019 \nAutori: Matteo Costa, Matteo Coscia, Matteo Accornero, Paride D'Angelo."
        icon: StandardIcon.Information
    }

    Action {
        id: nothing
    }

    Action {
        id: cutAction
        text: "Cut"
        shortcut: "ctrl+x"
        iconSource: "images/editcut.png"
        iconName: "edit-cut"
        onTriggered: textArea.cut()
    }

    Action {
        id: copyAction
        text: "&Copy"
        shortcut: "Ctrl+C"
        iconSource: "images/editcopy.png"
        iconName: "edit-copy"
        onTriggered: textArea.copy()
    }

    Action {
        id: pasteAction
        text: "&Paste"
        shortcut: "ctrl+v"
        iconSource: "images/editpaste.png"
        iconName: "edit-paste"
        onTriggered: textArea.paste()
    }

    Action {
        id: alignLeftAction
        text: "&Left"
        iconSource: "images/textleft.png"
        iconName: "format-justify-left"
        shortcut: "ctrl+l"
        onTriggered: document.alignment = Qt.AlignLeft
        checkable: true
        checked: document.alignment == Qt.AlignLeft
    }
    Action {
        id: alignCenterAction
        text: "&Center"
        iconSource: "images/textcenter.png"
        iconName: "format-justify-center"
        onTriggered: document.alignment = Qt.AlignHCenter
        checkable: true
        checked: document.alignment == Qt.AlignHCenter
    }
    Action {
        id: alignRightAction
        text: "&Right"
        iconSource: "images/textright.png"
        iconName: "format-justify-right"
        onTriggered: document.alignment = Qt.AlignRight
        checkable: true
        checked: document.alignment == Qt.AlignRight
    }
    Action {
        id: alignJustifyAction
        text: "&Justify"
        iconSource: "images/textjustify.png"
        iconName: "format-justify-fill"
        onTriggered: document.alignment = Qt.AlignJustify
        checkable: true
        checked: document.alignment == Qt.AlignJustify
    }

    Action {
        id: boldAction
        text: "&Bold"
        iconSource: "images/textbold.png"
        iconName: "format-text-bold"
        onTriggered: document.bold = !document.bold
        checkable: true
        checked: document.bold
    }

    Action {
        id: italicAction
        text: "&Italic"
        iconSource: "images/textitalic.png"
        iconName: "format-text-italic"
        onTriggered: document.italic = !document.italic
        checkable: true
        checked: document.italic
    }
    Action {
        id: underlineAction
        text: "&Underline"
        iconSource: "images/textunder.png"
        iconName: "format-text-underline"
        onTriggered: document.underline = !document.underline
        checkable: true
        checked: document.underline
    }

    Action {
        id: showCursor
        text: "&Show Cursor"
        iconSource: "images/showcursors.png"
        iconName: "format-text-bold"
        onToggled: {
            document.showCursor = !document.showCursor
            textArea.cursorPosition = document.cursorPosition
        }
        checkable: true
        checked: document.showCursor
    }

    Action {
        id: showClientColor
        text: "&Show Client Color"
        iconSource: "images/brush.png"
        iconName: "format-text-bold"
        onToggled: {
            document.showClientColor = !document.showClientColor
            textArea.readOnly = document.showClientColor
            textArea.cursorPosition = document.cursorPosition
        }
        checkable: true
        checked: document.showClientColor
    }

    Action {
        id: modifyAccount
        text: "&Modify Account"
        iconSource: "images/modifyprofile.png"
        iconName: "format-text-bold"
        onTriggered: document.serverModifyProfile()
    }




    FileDialog {
        id: fileDialog
        nameFilters: ["Text files (*.txt)", "HTML files (*.html, *.htm)"]
        onAccepted: {
            if (fileDialog.selectExisting)
                document.fileUrl = fileUrl
            else
                document.saveAs(fileUrl, selectedNameFilter)
        }
    }

    ColorDialog {
        id: colorDialog
        color: "black"
    }

    Action {
        id: fileOpenAction
        text: "Open file"
        iconSource: "images/fileopen.png"
        iconName: "document-open"
        onTriggered: {

            fileDialog.selectExisting = true
            fileDialog.open()
        }
    }
    Action {
        id: fileServerOpenAction
        iconSource: "images/fileopen.png"
        text: "Open File"
        onTriggered: {
            document.serverOpenFile()
        }
    }

    Action {
        id: fileServerOpenURIAction
        iconSource: "images/openfileuri.png"
        text: "Open File from URI"
        onTriggered: {
            document.serverOpenURIFile()
        }
    }

    Action {
        id: fileShareAction
        iconSource: "images/share.png"
        text: "Share"
        onTriggered: {
            document.serverShareFile()
        }
    }


    Action {
        id: fileServerDeleteAction
        iconSource: "images/bin.png"
        iconName: "delete-file"
        text: "Delete File"
        onTriggered: {
            document.serverDeleteFile()
        }
    }

    Action {
        id: fileServerCreateAction
        iconSource: "images/newfile.png"
        iconName: "new-file"
        text: "Create File"
        onTriggered: {
            document.serverCreateFile()
        }
    }

    Action {
        id: loginAction
        iconSource: "images/login.png"
        text: "Login"
        onTriggered: {
            document.loginAction()
        }
    }

    Action {
        id: logoutAction
        iconSource: "images/logout.png"
        text: "Logout"
        onTriggered: {
            document.logoutAction()
        }
    }


    Action {
        id: creatAccount
        iconSource: "images/createaccount.png"
        text: "Create Account"
        onTriggered: {
            document.createAccount()
        }
    }


    Action {
        id: deleteAccount
        iconSource: "images/deleteaccount.png"
        text: "Delete Account"
        onTriggered: {
            document.deleteAccount()
        }
    }



    /*Action {
        id: fileSaveAsAction
        iconSource: "images/filesave.png"
        iconName: "document-save"
        text: "Save As…"
        onTriggered: {
            fileDialog.selectExisting = false
            fileDialog.open()
        }
    }*/

    Action {
        id: fileExportPdf
        iconSource: "images/fileexportpdf.png"
        text: "Export PDF"
        onTriggered: {
            document.exportPdf()
        }
    }

    menuBar: MenuBar {
        Menu {
            title: "&File"
            MenuItem { action: fileServerOpenAction }
            MenuItem { action: fileServerOpenURIAction }
            MenuItem { action: fileServerCreateAction }
            MenuItem { action: fileServerDeleteAction }
            MenuItem { action: fileExportPdf }
            MenuItem { text: "Quit"; onTriggered: Qt.quit() }
        }

        Menu {
            title: "&Account"
            MenuItem { action: loginAction }
            MenuItem { action: logoutAction }
            MenuItem { action: creatAccount }
            MenuItem { action: deleteAccount }
        }

        Menu {
            title: "&Edit"
            MenuItem { action: copyAction }
            MenuItem { action: cutAction }
            MenuItem { action: pasteAction }
        }
        Menu {
            title: "&Format"
            MenuItem { action: boldAction }
            MenuItem { action: italicAction }
            MenuItem { action: underlineAction }

            MenuSeparator {}

            MenuItem { action: alignLeftAction }
            MenuItem { action: alignCenterAction }
            MenuItem { action: alignRightAction }
            //MenuItem { action: alignJustifyAction }
            MenuSeparator {}
            MenuItem {
                text: "&Color ..."
                onTriggered: {
                    colorDialog.color = document.textColor
                    colorDialog.open()
                }
            }
            MenuItem { action: showCursor }
            MenuItem { action: showClientColor }

        }
        Menu {
            title: "&Help"
            MenuItem { text: "Info" ; onTriggered: aboutBox.open() }
        }
    }

    ToolBar {
        id: mainToolBar
        width: parent.width
        RowLayout {
            anchors.fill: parent
            spacing: 10

            ToolButton { action: copyAction }
            ToolButton { action: cutAction }
            ToolButton { action: pasteAction }

            Rectangle { width: 2; height: 25; color: "#AAAAAA"  }

            ToolButton { action: boldAction }
            ToolButton { action: italicAction }
            ToolButton { action: underlineAction }

            Rectangle { width: 2; height: 25; color: "#AAAAAA"  }

            ToolButton { action: alignLeftAction }
            ToolButton { action: alignCenterAction }
            ToolButton { action: alignRightAction }
            //ToolButton { action: alignJustifyAction }

            Rectangle { width: 2; height: 25; color: "#AAAAAA"  }

            ToolButton {
                id: colorButton
                property var color : document.textColor
                Rectangle {
                    id: colorRect
                    anchors.fill: parent
                    anchors.margins: 8
                    color: Qt.darker(document.textColor, colorButton.pressed ? 1.4 : 1)
                    border.width: 1
                    border.color: Qt.darker(colorRect.color, 2)
                }
                onClicked: {
                    colorDialog.color = document.textColor
                    colorDialog.open()
                }
            }


            Rectangle { width: 2; height: 25; color: "#AAAAAA" }
            ToolButton { action: showCursor }
            ToolButton { action: showClientColor }
            ToolButton { action : modifyAccount }
            Rectangle { width: 2; height: 25; color: "#AAAAAA" }

            Item { Layout.fillWidth: true }


        }
    }

    ToolBar {
        id: toolBar2
        width: parent.width
        anchors.top: mainToolBar.bottom
        RowLayout {
            anchors.fill: parent
            spacing: 8

            ToolButton { action: fileServerCreateAction }
            ToolButton { action: fileServerOpenAction }
            ToolButton { action: fileServerOpenURIAction }
            ToolButton { action: fileServerDeleteAction }
            ToolButton { action: fileShareAction }
            ToolButton { action: fileExportPdf }

            Rectangle { width: 2; height: 25; color: "#AAAAAA"  }

            ToolButton { action: loginAction }
            ToolButton { action: logoutAction }
            ToolButton { action: creatAccount }
            ToolButton { action: deleteAccount }

            Rectangle { width: 2; height: 25; color: "#AAAAAA"  }

            RowLayout {
                //anchors.fill: parent
                ComboBox {
                    id: fontFamilyComboBox
                    implicitWidth: 150
                    model: Qt.fontFamilies()
                    property bool special : false
                    onActivated: {
                        if (special == false || index != 0) {
                            document.fontFamily = textAt(index)
                        }
                    }
                }
                SpinBox {
                    id: fontSizeSpinBox
                    activeFocusOnPress: false
                    implicitWidth: 50
                    value: 0
                    property bool valueGuard: true
                    onValueChanged: if (valueGuard) document.fontSize = value
                }
            }

            Item { Layout.fillWidth: true }
        }
    }



    TextArea {
        Accessible.name: "document"
        id: textArea
        frameVisible: false
        width: parent.width
        anchors.top: toolBar2.bottom
        anchors.bottom: secondaryToolBar.top
        wrapMode:  WrapAtWordBoundaryOrAnywhere
        baseUrl: "qrc:/"
        text: document.text
        textFormat: Qt.RichText
        Component.onCompleted: forceActiveFocus()
    }

    ToolBar {
        id: secondaryToolBar
        width: parent.width
        height: 20
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 0

        Text {
            id: element
            x: 0
            text: qsTr(document.state)
            anchors.top: parent.top
            anchors.topMargin: 2
            Layout.alignment: Qt.AlignLeft | Qt.AlignTop
            Layout.preferredHeight: -1
            Layout.preferredWidth: -1
            Layout.topMargin: 0
            font.pixelSize: 12
        }

    }

    MessageDialog {
        id: errorDialog
    }

    DocumentHandler {
        id: document
        target: textArea
        cursorPosition: textArea.cursorPosition
        selectionStart: textArea.selectionStart
        selectionEnd: textArea.selectionEnd
        textColor: colorDialog.color
        Component.onCompleted: document.fileUrl = ""//"qrc:/example.html"
        onFontSizeChanged: {
            fontSizeSpinBox.valueGuard = false
            fontSizeSpinBox.value = document.fontSize
            fontSizeSpinBox.valueGuard = true
        }
        onFontFamilyChanged: {
            var index = Qt.fontFamilies().indexOf(document.fontFamily)
            if (index === -1) {
                fontFamilyComboBox.currentIndex = 0
                fontFamilyComboBox.special = true
            } else {
                fontFamilyComboBox.currentIndex = index
                fontFamilyComboBox.special = false
            }
        }
        onError: {
            errorDialog.text = message
            errorDialog.visible = true
        }

        onCursorPositionMatte: {
            textArea.cursorPosition = document.cursorPosition
        }
    }

}

