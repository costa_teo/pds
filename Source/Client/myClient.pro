QT += widgets
QT += printsupport
QT += qml quick

TARGET = texteditor
!no_desktop: QT += widgets

include(shared/shared.pri)

HEADERS       += \
    $$PWD/../Library/Action/action.h \
    $$PWD/../Library/Message/errormessage.h \
    $$PWD/../Library/Message/listdocumentmessage.h \
    $$PWD/../Library/Message/managefilemessage.h \
    $$PWD/../Library/Message/accountmessage.h \
    $$PWD/../Library/Message/activeclientmessage.h \
    $$PWD/../Library/Message/message.h \
    $$PWD/../Library/Client/client.h \
	$$PWD/../Library/Model/model.h \
    $$PWD/../QT_Library/LoginDialog/logindialog.h \
    $$PWD/../QT_Library/ProfileDialog/profiledialog.h \
    $$PWD/../QT_Library/InputTextDialog/inputtextdialog.h \
    $$PWD/../QT_Library/MessageBox/messagebox.h \
    $$PWD/../QT_Library/ProfileDialog/imageswindow.h \
    $$PWD/../QT_Library/ProfileDialog/clickableLabel.h \
    $$PWD/../QT_Library/CreateDialog/createdialog.h \
    $$PWD/src/documenthandler.h \
    $$PWD/src/PDFConverter/pdfconverter.h \
    $$PWD/src/ConnectionInterface/connectioninterface.h \
    $$PWD/src/Controller/controller.h \
    $$PWD/src/EditDistance/editdistance.h \
    ../Library/Message/editmessage.h \
    ../Library/Message/infomessage.h \
    ../Library/Symbol/symbol.h \
    ../Library/FracPos/fracpos.h \
    ../Library/Constants/constants.h \
    ../Library/library.h

SOURCES       += \
                $$PWD/src/ConnectionInterface/connectioninterface.cpp \
                $$PWD/src/Controller/controller.cpp \
                $$PWD/src/documenthandler.cpp \
                $$PWD/src/main.cpp \
                ../Library/Symbol/symbol.cpp \

OTHER_FILES += \
    qml/main.qml \
    qml/ToolBarSeparator.qml

RESOURCES += \
    resources.qrc

target.path = ./tmp
INSTALLS += target


RC_ICONS = qml/images/myIco.ico
