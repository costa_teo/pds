#ifndef FINDDIALOG_H
#define FINDDIALOG_H

#include <QDialog>
#include <QtGui>
#include <QLabel>
#include <QLineEdit>
#include <QCheckBox>
#include <QPushButton>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include "../../Library/Constants/constants.h"

class QCheckBox;
class QLabel;
class QLineEdit;
class QPushButton;

class LoginDialog : public QDialog
{
    Q_OBJECT
public:

LoginDialog(QWidget *parent = nullptr)
   : QDialog(parent)
{
    this->setFixedSize(280, 170);
    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
    label = new QLabel(tr("Insert credentials:"));
    label_user = new QLabel(COMMON_MESSAGES::L_USERNAME);
    label_pass = new QLabel(COMMON_MESSAGES::L_PASSWORD);

    lineEdit_user = new QLineEdit(this);
    lineEdit_pass = new QLineEdit(this);

    lineEdit_user->setEchoMode(QLineEdit::Normal);
    lineEdit_pass->setEchoMode(QLineEdit::Password);

    lineEdit_user->setMaxLength(CONSTANTS::MAX_USER_INPUT_LENGHT);
    lineEdit_pass->setMaxLength(CONSTANTS::MAX_USER_INPUT_LENGHT);

    loginButton = new QPushButton(COMMON_MESSAGES::B_LOGIN);
    loginButton->setDefault(true);
    createButton = new QPushButton(COMMON_MESSAGES::B_CREATE);

    QHBoxLayout *firstRow = new QHBoxLayout;
    firstRow->addWidget(label);

    QHBoxLayout *secondRow = new QHBoxLayout;
    secondRow->addWidget(label_user);
    secondRow->addWidget(lineEdit_user);

    QHBoxLayout *thirdRow = new QHBoxLayout;
    thirdRow->addWidget(label_pass);
    thirdRow->addWidget(lineEdit_pass);

    QHBoxLayout *fourRow = new QHBoxLayout;
    fourRow->addWidget(loginButton);
    fourRow->addWidget(createButton);

    QVBoxLayout *column = new QVBoxLayout(this);
    column->addLayout(firstRow);
    column->addLayout(secondRow);
    column->addLayout(thirdRow);
    column->addLayout(fourRow);

    setWindowTitle(COMMON_MESSAGES::D_LOGIN_TITLE);

    connect(loginButton, &QPushButton::clicked, [&](){
        valid = true;
        this->close();
    });
    connect(createButton, &QPushButton::clicked, [&](){
        create = true;
        this->close();
    });
}

QString getUser(){
    return lineEdit_user->text();
}

QString getPass(){
    return lineEdit_pass->text();
}

bool isValid(){
    return valid;
}
bool isCreate(){
    return create;
}

private:
    bool valid = false;
    bool create = false;
    QLabel *label;
    QLabel *label_pass;
    QLabel *label_user;
    QLineEdit *lineEdit_user;
    QLineEdit *lineEdit_pass;
    QPushButton *loginButton;
    QPushButton * createButton;
};

#endif // FINDDIALOG_H
