#ifndef INPUTTEXTDIALOG_H
#define INPUTTEXTDIALOG_H

#include <QDialog>
#include <QLineEdit>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QPushButton>
#include <QLabel>
#include "../../Library/Constants/constants.h"

class InputTextDialog : public QDialog
{
    Q_OBJECT
public:

    InputTextDialog(QString title, QString message, QString placeholder, QWidget *parent = nullptr)
       : QDialog(parent)
	{
        setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
        QGridLayout *layout = new QGridLayout(this);
		this->setWindowTitle(title);

        layout->addWidget(new QLabel(message, this), 0, 0, 1, 1);

        lineedit = new QLineEdit(this);
        lineedit->setMaxLength(CONSTANTS::MAX_USER_INPUT_LENGHT);
        lineedit->setText(placeholder);
        layout->addWidget(lineedit, 1, 0, 1, 2);
        okButton = new QPushButton(COMMON_MESSAGES::B_OK, this);
        cancelButton = new QPushButton(COMMON_MESSAGES::B_CANCEL, this);
        layout->addWidget(okButton, 2, 0, 1, 1);
        layout->addWidget(cancelButton, 2, 1, 1, 1);

        connect(cancelButton, &QPushButton::clicked, this, &InputTextDialog::closeWindow);
        connect(okButton, &QPushButton::clicked, this, &InputTextDialog::acceptOk);
    }
    QString getText(){
        return lineedit->text();
    }
    bool getOk(){
        return ok;
    }
    void closeWindow(){
        ok = false;
        this->close();
    }
    void acceptOk(){
        ok = true;
        this->close();
    }
private:
    bool ok = false;
    QLineEdit *lineedit;
    QLabel *l;
    QPushButton *okButton;
    QPushButton *cancelButton;
};

#endif //INPUTTEXTDIALOG_H
