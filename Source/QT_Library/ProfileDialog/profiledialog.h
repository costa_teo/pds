#ifndef PROFILEDIALOG_H
#define PROFILEDIALOG_H

#include "imageswindow.h"

class ProfileDialog : public QDialog
{
    Q_OBJECT
public:

ProfileDialog(QWidget *parent = nullptr, uint iconNumber = 0)
   : QDialog(parent)
{
    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
    QGridLayout *layout = new QGridLayout(this);
    this->setFixedSize(450, 450);
    QGroupBox *box = new QGroupBox(COMMON_MESSAGES::L_PROFILE, this);
    QGridLayout *boxLayout = new QGridLayout(box);

    label_nick = new QLabel(COMMON_MESSAGES::L_CHANGE_NICKNAME, box);
    label_pass = new QLabel(COMMON_MESSAGES::L_CHANGE_PASSWORD, box);

    lineEdit_nick = new QLineEdit(box);
    lineEdit_pass = new QLineEdit(box);
    lineEdit_pass->setEchoMode(QLineEdit::Password);

    lineEdit_nick->setMaxLength(CONSTANTS::MAX_USER_INPUT_LENGHT);
    lineEdit_pass->setMaxLength(CONSTANTS::MAX_USER_INPUT_LENGHT);

    lineEdit_nick->setText("");
    lineEdit_pass->setText("");

    this->x = iconNumber/10;
    this->y = iconNumber%10;

    if(iconNumber > 80){
        iconNumber = 0;
        x = 0;
        y= 0;
    }
    pix.load(ACCOUNT_ICON[x*10+y]);
    image = new QLabel(box);
    image->setPixmap(pix);
    confButton = new QPushButton(COMMON_MESSAGES::B_OK, box);
    annulButton = new QPushButton(COMMON_MESSAGES::B_CANCEL, box);
    changeButton = new QPushButton(COMMON_MESSAGES::B_CHANGE_ICON, box);

    layout->addWidget(box, 0, 0 , 1, 1);
    boxLayout->addWidget(image, 0, 0, 1, 1);
    boxLayout->addWidget(changeButton, 0, 1, 1, 1);
    boxLayout->addWidget(label_nick, 2, 0, 1, 1);
    boxLayout->addWidget(lineEdit_nick, 2, 1, 1, 1);
    boxLayout->addWidget(label_pass, 3, 0, 1, 1);
    boxLayout->addWidget(lineEdit_pass, 3, 1, 1, 1);
    boxLayout->addWidget(confButton, 4, 0, 1, 1);
    boxLayout->addWidget(annulButton, 4, 1, 1, 1);

    setWindowTitle(COMMON_MESSAGES::L_PROFILE);

    connect(annulButton, &QPushButton::clicked, [&](){
        this->close();
    });
    connect(changeButton, &QPushButton::clicked, [&](){
        ImagesWindow iw;
        iw.exec();

        if(iw.isValid()){
            QString path = ACCOUNT_ICON[iw.getX()*10+iw.getY()];
            pix.load(path);
            image->setPixmap(pix);
            this->x = iw.getX();
            this->y = iw.getY();
        }
    });
    connect(confButton, &QPushButton::clicked, [&](){
        valid = true;
        this->close();
    });

}
bool isValid() {
    return valid;
}
QString getNickname(){
    return lineEdit_nick->text();
}
QString getPassword(){
    return lineEdit_pass->text();
}
void setNickname(QString nickname){
    this->lineEdit_nick->setText(nickname);
}
uint getX(){
    return x;
}
uint getY(){
    return y;
}
uint getIconNumber(){
    return x*10+y;
}
private:
    bool valid = false;
    QLineEdit *lineEdit_nick;
    QLineEdit *lineEdit_pass;
    QLabel *label_nick;
    QLabel *label_pass;
    QPushButton *confButton;
    QPushButton *annulButton;
    QPushButton *changeButton;
    QPixmap pix;
    uint x;
    uint y;
    QLabel *image;
};

#endif // PROFILEDIALOG_H
