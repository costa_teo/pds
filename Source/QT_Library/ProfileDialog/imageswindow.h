#ifndef IMAGESWINDOW_H
#define IMAGESWINDOW_H

#include <QDialog>
#include <QtGui>
#include <QLineEdit>
#include <QCheckBox>
#include <QPushButton>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QGroupBox>
#include <QGraphicsPixmapItem>
#include <QMainWindow>
#include <QScrollBar>
#include <QScrollArea>
#include "../../Library/Constants/constants.h"
#include "./clickableLabel.h"

class ImagesWindow : public QDialog
{
    Q_OBJECT
public:

ImagesWindow(QWidget *parent = nullptr)
   : QDialog(parent)
{
    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
    this->setWindowTitle(COMMON_MESSAGES::D_IMAGES_TITLE);
    this->setFixedSize(1200, 500);
    QGridLayout *layout = new QGridLayout(this);

    box = new QGroupBox(COMMON_MESSAGES::D_IMAGES_TITLE, this);
    box->setFixedSize(1150, 1100);
    layout->addWidget(box, 0, 0, 1, 1);
    QGridLayout *ly = new QGridLayout(box);

    for(uint i=0;i<11;i++){
        for(uint j=0;j<10;j++){
            if(i*10+j>80)
                break;
            ClickableLabel *image = new ClickableLabel(box, i, j);
            QString s = ACCOUNT_ICON[i*10+j];
            connect(image, SIGNAL(clicked(uint, uint)), this, SLOT(on_label_clicked(uint, uint)));
            image->setPixmap(QPixmap(s));
            ly->addWidget(image, static_cast<int>(i), static_cast<int>(j), 1, 1);
        }
    }

}
protected:
void wheelEvent(QWheelEvent *e){
    static int delta = 0;
    delta += e->delta();
    if(delta>590){
        delta = 590;
    }else if(delta<0){
        delta=0;
    }
    box->move(15, -delta);
    e->accept();
}
public slots:
void scroll(int value){
   box->move(25, -value*590/36);
}
void on_label_clicked(uint x, uint y){
    valid = true;
    this->x = x;
    this->y = y;
    this->close();
}
bool isValid(){
    return valid;
}
uint getX(){
    return this->x;
}
uint getY(){
    return this->y;
}
private:
    bool valid = false;
    uint x = 0;
    uint y = 0;
    QGroupBox *box;
};

	
#endif // IMAGESWINDOW_H
