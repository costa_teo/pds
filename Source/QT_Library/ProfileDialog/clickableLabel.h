#ifndef CLICKABLELABEL_H
#define CLICKABLELABEL_H

#include <QLabel>
#include <QWidget>

class ClickableLabel : public QLabel { 
    Q_OBJECT 
    uint x;
    uint y;
public:
    explicit ClickableLabel(QWidget* parent = nullptr, uint x = 0, uint y = 0) : QLabel(parent){
        this->x = x;
        this->y = y;
    }

signals:
    void clicked(uint, uint);

protected:
    void mousePressEvent(QMouseEvent*) {
        emit clicked(x, y);
    }
};

#endif // CLICKABLELABEL_H
