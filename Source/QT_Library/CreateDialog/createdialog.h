#ifndef CREATEDIALOG_H
#define CREATEDIALOG_H

#include "../ProfileDialog/imageswindow.h"
#include <QMessageBox>

class CreateAccDialog : public QDialog {
    Q_OBJECT
public:
    CreateAccDialog (QWidget *parent = nullptr) : QDialog(parent) {

        setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);

        this->setFixedSize(300, 300);

        label_user = new QLabel(COMMON_MESSAGES::L_USERNAME);
        label_pass = new QLabel(COMMON_MESSAGES::L_PASSWORD);
        label_nick = new QLabel(COMMON_MESSAGES::L_NICKNAME);
        lineEdit_nick = new QLineEdit();
        lineEdit_pass = new QLineEdit();
        lineEdit_pass->setEchoMode(QLineEdit::Password);
        lineEdit_user = new QLineEdit();

        lineEdit_user->setMaxLength(CONSTANTS::MAX_USER_INPUT_LENGHT);
        lineEdit_nick->setMaxLength(CONSTANTS::MAX_USER_INPUT_LENGHT);
        lineEdit_pass->setMaxLength(CONSTANTS::MAX_USER_INPUT_LENGHT);

        pix.load(ACCOUNT_ICON[0]);
        image = new QLabel();
        image->setPixmap(pix);

        confButton = new QPushButton(COMMON_MESSAGES::B_CREATE);
        annulButton = new QPushButton(COMMON_MESSAGES::B_CANCEL);
        changeButton = new QPushButton(COMMON_MESSAGES::B_SELECT_ICON);

        QHBoxLayout *zeroRow = new QHBoxLayout;
        zeroRow->addWidget(changeButton);
        zeroRow->addWidget(image);


        QHBoxLayout *firstRow = new QHBoxLayout;
        firstRow->addWidget(label_user);
        firstRow->addWidget(lineEdit_user);

        QHBoxLayout *secondRow = new QHBoxLayout;
        secondRow->addWidget(label_pass);
        secondRow->addWidget(lineEdit_pass);

        QHBoxLayout *thirdRow = new QHBoxLayout;
        thirdRow->addWidget(label_nick);
        thirdRow->addWidget(lineEdit_nick);

        QHBoxLayout *fourRow = new QHBoxLayout;
        fourRow->addWidget(confButton);
        fourRow->addWidget(annulButton);


        QVBoxLayout *column = new QVBoxLayout(this);
        column->addLayout(zeroRow);
        column->addLayout(firstRow);
        column->addLayout(secondRow);
        column->addLayout(thirdRow);
        column->addLayout(fourRow);


        setWindowTitle(COMMON_MESSAGES::D_CREATE_ACCOUNT_TITLE);

        lineEdit_user->setFocus();

        connect(changeButton, &QPushButton::clicked, [&](){
            ImagesWindow iw;
            iw.exec();

            if(iw.isValid()){
                QString path = ACCOUNT_ICON[iw.getX()*10+iw.getY()];
                pix.load(path);
                image->setPixmap(pix);
                this->x = iw.getX();
                this->y = iw.getY();
            }
        });
        connect(annulButton, &QPushButton::clicked, [&](){
            this->close();
        });
        connect(confButton, &QPushButton::clicked, [&](){
            if(lineEdit_nick->text() != "" && lineEdit_pass->text() != "" && lineEdit_user->text() != ""){
                valid = true;
                this->close();
            }
            else{
                QMessageBox msgBox;
                msgBox.setText(COMMON_MESSAGES::E_MISSING_CREDENTIALS);
                msgBox.exec();
            }

        });
}

uint getIconNumber(){
    return x*10+y;
}
QString getUsername(){
    return lineEdit_user->text();
}
QString getPassword(){
    return lineEdit_pass->text();
}
QString getNickname(){
    return lineEdit_nick->text();
}
bool isValid(){
    return valid;
}
private:
    bool valid = false;
    QLineEdit *lineEdit_user;
    QLineEdit *lineEdit_nick;
    QLineEdit *lineEdit_pass;
    QLabel *label_nick;
    QLabel *label_pass;
    QLabel *label_user;
    QPushButton *confButton;
    QPushButton *annulButton;
    QPushButton *changeButton;
    QPixmap pix;
    uint x=0;
    uint y=0;
    QLabel *image;


};
#endif //CREATEDIALOG_H
