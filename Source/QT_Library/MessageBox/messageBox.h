#ifndef MESSAGEBOX_H
#define MESSAGEBOX_H

#include <QMessageBox>
#include <QObject>
#include <QGridLayout>
#include <QLabel>
#include <QPushButton>
#include "../../Library/Constants/constants.h"

class CMessageBox : public QDialog
{
    Q_OBJECT

public:
CMessageBox(QWidget *parent = nullptr)
   : QDialog(parent, Qt::MSWindowsFixedSizeDialogHint | Qt::WindowTitleHint)
{
    this->setWindowTitle(COMMON_MESSAGES::D_CONNECTION_ERROR_TITLE);
    QGridLayout *layout = new QGridLayout(this);
    layout->addWidget(new QLabel(COMMON_MESSAGES::D_CONNECTION_ERROR_MESSAGE, this), 0, 0, 1, 2);
    layout->addWidget(new QLabel(COMMON_MESSAGES::Q_RETRY, this), 1, 0, 1, 2);
    retryButton = new QPushButton(COMMON_MESSAGES::B_RETRY, this);
    layout->addWidget(retryButton, 2, 0, 1, 1);
    noButton = new QPushButton("No", this);
    layout->addWidget(noButton, 2, 1, 1, 1);

    connect(retryButton, &QPushButton::clicked, [&](){
        ret = QMessageBox::Retry;
        this->close();
    });
    connect(noButton, &QPushButton::clicked, [&](){
        ret = QMessageBox::No;
        this->close();
    });
}
QMessageBox::StandardButton getRet(){
    return ret;
}
private:
QMessageBox::StandardButton ret;
QPushButton *retryButton;
QPushButton *noButton;
};

#endif //MESSAGEBOX_H
