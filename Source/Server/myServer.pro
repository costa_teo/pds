QT           += network widgets sql

HEADERS = \
    $$PWD/../Library/Action/action.h \
    $$PWD/../Library/Message/errormessage.h \
    $$PWD/../Library/Message/listdocumentmessage.h \
    $$PWD/../Library/Message/managefilemessage.h \
    $$PWD/../Library/Message/accountmessage.h \
    $$PWD/../Library/Message/message.h \
    $$PWD/../Library/Message/infomessage.h \
    $$PWD/../Library/FracPos/fracpos.h \
    $$PWD/../Library/Symbol/symbol.h \
    $$PWD/../Library/Model/model.h \
    $$PWD/src/connectioninterface.h \
    $$PWD/src/document.h \
    $$PWD/src/documenthandler.h \
    $$PWD/src/connectionmanager.h \
    $$PWD/src/safequeue.h \
    $$PWD/../Library/Message/activeclientmessage.h \
    $$PWD/../Library/Client/client.h \
    $$PWD/src/custom_sql_exceptions.h \
    $$PWD/src/dbinterface.h \
    $$PWD/src/securitymanager.h \
    ../Library/Constants/constants.h \
    ../Library/Message/editmessage.h \
    ../Library/library.h
SOURCES = \
    $$PWD/src/documenthandler.cpp \
    $$PWD/src/connectioninterface.cpp \
    $$PWD/src/document.cpp \
    $$PWD/src/main.cpp \
    $$PWD/src/connectionmanager.cpp \
    $$PWD/src/safequeue.cpp \
    $$PWD/../Library/Symbol/symbol.cpp \
    $$PWD/src/dbinterface.cpp \
    $$PWD/src/securitymanager.cpp \
    $$PWD/src/custom_sql_exceptions.cpp

TARGET = server
target.path = ./tmp
INSTALLS += target

CONFIG += c++17

QMAKE_MACOSX_DEPLOYMENT_TARGET = 10.14
