#ifndef SECURITYMANAGER_H
#define SECURITYMANAGER_H

#include "connectioninterface.h"

class SecurityManager {
private:
    typedef struct s_client {
        ConnectionInterface *ci;
        Client client;
    } clientInfo;

    static std::vector<clientInfo> clients;
    static int lastId;
public:
    SecurityManager() = delete;
    static int isAlreadyLogged(ConnectionInterface *);
    static int isAlreadyLoggedWithUsername(QString);
    static int doLogin(ConnectionInterface *, QString, QString, uint, uint);
    static void doLogout(ConnectionInterface *);
    static int findClientIdByCi(ConnectionInterface *);
    static ConnectionInterface *findConnectionInterfaceByClientId(int);
    static Client findClientByClientId(int);
    static bool checkMessageOrigin(ConnectionInterface *, int);
    static void updateClientInfo(int, QString, uint);
    static void resetClientPosition(int);
};

#endif // SECURITYMANAGER_H
