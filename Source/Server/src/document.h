
#ifndef DOCUMENT_H
#define DOCUMENT_H

#include <iostream>
#include <vector>
#include <algorithm>

#include "../Library/Model/model.h"
#include "../Library/Message/editmessage.h"
#include "../Library/Symbol/symbol.h"

class Document
{
private:
    Model model;
    int documentId;
    int opened = 0;
    std::vector<ExternalAction> lastActions;
public:
    Document() = delete;
    Document(int);

    void addClient(Client &);
    void removeClient(int);
    int containsClient(int);
    int isAlreadyOpened();
    void closeDocument();
    void loadModelFromString(const vector<InternalSymbol>&, std::vector<ExternalAction>&);
    void modifyModel(std::vector<ExternalAction> &);
    void updateCursor(int, FracPos);

    QString getDocumentName();
    void setDocumentName(QString);
    QString getURI();
    void setURI(QString);
    const std::vector<int> getActiveClientsId();
    const std::vector<Client> getActiveClients();
    std::vector<ExternalAction> getLastActions();
    vector<InternalSymbol> getInternalSymbols();
};

#endif // DOCUMENT_H
