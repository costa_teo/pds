#ifndef CONNECTIONMANAGER_H
#define CONNECTIONMANAGER_H

#include "safequeue.h"
#include "securitymanager.h"

static SafeQueue bigQueue;

class ConnectionManager : public QTcpServer {
    Q_OBJECT
public:
    explicit ConnectionManager(QObject* = nullptr);
    void serveClients();
    void sendMessageToClients(MessageWrapper &, vector<int> &);
    MessageWrapper getMessageFromClients();
signals:
    void sendMessage(AccountMessage);
    void sendMessage(ActiveClientMessage);
    void sendMessage(ErrorMessage);
    void sendMessage(InfoMessage);
    void sendMessage(ListDocumentMessage);
    void sendMessage(ManageFileMessage);
    void sendMessage(EditMessage);
public slots:
    void threadExec();
    void sendResponseMessage(MessageWrapper &);
};

#endif // CONNECTIONMANAGER_H
