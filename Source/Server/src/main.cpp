#include <QApplication>

#include "documenthandler.h"

int main(int argc, char** argv)
{
    QCoreApplication app(argc, argv);

    freopen( "serverLogs.txt", "w", stderr );

    std::cerr << "BEGIN\n";
    DocumentHandler* handler;

    try {
        handler = new DocumentHandler();

        std::cerr << "Handler created\n";

        while(true){
            std::cerr << "\nHandling new request...\n";
            handler->handleRequest();
        }
    } catch (...) {
        std::cerr << "MAIN - CRITICAL EXCEPTION\n";
        //Too late to do anything
    }

    delete handler;

    return 0;
}
