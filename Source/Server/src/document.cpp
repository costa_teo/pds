#include "document.h"

Document::Document(int documentId) : documentId(documentId){
}

QString Document::getDocumentName() {
    return model.getDocumentName();
}

void Document::setDocumentName(QString name) {
    model.setDocumentName(name);
}

QString Document::getURI() {
    return model.getURI();
}

void Document::setURI(QString URI) {
    model.setURI(URI);
}

const std::vector<int> Document::getActiveClientsId() {
    std::vector<int> v;

    for(Client c : model.getActiveClients()) {
        v.push_back(c.getClientId());
    }

    return v;
}

const std::vector<Client> Document::getActiveClients() {
    return model.getActiveClients();
}

void Document::addClient(Client &client) {
    model.addActiveClient(client);
}

void Document::removeClient(int client) {
    model.removeClient(client);
}

int Document::containsClient(int client) {
    return model.containsClient(client);
}

int Document::isAlreadyOpened() {
    return opened;
}

void Document::closeDocument() {
    opened = 0;
}

void Document::loadModelFromString(const vector<InternalSymbol> &text, std::vector<ExternalAction> &actions) {
    model = Model(model.getDocumentName(), model.getURI(), text, actions);
    opened = 1;
}

void Document::modifyModel(std::vector<ExternalAction> &actions) {
    model.makeAction(actions);
}

std::vector<ExternalAction> Document::getLastActions() {
    return model.getActionsNewClient();

}

void Document::updateCursor(int clientId, FracPos position) {
    model.updateClient(clientId, position);
}

vector<InternalSymbol> Document::getInternalSymbols(){
    return model.getInternalSymbols();
}
