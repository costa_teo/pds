#include "connectionmanager.h"

ConnectionManager::ConnectionManager(QObject *parent) : QTcpServer(parent){

    qRegisterMetaType<AccountMessage>("AccountMessage");
    qRegisterMetaType<ActiveClientMessage>("ActiveClientMessage");
    qRegisterMetaType<ErrorMessage>("ErrorMessage");
    qRegisterMetaType<InfoMessage>("InfoMessage");
    qRegisterMetaType<ListDocumentMessage>("ListDocumentMessage");
    qRegisterMetaType<ManageFileMessage>("ManageFileMessage");
    qRegisterMetaType<EditMessage>("EditMessage");
}

void ConnectionManager::serveClients(){

    QThread *conManager = new QThread();
    this->moveToThread(conManager);
    connect(conManager, SIGNAL(started()), this, SLOT(threadExec()));
    conManager->start();
    std::cerr << "CM - Thread started\n";
}

void ConnectionManager::threadExec(){

    if(!this->listen(QHostAddress::Any, 4444)){
        std::cerr << "CM - Error in Listen\n";
    }
    else{
        std::cerr << "CM - Listen done\n";
    }
}

/*
 * Override of the corresponding method in QTcpServer class:
 * creation of a new Thread and make it run to handle a new arriving connection
 */
void QTcpServer::incomingConnection(qintptr socketDescriptor){
    try {
        std::cerr << "Connected with a Client. SocketDescriptor = " << socketDescriptor <<  "\n";
        ConnectionInterface *connInterface = new ConnectionInterface(socketDescriptor, nullptr, &bigQueue); //new ConnectionInterface(socketDescriptor, this);
        QThread *connInterfaceThread = new QThread();
        connInterface->moveToThread(connInterfaceThread);
        connect(connInterface, SIGNAL(finished()), connInterface, SLOT(deleteLater()));
        connect(connInterfaceThread, SIGNAL(started()), connInterface, SLOT(start()));
        connInterfaceThread->start();
    } catch (...) {
        std::cerr << "CM - UNEXPECTED EXCEPTION";
        //Should never happen, but at least Server does not crash
    }
}

void ConnectionManager::sendResponseMessage(MessageWrapper &msg) {
    int type = msg.getType();
    ConnectionInterface *ci = msg.ci;

    this->disconnect();

    if(type == MessageType::T_EditMessage){
        connect(this, SIGNAL(sendMessage(EditMessage)), ci, SLOT(sendEditMessage(EditMessage)));
        emit sendMessage(std::get<EditMessage>(msg.message));
    } else if(type == MessageType::T_ListDocumentMessage){
        connect(this, SIGNAL(sendMessage(ListDocumentMessage)), ci, SLOT(sendListDocumentMessage(ListDocumentMessage)));
        emit sendMessage(std::get<ListDocumentMessage>(msg.message));
    } else if(type == MessageType::T_AccountMessage){
        connect(this, SIGNAL(sendMessage(AccountMessage)), ci, SLOT(sendAccountMessage(AccountMessage)));
        emit sendMessage(std::get<AccountMessage>(msg.message));
    } else if(type == MessageType::T_ManageFileMessage){
        connect(this, SIGNAL(sendMessage(ManageFileMessage)), ci, SLOT(sendManageFileMessage(ManageFileMessage)));
        emit sendMessage(std::get<ManageFileMessage>(msg.message));
    } else if(type == MessageType::T_ErrorMessage){
        connect(this, SIGNAL(sendMessage(ErrorMessage)), ci, SLOT(sendErrorMessage(ErrorMessage)));
        emit sendMessage(std::get<ErrorMessage>(msg.message));
    } else if(type == MessageType::T_ActiveClientMessage){
        connect(this, SIGNAL(sendMessage(ActiveClientMessage)), ci, SLOT(sendActiveClientMessage(ActiveClientMessage)));
        emit sendMessage(std::get<ActiveClientMessage>(msg.message));
    } else if(type == MessageType::T_InfoMessage){
        connect(this, SIGNAL(sendMessage(InfoMessage)), ci, SLOT(sendInfoMessage(InfoMessage)));
        emit sendMessage(std::get<InfoMessage>(msg.message));
    }
}

void ConnectionManager::sendMessageToClients(MessageWrapper &msg, vector<int> &clients) {
    for(int clientId : clients){
        msg.ci = SecurityManager::findConnectionInterfaceByClientId(clientId);
        sendResponseMessage(msg);
    }
}

MessageWrapper ConnectionManager::getMessageFromClients() {
    return bigQueue.pop();
}
