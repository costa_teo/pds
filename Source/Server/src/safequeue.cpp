#include "safequeue.h"

SafeQueue::SafeQueue() {}

void SafeQueue::push(MessageWrapper msg) {
    std::unique_lock<std::mutex> ul(m);

    q.push(msg);
    cv.notify_all();
}

MessageWrapper SafeQueue::pop() {
    std::unique_lock<std::mutex> ul(m);

    while(q.empty()) {
        cv.wait(ul);
    }

    MessageWrapper msg = q.front();
    q.pop();

    return msg;
}

// Remember to add all new types here
int MessageWrapper::getType() {
    int type = -1;

    std::visit([&type](wrapper &&wrap){
        if(std::holds_alternative<ManageFileMessage>(wrap)) {
            type = T_ManageFileMessage;
        } else if (std::holds_alternative<ListDocumentMessage>(wrap)) {
            type = T_ListDocumentMessage;
        } else if (std::holds_alternative<ErrorMessage>(wrap)) {
            type = T_ErrorMessage;
        } else if (std::holds_alternative<EditMessage>(wrap)) {
            type = T_EditMessage;
        } else if (std::holds_alternative<AccountMessage>(wrap)) {
            type = T_AccountMessage;
        } else if (std::holds_alternative<InfoMessage>(wrap)) {
            type = T_InfoMessage;
        } else if (std::holds_alternative<ActiveClientMessage>(wrap)) {
            type = T_ActiveClientMessage;
        }
    }, this->message);

    return type;
}
