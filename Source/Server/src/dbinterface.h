#ifndef DBINTERFACE_H
#define DBINTERFACE_H

#include <QtSql>
#include <QDebug>
#include <QSqlQuery>
#include <fstream>

#include "document.h"
#include "../Library/Client/client.h"
#include "custom_sql_exceptions.h"

class DBInterface {
public:
    explicit DBInterface();
    void openDBConnection();
    void closeDBConnection();
    void checkCredentials(QString, QString);
    uint createUser(QString, QString, QString, uint);
    Document createDocument(QString, QString);
    vector<Document> loadDocuments();
    void addDocumentAccess(QString, QString);
    void removeDocument(QString, QString);
    void deleteUser(QString);
    vector<QString> getAccessibileDocuments(QString);
    void updateUserProfile(QString, QString, uint);
    void updateUserPassword(QString, QString);
    void updateDocumentContent(QString, QString);
    QString loadDocumentContent(QString);
    Client getUser(QString);
private:
    QSqlDatabase db;
    QSqlQuery query;

    int getUID(QString);
};

#endif // DBINTERFACE_H
