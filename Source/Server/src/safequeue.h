#ifndef SAFEQUEUE_H
#define SAFEQUEUE_H

#include <queue>
#include <condition_variable>

#include "connectioninterface.h"

class MessageWrapper {
    using wrapper = std::variant<ManageFileMessage, ListDocumentMessage, ErrorMessage, EditMessage, AccountMessage, InfoMessage, ActiveClientMessage>;
public:
    ConnectionInterface *ci;
    wrapper message;

    MessageWrapper(wrapper message, ConnectionInterface *ci) :
        ci(ci), message(message) {}

    int getType();
};

class SafeQueue {
public:
    explicit SafeQueue();
    void push(MessageWrapper);
    MessageWrapper pop();
private:
    std::queue<MessageWrapper> q;
    std::mutex m;
    std::condition_variable cv;
};

#endif // SAFEQUEUE_H
