#include "dbinterface.h"

DBInterface::DBInterface()
{
    db = QSqlDatabase::addDatabase("QSQLITE");
    query = QSqlQuery("", db);
    QString path = QStandardPaths::standardLocations(QStandardPaths::AppDataLocation)[0];
    path.append("/shared_docs.db");

    db.setDatabaseName(path);
}

void DBInterface::openDBConnection()
{
    if(!db.open()) {
        throw ConnectionException("Failed to open the DB");
    }
    else {
        qDebug() << "Application started";
    }
}

void DBInterface::closeDBConnection() {
    db.close();
}

void DBInterface::checkCredentials(QString receivedUsername, QString receivedPassword)
{
    if(!db.isOpen()) {
        throw ConnectionException("DB is not opened");
    }
    query.prepare("SELECT * FROM user WHERE username = :username AND password = :password");
    query.bindValue(":username", receivedUsername);
    query.bindValue(":password", receivedPassword);
    if(query.exec()) {
        if(!query.next()) {
            throw BadParamException("Invalid username or password");
        }
    }
    else {
        throw QueryException(query.lastError().databaseText());
    }
}

uint DBInterface::createUser(QString receivedUsername, QString receivedPassword, QString receivedNickname, uint avatarNumber)
{
    if(!db.isOpen()) {
        throw ConnectionException("DB is not opened");
    }
    query.prepare("SELECT * FROM user WHERE username = :username");
    query.bindValue(":username", receivedUsername);
    if(query.exec()) {
        if(!query.next()) {
            query.prepare("INSERT INTO user (uid, username, password, nickname, avatar) VALUES (NULL, :username, :password, :nickname, :avatar)");
            query.bindValue(":username", receivedUsername);
            query.bindValue(":password", receivedPassword);
            query.bindValue(":nickname", receivedNickname);
            query.bindValue(":avatar", avatarNumber);
            if(query.exec()) {
                return query.lastInsertId().toUInt();
                qDebug("User correctly created");
            }
            else
                throw QueryException(query.lastError().databaseText());
        }
        else {
            throw BadParamException("A user with the selected username already exists");
        }
    }
    else {
        throw QueryException(query.lastError().databaseText());
    }
}

Document DBInterface::createDocument(QString receivedUsername, QString receivedDocname)
{
    int did = -1, uid = -1;

    std::hash<std::string> hash_fn;
    size_t hashedURI = hash_fn(receivedDocname.toStdString());
    QString URI(QString::fromStdString(std::to_string(hashedURI)));

    if(!db.isOpen()) {
        throw ConnectionException("DB is not opened");
    }

    uid = getUID(receivedUsername);
    if(uid < 0) {
        throw InternalException("Invalid username");
    }

    query.prepare("SELECT * FROM document WHERE docname = :docname");
    query.bindValue(":docname", receivedDocname);
    if(query.exec()) {
        if(!query.next()) {
            db.transaction();
            query.prepare("INSERT INTO document (did, docname, uri, content) VALUES (NULL, :docname, :uri, '')");
            query.bindValue(":docname", receivedDocname);
            query.bindValue(":uri", URI);
            if(query.exec()) {
                did = (query.lastInsertId()).toInt();
                query.prepare("INSERT INTO has_access (uid, did, is_owner) VALUES (:uid, :did, TRUE)");
                query.bindValue(":uid", uid);
                query.bindValue(":did", did);
                if(query.exec()) {
                    db.commit();
                    qDebug("Document correctly created");
                }
                else {
                    db.rollback();
                    throw QueryException(query.lastError().databaseText());
                }
            }
            else {
                db.rollback();
                throw QueryException(query.lastError().databaseText());
            }
        }
        else {
            throw BadParamException("Invalid document name");
        }
    }
    else {
        throw QueryException(query.lastError().databaseText());
    }

    Document newDoc(did);
    newDoc.setDocumentName(receivedDocname);
    newDoc.setURI(URI);

    return newDoc;
}

vector<Document> DBInterface::loadDocuments()
{
    vector<Document> documents;

    if(!db.isOpen()) {
        throw ConnectionException("DB is not opened");
    }
    query.prepare("SELECT * FROM document");
    if(query.exec()) {
        for(int i=0; query.next(); i++) {
            Document d = Document(i);
            d.setDocumentName(query.value(1).toString());
            d.setURI(query.value(2).toString());
            documents.push_back(d);
        }
        return documents;
    }
    else
        throw QueryException(query.lastError().databaseText());
}

void DBInterface::addDocumentAccess(QString receivedUsername, QString receivedURI)
{
    int uid;

    if(!db.isOpen()) {
        throw ConnectionException("DB is not opened");
    }
    uid = getUID(receivedUsername);
    if(uid < 0) {
        throw InternalException("Invalid username");
    }
    query.prepare("SELECT did FROM document WHERE uri = :uri");
    query.bindValue(":uri", receivedURI);
    if(query.exec()) {
        if(query.next()) {
            int did = query.value(0).toInt();
            query.prepare("INSERT OR IGNORE INTO has_access (uid, did, is_owner) VALUES (:uid, :did, FALSE)");
            query.bindValue(":uid", uid);
            query.bindValue(":did", did);
            if(query.exec())
                qDebug("Access correctly granted");
            else
                throw QueryException(query.lastError().databaseText());
        }
        else {
            throw BadParamException("Invalid URI");
        }
    }
    else {
        throw QueryException(query.lastError().databaseText());
    }
}

void DBInterface::removeDocument(QString receivedUsername, QString receivedDocname)
{
    int uid;
    if(!db.isOpen()) {
        throw ConnectionException("DB is not opened");
    }
    uid = getUID(receivedUsername);
    if(uid < 0) {
        throw InternalException("Invalid username");
    }
    query.prepare("SELECT * FROM has_access WHERE uid = :uid AND did = (SELECT did FROM document WHERE docname = :docname)");
    query.bindValue(":uid", uid);
    query.bindValue(":docname", receivedDocname);
    if(query.exec()) {
        if(query.next()) {
            int did = query.value(1).toInt();
            db.transaction();
            query.prepare("DELETE FROM has_access WHERE uid = :uid AND did = :did");
            query.bindValue(":uid", uid);
            query.bindValue(":did", did);
            if(query.exec()) {
                query.prepare("SELECT COUNT(uid) FROM has_access WHERE did = :did");
                query.bindValue(":did", did);
                if(query.exec()) {
                    query.next();
                    if(query.value(0).toInt() == 0) {
                        query.prepare("DELETE FROM document WHERE did = :did");
                        query.bindValue(":did", did);
                        if(query.exec()) {
                            db.commit();
                            qDebug("Document correctly and permanently removed");
                        }
                        else {
                            db.rollback();
                            throw QueryException(query.lastError().databaseText());
                        }
                    }
                    else {
                        db.commit();
                        qDebug("Document correctly removed");
                    }
                }
                else {
                    db.rollback();
                    throw QueryException(query.lastError().databaseText());
                }
            }
            else {
                db.rollback();
                throw QueryException(query.lastError().databaseText());
            }
        }
        else {
            throw BadParamException("Invalid document name");
        }
    }
    else {
        throw QueryException(query.lastError().databaseText());
    }
}

void DBInterface::deleteUser(QString receivedUsername)
{
    if(!db.isOpen()) {
        throw ConnectionException("DB is not opened");
    }
    int uid = getUID(receivedUsername);
    if(uid < 0) {
        throw InternalException("Invalid username");
    }
    db.transaction();
    query.prepare("DELETE FROM user WHERE username = :username");
    query.bindValue(":username", receivedUsername);
    if(query.exec()) {
        if(query.numRowsAffected() == 1) {
            query.prepare("DELETE FROM has_access WHERE uid = :uid");
            query.bindValue(":uid", uid);
            if(query.exec()) {
                db.commit();
                qDebug("User correctly deleted");
            }
            else {
                db.rollback();
                throw QueryException(query.lastError().databaseText());
            }
        }
        else {
            db.rollback();
            throw BadParamException("Invalid username");
        }
    }
    else {
        db.rollback();
        throw QueryException(query.lastError().databaseText());
    }
}

void DBInterface::updateUserProfile(QString receivedUsername, QString receivedNickname, uint avatarNumber)
{
    if(!db.isOpen()) {
        throw ConnectionException("DB is not opened");
    }
    query.prepare("UPDATE user SET nickname = :nickname, avatar = :avatar WHERE username = :username");
    query.bindValue(":nickname", receivedNickname);
    query.bindValue(":avatar", avatarNumber);
    query.bindValue(":username", receivedUsername);
    if(query.exec()) {
        if(query.numRowsAffected() == 1)
            qDebug("Profile correctly updated");
        else
            throw BadParamException("Invalid username");
    }
    else
        throw QueryException(query.lastError().databaseText());
}

void DBInterface::updateUserPassword(QString receivedUsername, QString receivedPassword)
{
    if(!db.isOpen()) {
        throw ConnectionException("DB is not opened");
    }
    query.prepare("UPDATE user SET password = :password WHERE username = :username");
    query.bindValue(":username", receivedUsername);
    query.bindValue(":password", receivedPassword);
    if(query.exec()) {
        if(query.numRowsAffected() == 1)
            qDebug("Password correctly updated");
        else
            throw BadParamException("Invalid username");
    }
    else
        throw QueryException(query.lastError().databaseText());
}

std::vector<QString> DBInterface::getAccessibileDocuments(QString receivedUsername)
{
    std::vector<QString> documentNames;
    if(!db.isOpen()) {
        throw ConnectionException("DB is not opened");
    }
    query.prepare("SELECT docname FROM has_access h, document d "
                  "WHERE d.did = h.did AND h.uid = (SELECT uid FROM user WHERE username = :username)");
    query.bindValue(":username", receivedUsername);
    if(query.exec()) {
        while(query.next()) {
            documentNames.push_back(query.value(0).toString());
        }
    }
    else
        throw QueryException(query.lastError().databaseText());

    return documentNames;
}

void DBInterface::updateDocumentContent(QString documentName, QString currentContent)
{
    if(!db.isOpen()) {
        throw ConnectionException("DB is not opened");
    }
    query.prepare("UPDATE document SET content = :content WHERE docname = :docname");
    query.bindValue(":content", currentContent);
    query.bindValue(":docname", documentName);
    if(query.exec()) {
        if(query.numRowsAffected() == 1) {
             //qDebug("Content correctly updated");
        }
        else
            throw BadParamException("Invalid document name");
    }
    else
        throw QueryException(query.lastError().databaseText());
}

QString DBInterface::loadDocumentContent(QString documentName)
{
    if(!db.isOpen()) {
        throw ConnectionException("DB is not opened");
    }
    query.prepare("SELECT content FROM document WHERE docname = :docname");
    query.bindValue(":docname", documentName);
    if(query.exec()) {
        if(query.next())
            return query.value(0).toString();
        else
            throw BadParamException("Invalid document name");
    }
    else
        throw QueryException(query.lastError().databaseText());
}

Client DBInterface::getUser(QString receivedUsername)
{
    if(!db.isOpen()) {
        throw ConnectionException("DB is not opened");
    }
    query.prepare("SELECT * FROM user WHERE username = :username");
    query.bindValue(":username", receivedUsername);
    if(query.exec()) {
        if(query.next()) {
            Client c = Client(query.value(0).toInt(),
                              receivedUsername,
                              query.value(3).toString(),
                              query.value(4).toUInt());
            return c;
        }
        else
            throw BadParamException("Invalid username");
    }
    else
        throw QueryException(query.lastError().databaseText());

}

int DBInterface::getUID(QString receivedUsername) {
    int uid = -1;
    if(!db.isOpen()) {
        return uid;
    }
    query.prepare("SELECT uid FROM user WHERE username = :username");
    query.bindValue(":username", receivedUsername);
    if(query.exec()) {
        if(query.next())
            uid = query.value(0).toInt();
    }
    else {
        qDebug("Failed to exec query1");
    }
    return uid;
}
