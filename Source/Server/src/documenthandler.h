#ifndef DOCUMENTHANDLER_H
#define DOCUMENTHANDLER_H

#include "connectionmanager.h"
#include "document.h"
#include "dbinterface.h"

class DocumentHandler
{
public:
    explicit DocumentHandler();
    void handleRequest();
    ~DocumentHandler(){
        delete connection;
        database.closeDBConnection();
    }
private:
    ConnectionManager *connection;
    std::vector<Document> documents;
    DBInterface database;
    std::mutex docM;

    void manageFileStatus(const ManageFileMessage&, ConnectionInterface *);
    void getFileList(const ListDocumentMessage&, ConnectionInterface *);
    void manageError(const ErrorMessage&, ConnectionInterface *);
    void editDocument(EditMessage&, ConnectionInterface *);
    void manageAccount(const AccountMessage&, ConnectionInterface *);

    QString toDBRepresentation(vector<InternalSymbol>);
    vector<InternalSymbol> fromDBRepresentation(QString JsonText);

    void removeClientFromDocuments(int, ConnectionInterface *, std::vector<Document> &, ConnectionManager *);
    void openFile(ManageFileMessage, ConnectionInterface *, std::vector<Document> &, ConnectionManager *, DBInterface);
    [[noreturn]] void periodicalDocumentSaving();   
};

#endif // DOCUMENTHANDLER_H
