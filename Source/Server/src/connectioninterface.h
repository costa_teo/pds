#ifndef CONNECTIONINTERFACE_H
#define CONNECTIONINTERFACE_H

#include <QThread>
#include <QTcpServer>
#include <QTcpSocket>
#include <QJsonObject>
#include <QTimer>

#include "../Library/library.h"

class SafeQueue;

class ConnectionInterface : public QThread
{
    Q_OBJECT
public:
    explicit ConnectionInterface(qintptr, QTcpServer*, SafeQueue*);

    ConnectionInterface(const ConnectionInterface& connectionInterface) : QThread(nullptr) {
        this->sockTcp = connectionInterface.sockTcp;
        this->socketDescriptor = connectionInterface.socketDescriptor;
        this->serverQueue = connectionInterface.serverQueue;
    }
    ~ConnectionInterface(){
        delete timer;
    }
private:
    QTcpSocket *sockTcp;
    qintptr socketDescriptor;
    SafeQueue *serverQueue;

    QTimer *timer;
    EditMessage queuedMessage;

    int bytesToRead = 0;
    QByteArray data  =  {};

    std::vector<ExternalAction> final;
    int partCount = 0;
    int totPart = 0;

    QByteArray buildFinalMessage(QJsonDocument json);
    void trueSend(EditMessage &);
    void writeSocket(QByteArray&);

public slots:
    void receiveMessage();
    void disconnected();
    void start();
    void tryToSend();

    void sendAccountMessage(AccountMessage msg);
    void sendEditMessage(EditMessage);
    void sendListDocumentMessage(ListDocumentMessage msg);
    void sendManageFileMessage(ManageFileMessage msg);
    void sendErrorMessage(ErrorMessage msg);
    void sendActiveClientMessage(ActiveClientMessage msg);
    void sendInfoMessage(InfoMessage msg);
};

#endif // CONNECTIONINTERFACE_H
