#ifndef SYMBOL_H
#define SYMBOL_H

#include "FracPos/fracpos.h"

class Symbol {
    char c;

protected:
    FracPos position;


public:

    void setC(char c){
        this->c=c;
    }

    FracPos getFracPos(){
        return position;
    }

    char getC(){
        return c;
    }

    bool operator<(Symbol &that){
     return this->position < that.position;
    }

    Symbol(char c, FracPos position) : c(c), position(position){

    }
    explicit Symbol(const Symbol &left, char c, int client_id, const Symbol &right) : c(c),
        position(left.position, client_id, right.position) {
    }

    explicit Symbol(char c, int client_id, Symbol right) : c(c),
        position(client_id, right.position){
    }

    explicit Symbol(Symbol left, char c, int client_id) : c(c),
        position(left.position, client_id){
    }

    explicit Symbol(char c, int client_id) : c(c),
        position(client_id){
    }


    //Symbol &operator=(const Symbol &that)=delete;
    //Symbol &operator==(const Symbol &that)=delete;

};



#endif // SYMBOL_H
