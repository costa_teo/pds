#include "connectioninterface.h"
#include "safequeue.h"
#include <math.h>

#define mSECONDS 1500

ConnectionInterface::ConnectionInterface(qintptr id, QTcpServer *parent, SafeQueue *bigQueue) :
    QThread (parent), socketDescriptor(id), serverQueue(bigQueue){
}

void ConnectionInterface::start(){
    try {
        sockTcp = new QTcpSocket(nullptr);
        sockTcp->setSocketDescriptor(this->socketDescriptor);
        connect(sockTcp, SIGNAL(readyRead()), this, SLOT(receiveMessage()), Qt::DirectConnection);
        connect(sockTcp, SIGNAL(disconnected()), this, SLOT(disconnected()));

        connect(sockTcp, QOverload<QAbstractSocket::SocketError>::of(&QAbstractSocket::error),
                [=](QAbstractSocket::SocketError socketError){
            std::cerr << socketError;

            disconnect(sockTcp, SIGNAL(disconnected()), this, SLOT(disconnected()));

            disconnected();
        });

        timer = new QTimer();
        timer->setSingleShot(true);
        connect(timer, SIGNAL(timeout()), this, SLOT(tryToSend()));
        
        qDebug() << "start() : " << QThread::currentThreadId();
        std::cerr << "CI - Performing exec to enter event loop...\n";
        int ret = exec();
        std::cerr << "CI - Exec returned "<< ret << "\n";
    } catch (...) {
        std::cerr << "CI - UNHANDLED EXCEPTION IN RECEIVER THREAD\n";
        
        disconnect(sockTcp, SIGNAL(disconnected()), this, SLOT(disconnected()));

        disconnected();
    }
}

void ConnectionInterface::receiveMessage() {

    if(data.isEmpty()){//nuova lettura
        if(sockTcp->bytesAvailable() < 8) {
            emit sockTcp->readyRead();
            return;
        }
        bytesToRead = sockTcp->read(8).toInt();
        data = sockTcp->read(bytesToRead);
        bytesToRead -= data.size();
        if(bytesToRead > 0){
            return;
        }
    }else{ //lettura sucessiva
        QByteArray buff = sockTcp->read(bytesToRead);
        bytesToRead -= buff.size();
        data.append(buff);
        if(bytesToRead > 0){
            return;
        }
    }

    QJsonDocument json = QJsonDocument::fromJson(data);
    if(json.isObject()){
        QJsonObject obj = json.object();

        if(obj.find("messageType").value().toInt() == T_ManageFileMessage) {
            std::cerr << "CI - Received message of type " << T_ManageFileMessage <<"\n";

            ManageFileMessage msg(obj);

            msg.log();
            serverQueue->push(MessageWrapper(msg, this));
            std::cerr <<  "CI - Message pushed into queue\n";
        } else if(obj.find("messageType").value().toInt() == T_ListDocumentMessage) {
            std::cerr << "CI - Received message of type " << T_ListDocumentMessage <<"\n";

            ListDocumentMessage msg(obj);

            msg.log();
            serverQueue->push(MessageWrapper(msg, this));
            std::cerr <<  "CI - Message pushed into queue\n";
        } else if(obj.find("messageType").value().toInt() == T_ErrorMessage) {
            std::cerr << "CI - Received message of type " << T_ErrorMessage <<"\n";

            ErrorMessage msg(obj);

            msg.log();
            serverQueue->push(MessageWrapper(msg, this));
            std::cerr <<  "CI - Message pushed into queue\n";
        } else if(obj.find("messageType").value().toInt() == T_ActiveClientMessage) {
            std::cerr << "CI - Received message of type " << T_ActiveClientMessage <<"\n";

            ActiveClientMessage msg(obj);

            msg.log();
            serverQueue->push(MessageWrapper(msg, this));
            std::cerr <<  "CI - Message pushed into queue\n";
        } else if(obj.find("messageType").value().toInt() == T_EditMessage) {
            std::cerr << "CI - Received message of type " << T_EditMessage <<"\n";

            EditMessage msg(obj);

            msg.log();
            serverQueue->push(MessageWrapper(msg, this));
            std::cerr <<  "CI - Message pushed into queue\n";

        } else if(obj.find("messageType").value().toInt() == T_AccountMessage) {
            std::cerr << "CI - Received message of type " << T_AccountMessage <<"\n";

            AccountMessage msg(obj);

            msg.log();
            serverQueue->push(MessageWrapper(msg, this));
            std::cerr <<  "CI - Message pushed into queue\n";
        } else if(obj.find("messageType").value().toInt() == T_InfoMessage) {
            std::cerr << "CI - Received message of type " << T_InfoMessage <<"\n";
            InfoMessage msg(obj);

            msg.log();
            serverQueue->push(MessageWrapper(msg, this));
            std::cerr <<  "CI - Message pushed into queue\n";
        } else {
            std::cerr << "CI - Bad Message type" <<"\n";
        }
    }

    bytesToRead = 0;
    data.clear();

    if(sockTcp->bytesAvailable()>0){
        emit sockTcp->readyRead();
    }
}

void ConnectionInterface::sendEditMessage(EditMessage msg) {
    std::cerr <<  "CI - Sending Edit Message\n";
    msg.log();

    if(!timer->isActive()) {
        timer->setInterval(mSECONDS);
        timer->start();
    }
    
    if(msg.document != queuedMessage.document) {
        if((queuedMessage.externalActions.size() > 0 || queuedMessage.externalActions.size() > 0))
            tryToSend();

        queuedMessage.document = msg.document;
        queuedMessage.clientId = msg.clientId;
    }

    queuedMessage.externalActions.insert(queuedMessage.externalActions.end(), msg.externalActions.begin(), msg.externalActions.end());

    for(pair<int, FracPos> newP : msg.clientsCursors) {
        bool found = false;
        for(pair<int, FracPos> &oldP : queuedMessage.clientsCursors) {
            if(newP == oldP) {
                oldP = newP;
                found = true;
                break;
            }
        }

        if(!found) {
            queuedMessage.clientsCursors.push_back(newP);
        }
    }
}

void ConnectionInterface::sendListDocumentMessage(ListDocumentMessage msg) {
    std::cerr <<  "CI - Sending List Document Message\n";
    msg.log();
    
    QJsonDocument json = msg.toJsonMessage();
    QByteArray finalMessage = buildFinalMessage(json);
    /*Send of message transformed in Json representation*/
    writeSocket(finalMessage);
}

void ConnectionInterface::sendAccountMessage(AccountMessage msg){
    std::cerr <<  "CI - Sending Account Message\n";
    msg.log();
    
    QJsonDocument json = msg.toJsonMessage();
    QByteArray finalMessage = buildFinalMessage(json);
    /*Send of message transformed in Json representation*/
    writeSocket(finalMessage);
}

void ConnectionInterface::sendManageFileMessage(ManageFileMessage msg) {
    std::cerr <<  "CI - Sending Manage File Message\n";
    msg.log();

    if(msg.operationType == Open && !timer->isActive()) {
        timer->setInterval(250);    //After open, an edit message will come soon
        timer->start();
    }
    
    QJsonDocument json = msg.toJsonMessage();
    QByteArray finalMessage = buildFinalMessage(json);
    /*Send of message transformed in Json representation*/
    writeSocket(finalMessage);
}

void ConnectionInterface::sendErrorMessage(ErrorMessage msg) {
    std::cerr <<  "CI - Sending Error Message\n";
    msg.log();
    
    QJsonDocument json = msg.toJsonMessage();
    QByteArray finalMessage = buildFinalMessage(json);
    /*Send of message transformed in Json representation*/
    writeSocket(finalMessage);
}

void ConnectionInterface::sendActiveClientMessage(ActiveClientMessage msg) {
    std::cerr <<  "CI - Sending Active Client Message\n";
    msg.log();
    
    QJsonDocument json = msg.toJsonMessage();
    QByteArray finalMessage = buildFinalMessage(json);
    /*Send of message transformed in Json representation*/
    writeSocket(finalMessage);
}

void ConnectionInterface::sendInfoMessage(InfoMessage msg){
    std::cerr <<  "CI - Sending Info Message\n";
    msg.log();
    
    QJsonDocument json = msg.toJsonMessage();
    QByteArray finalMessage = buildFinalMessage(json);
    /*Send of message transformed in Json representation*/
    writeSocket(finalMessage);
}

void ConnectionInterface::writeSocket(QByteArray& finalMessage){
    int bytesToSend = finalMessage.size();
    while(bytesToSend > 0){
        int sent = static_cast<int>(sockTcp->write(finalMessage));
        if(sent == -1)
            throw std::exception();
        bytesToSend -= sent;
        finalMessage = finalMessage.remove(0, sent);
    }
}

QByteArray ConnectionInterface::buildFinalMessage(QJsonDocument json){ //It builds the message to be sent through socket : [ [dimension of the message] + [Json message] ]
    QByteArray messageDim;
    QByteArray zeros;
    int dimMax = 8;
    
    messageDim.setNum(json.toJson().size());
    for(int i=0;i<dimMax - messageDim.size();i++)
        zeros.push_back("0");
    zeros.append(messageDim);
    return zeros.append(json.toJson());
}

void ConnectionInterface::tryToSend() {

    if(queuedMessage.externalActions.size() > 0 || queuedMessage.clientsCursors.size() > 0) {
        trueSend(queuedMessage);

        queuedMessage.externalActions.clear();
        queuedMessage.clientsCursors.clear();
    }
}

void ConnectionInterface::trueSend(EditMessage &msgToSend) {
    QJsonDocument json = msgToSend.toJsonMessage();
    QByteArray finalMessage = buildFinalMessage(json);
    //Send of message transformed in Json representation
    writeSocket(finalMessage);
}

void ConnectionInterface::disconnected(){
    std::cerr <<  "CI - A Client has disconnected\n";
    
    ErrorMessage msg(-1, NetworkError, "Client disconnected");
    serverQueue->push(MessageWrapper(msg, this));
    
    sockTcp->close();
    sockTcp->deleteLater();

    timer->stop();
    delete timer;
    exit(0);
}
