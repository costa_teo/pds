#include "documenthandler.h"

#define SECONDS 90

void shareClientActivity(int, std::vector<Client>, std::vector<int>, ConnectionInterface *, ConnectionManager *);
bool securityCheck(int, ConnectionInterface *, ConnectionManager *);

DocumentHandler::DocumentHandler() {
    connection = new ConnectionManager(nullptr);

    //Connect with DB
    database.openDBConnection();

    std::cerr << "DH - DB connected\n";

    //Start listening for clients
    connection->serveClients();

    std::cerr << "DH - Launched thread to manage clients\n";

    //Load documents from DB
    documents = database.loadDocuments();

    std::cerr << "DH - Documents loaded from DB\n";

    std::thread thread([this](){
        periodicalDocumentSaving();
    });
    thread.detach();

    std::cerr << "DH - Thread for autosavings started\n";
}

void DocumentHandler::handleRequest() {
    MessageWrapper newMessage = connection->getMessageFromClients();

    int type = newMessage.getType();

    std::cerr << "DH - Handling message of type " << type;

    try {
        if(type == T_ManageFileMessage) {
            manageFileStatus(std::get<ManageFileMessage>(newMessage.message), newMessage.ci);
        } else if (type == T_ListDocumentMessage) {
            getFileList(std::get<ListDocumentMessage>(newMessage.message), newMessage.ci);
        } else if (type == T_ErrorMessage) {
            manageError(std::get<ErrorMessage>(newMessage.message), newMessage.ci);
        } else if (type == T_EditMessage) {
            editDocument(std::get<EditMessage>(newMessage.message), newMessage.ci);
        } else if (type == T_AccountMessage) {
            manageAccount(std::get<AccountMessage>(newMessage.message), newMessage.ci);
        }
    } catch (QString s) {
        //If exception occurs, skip to next message
        std::cerr << "DH - HANDLED EXCEPTION IN QUEUE THREAD\n";

        //Errore
        MessageWrapper response(ErrorMessage(-1, NoAction, s), newMessage.ci);
        connection->sendResponseMessage(response);
    } catch (...) {
        //If exception occurs, skip to next message
        std::cerr << "DH - UNHANDLED EXCEPTION IN QUEUE THREAD\n";
    }
}

void DocumentHandler::manageFileStatus(const ManageFileMessage &msg, ConnectionInterface *ci) {
    std::cerr << " - Manage File\n";
    msg.log();

    if(!securityCheck(msg.clientId, ci, connection))
        return;

    if(msg.operationType == Open) {
        std::vector<QString> names;


        try {
            names = database.getAccessibileDocuments(SecurityManager::findClientByClientId(msg.clientId).getUsername());
        } catch (QException e) {
            std::cerr << "DH - " << e.what() << "\n";
            throw COMMON_MESSAGES::E_INTERNAL;
        } catch (...) {
            std::cerr << "DH - Unexpected error during OPEN\n";

            //Errore
            MessageWrapper response(ErrorMessage(msg.clientId, NoAction, COMMON_MESSAGES::E_OPEN), ci);
            connection->sendResponseMessage(response);
            return;
        }

        auto it = std::find(names.begin(), names.end(), msg.document);

        if(it == names.end()) {
            std::cerr << "DH - Client " << msg.clientId << " requested a file not accessible by him\n";

            //Errore
            MessageWrapper response(ErrorMessage(msg.clientId, NoAction, COMMON_MESSAGES::E_DENIED), ci);
            connection->sendResponseMessage(response);
            return;
        }

        QString URI;
        int flag = 0;

        {std::lock_guard<std::mutex> lg(docM);
            for(Document doc : documents) {
                if(doc.getDocumentName() == msg.document) {
                    URI = doc.getURI();
                    flag = 1;
                    break;
                }
            }
        }

        if(flag == 0) {
            //Errore
            MessageWrapper response(ErrorMessage(msg.clientId, NoAction, COMMON_MESSAGES::E_NOT_FOUND), ci);
            connection->sendResponseMessage(response);
            return;
        }

        openFile(ManageFileMessage(msg.clientId, Open, msg.document, URI), ci, documents, connection, database);

        std::vector<Client> activeClients;
        std::vector<int> destinationClients;

        {std::lock_guard<std::mutex> lg(docM);
            for(Document doc : documents) {
                if(doc.getDocumentName() == msg.document) {
                    activeClients = doc.getActiveClients();
                    destinationClients = doc.getActiveClientsId();
                    break;
                }
            }
        }

        shareClientActivity(msg.clientId, activeClients, destinationClients, ci, connection);
    } else if(msg.operationType == OpenByURI) {

        try {
            database.addDocumentAccess(SecurityManager::findClientByClientId(msg.clientId).getUsername(), msg.URI);
        } catch (BadParamException e) {
            std::cerr << "DH - " << e.what() << "\n";

            //Invalid URI
            MessageWrapper response(ErrorMessage(msg.clientId, NoAction, COMMON_MESSAGES::E_INVALID_URI), ci);
            connection->sendResponseMessage(response);
            return;
        } catch (QException e) {
            std::cerr << "DH - " << e.what() << "\n";
            throw COMMON_MESSAGES::E_INTERNAL;
        } catch (...) {
            std::cerr << "DH - Unexpected error during OPEN by URI\n";

            //Errore
            MessageWrapper response(ErrorMessage(msg.clientId, NoAction, COMMON_MESSAGES::E_OPEN_URI), ci);
            connection->sendResponseMessage(response);
            return;
        }

        int flag = 0;
        QString docName;

        {std::lock_guard<std::mutex> lg(docM);
            for(Document &doc : documents) {
                if(doc.getURI() == msg.URI) {
                    docName = doc.getDocumentName();
                    flag = 1;
                    break;
                }
            }
        }

        if(flag == 0) {
            //Errore
            MessageWrapper response(ErrorMessage(msg.clientId, NoAction, COMMON_MESSAGES::E_NOT_FOUND), ci);
            connection->sendResponseMessage(response);
            return;
        }

        openFile(ManageFileMessage(msg.clientId, OpenByURI, docName, msg.URI), ci, documents, connection, database);

        std::vector<Client> activeClients;
        std::vector<int> destinationClients;

        {std::lock_guard<std::mutex> lg(docM);
            for(Document doc : documents) {
                if(doc.getDocumentName() == docName) {
                    activeClients = doc.getActiveClients();
                    destinationClients = doc.getActiveClientsId();
                    break;
                }
            }
        }

        shareClientActivity(msg.clientId, activeClients, destinationClients, ci, connection);

    } else if(msg.operationType == Create) {

        Document newDoc(-1);

        try {
            newDoc = database.createDocument(SecurityManager::findClientByClientId(msg.clientId).getUsername(), msg.document);
        } catch (BadParamException e) {
            std::cerr << "DH - " << e.what() << "\n";

            //Invalid document name
            MessageWrapper response(ErrorMessage(msg.clientId, NoAction, COMMON_MESSAGES::E_INVALID_DOCUMENT), ci);
            connection->sendResponseMessage(response);
            return;
        } catch (QException e) {
            std::cerr << "DH - " << e.what() << "\n";
            throw COMMON_MESSAGES::E_INTERNAL;
        } catch (...) {
            std::cerr << "DH - Unexpected error during CREATE\n";

            //Errore
            MessageWrapper response(ErrorMessage(msg.clientId, NoAction, COMMON_MESSAGES::E_CREATE), ci);
            connection->sendResponseMessage(response);
            return;
        }

        {std::lock_guard<std::mutex> lg(docM);
            documents.push_back(newDoc);
        }
        std::cerr << "Document " << msg.document.toStdString() << " created by Client " << msg.clientId << "\n";

        try {
            openFile(ManageFileMessage(msg.clientId, Open, newDoc.getDocumentName(), newDoc.getURI()), ci, documents, connection, database);
        } catch (...) {
            std::lock_guard<std::mutex> lg(docM);
            documents.pop_back();
            return;
        }
    } else if(msg.operationType == Delete) {

        int flag = 0;

        {std::lock_guard<std::mutex> lg(docM);
            for(Document doc : documents) {
                if(doc.getDocumentName() == msg.document) {
                    flag = 1;

                    if(!doc.containsClient(msg.clientId)) {
                        //Errore
                        MessageWrapper response(ErrorMessage(msg.clientId, NoAction, COMMON_MESSAGES::E_NOT_OPENED), ci);
                        connection->sendResponseMessage(response);
                        return;
                    }

                    try {
                        database.removeDocument(SecurityManager::findClientByClientId(msg.clientId).getUsername(), msg.document);
                    } catch (QException e) {
                        std::cerr << "DH - " << e.what() << "\n";
                        throw COMMON_MESSAGES::E_INTERNAL;
                    } catch (...) {
                        std::cerr << "DH - Unexpected error during DELETE\n";

                        //Errore
                        MessageWrapper response(ErrorMessage(msg.clientId, NoAction, COMMON_MESSAGES::E_DELETE), ci);
                        connection->sendResponseMessage(response);
                        return;
                    }
                    docM.unlock();
                    removeClientFromDocuments(msg.clientId, ci, documents, connection);

                    MessageWrapper response(ManageFileMessage(msg.clientId, Delete, msg.document, msg.URI), ci);
                    connection->sendResponseMessage(response);

                    MessageWrapper response2(InfoMessage(msg.clientId, COMMON_MESSAGES::I_DELETED), ci);
                    connection->sendResponseMessage(response2);

                    break;
                }
            }
        }

        if(flag == 0){
            //Errore
            MessageWrapper response(ErrorMessage(msg.clientId, NoAction, COMMON_MESSAGES::E_NOT_FOUND), ci);
            connection->sendResponseMessage(response);
            return;
        }

    } else {
        //Errore
        MessageWrapper response(ErrorMessage(msg.clientId, NoAction, COMMON_MESSAGES::E_BAD_REQUEST), ci);
        connection->sendResponseMessage(response);
        return;
    }
}

void DocumentHandler::getFileList(const ListDocumentMessage &msg, ConnectionInterface *ci) {
    std::cerr << " - File List\n";
    msg.log();

    if(!securityCheck(msg.clientId, ci, connection))
        return;

    std::vector<QString> names;
    try {
        names = database.getAccessibileDocuments(SecurityManager::findClientByClientId(msg.clientId).getUsername());
    } catch (QException e) {
        std::cerr << "DH - " << e.what() << "\n";
        throw COMMON_MESSAGES::E_INTERNAL;
    } catch (...) {
        std::cerr << "DH - Unexpected error during FILE LIST\n";

        //Errore
        MessageWrapper response(ErrorMessage(msg.clientId, NoAction, COMMON_MESSAGES::E_LIST), ci);
        connection->sendResponseMessage(response);
        return;
    }

    if(!names.empty()) {
        ListDocumentMessage newMessage(names);
        newMessage.clientId = msg.clientId;
        MessageWrapper response(newMessage, ci);
        connection->sendResponseMessage(response);
    } else {
        MessageWrapper response(InfoMessage(msg.clientId, COMMON_MESSAGES::I_NO_FILE), ci);
        connection->sendResponseMessage(response);
    }
}

void DocumentHandler::manageError(const ErrorMessage &msg, ConnectionInterface *ci) {
    std::cerr << " - Error\n";
    msg.log();

    if(msg.errorType == NetworkError) {
        int id;
        try {
            id = SecurityManager::findClientIdByCi(ci);
        } catch (QString s) {
            std::cerr << "DH - Client was not logged. Returning\n";
            return;     //No need to do anything else
        }
        removeClientFromDocuments(id, ci, documents, connection);
        SecurityManager::doLogout(ci);

        std::cerr << "DH - Client removed from logged clients\n";
    } else {
        return;
    }
}

void DocumentHandler::editDocument(EditMessage &msg, ConnectionInterface *ci) {
    std::cerr << " - Edit\n";
    msg.log();

    if(!securityCheck(msg.clientId, ci, connection))
        return;

    std::vector<int> destinationClients;
    QString documentName = msg.document;
    int flag = 0;

    {std::lock_guard<std::mutex> lg(docM);
        for(Document &doc : documents) {
            if(doc.getDocumentName() == documentName) {
                //Check if document is opened by client
                if(!doc.containsClient(msg.clientId)) {
                    //Errore
                    MessageWrapper response(ErrorMessage(msg.clientId, NoAction, COMMON_MESSAGES::E_NOT_OPENED), ci);
                    connection->sendResponseMessage(response);
                    return;
                }

                if(!msg.externalActions.empty()) {
                    doc.modifyModel(msg.externalActions);
                }

                if(msg.clientsCursors.size() == 1) {
                    doc.updateCursor(msg.clientsCursors[0].first, msg.clientsCursors[0].second);
                }

                //Then send message to other interested clients
                destinationClients = doc.getActiveClientsId();
                flag = 1;
                break;
            }
        }
    }

    if(flag == 0) {
        //Errore
        MessageWrapper response(ErrorMessage(msg.clientId, NoAction, COMMON_MESSAGES::E_NOT_FOUND), ci);
        connection->sendResponseMessage(response);
        return;
    }

    auto it = std::find(destinationClients.begin(), destinationClients.end(), msg.clientId);
    if(it != destinationClients.end()){
        destinationClients.erase(it);
    }
    std::cerr << "DH - Sending edit to " << destinationClients.size() << " clients\n";

    MessageWrapper response(msg, nullptr);
    connection->sendMessageToClients(response, destinationClients);
}

void DocumentHandler::manageAccount(const AccountMessage &msg, ConnectionInterface *ci) {
    std::cerr << " - Account\n";
    msg.log();

    if(msg.accountOperationType == Login) {

        if(SecurityManager::isAlreadyLoggedWithUsername(msg.username)) {
            //Errore
            MessageWrapper response(ErrorMessage(msg.clientId, NoAction, COMMON_MESSAGES::E_ALREADY_LOGGED), ci);
            connection->sendResponseMessage(response);
            return;
        }

        Client tmp;
        try {
            database.checkCredentials(msg.username, msg.password);
            tmp = database.getUser(msg.username);
        } catch (BadParamException e) {
            std::cerr << "DH - " << e.what() << "\n";

            //Wrong credentials
            MessageWrapper response(ErrorMessage(-1, NoAction, COMMON_MESSAGES::E_WRONG_CREDENTIALS), ci);
            connection->sendResponseMessage(response);
            return;
        } catch (QException e) {
            std::cerr << "DH - " << e.what() << "\n";
            throw COMMON_MESSAGES::E_INTERNAL;
        } catch (...) {
            std::cerr << "DH - Unexpected error during LOGIN\n";

            //Errore
            MessageWrapper response(ErrorMessage(msg.clientId, NoAction, COMMON_MESSAGES::E_LOGIN), ci);
            connection->sendResponseMessage(response);
            return;
        }

        uint bgColor = (static_cast<uint>(tmp.getClientId()) < static_cast<uint>(COLORS.size())) ? static_cast<uint>(tmp.getClientId()) : static_cast<uint>(COLORS.size()) - 1;

        int clientId = SecurityManager::doLogin(ci, msg.username, tmp.getNickname(), tmp.getIconNumber(), bgColor);

        MessageWrapper response(AccountMessage(clientId, tmp.getUsername(), tmp.getNickname(), "SECRET", tmp.getIconNumber(), bgColor, Login), ci);
        connection->sendResponseMessage(response);
    } else if(msg.accountOperationType == NewAccount) {

        if(SecurityManager::isAlreadyLogged(ci)) {
            //Errore
            MessageWrapper response(ErrorMessage(msg.clientId, NoAction, COMMON_MESSAGES::E_STILL_LOGGED), ci);
            connection->sendResponseMessage(response);
            return;
        }

        if(msg.username.isEmpty() || msg.nickname.isEmpty() || msg.nickname.isEmpty() || msg.iconNumber >= ACCOUNT_ICON.size()) {
            //Errore
            MessageWrapper response(ErrorMessage(msg.clientId, NoAction, COMMON_MESSAGES::E_MISSING_CREDENTIALS), ci);
            connection->sendResponseMessage(response);
            return;
        }

        uint id = 0;

        try {
            id = database.createUser(msg.username, msg.password, msg.nickname, msg.iconNumber);
        } catch (BadParamException e) {
            std::cerr << "DH - " << e.what() << "\n";

            //Invalid username
            MessageWrapper response(ErrorMessage(msg.clientId, NoAction, COMMON_MESSAGES::E_INVALID_USER), ci);
            connection->sendResponseMessage(response);
            return;
        } catch (QException e) {
            std::cerr << "DH - " << e.what() << "\n";
            throw COMMON_MESSAGES::E_INTERNAL;
        } catch (...) {
            std::cerr << "DH - Unexpected error during NEW ACCOUNT\n";

            //Errore
            MessageWrapper response(ErrorMessage(msg.clientId, NoAction, COMMON_MESSAGES::E_NEW_ACC), ci);
            connection->sendResponseMessage(response);
            return;
        }

        uint bgColor = (id < static_cast<uint>(COLORS.size())) ? id : static_cast<uint>(COLORS.size()) - 1;

        int clientId = SecurityManager::doLogin(ci, msg.username, msg.nickname, msg.iconNumber, bgColor);

        MessageWrapper response(InfoMessage(clientId, COMMON_MESSAGES::I_ACC_CREATED), ci);
        connection->sendResponseMessage(response);

        MessageWrapper response2(AccountMessage(clientId, msg.username, msg.nickname, "SECRET", msg.iconNumber, bgColor, Login), ci);
        connection->sendResponseMessage(response2);
    } else if(msg.accountOperationType == DeleteAccount) {

        if(!securityCheck(msg.clientId, ci, connection))
            return;

        try {
            database.checkCredentials(msg.username, msg.password);
        } catch (BadParamException e) {
            std::cerr << "DH - " << e.what() << "\n";

            //Wrong credentials
            MessageWrapper response(ErrorMessage(-1, NoAction, COMMON_MESSAGES::E_WRONG_CREDENTIALS), ci);
            connection->sendResponseMessage(response);
            return;
        } catch (QException e) {
            std::cerr << "DH - " << e.what() << "\n";
            throw COMMON_MESSAGES::E_INTERNAL;
        } catch (...) {
            std::cerr << "DH - Unexpected error during LOGIN\n";

            //Errore
            MessageWrapper response(ErrorMessage(msg.clientId, NoAction, COMMON_MESSAGES::E_DELETE_ACC), ci);
            connection->sendResponseMessage(response);
            return;
        }

        if(!SecurityManager::isAlreadyLogged(ci) || msg.username.compare(SecurityManager::findClientByClientId(msg.clientId).getUsername()) != 0) {
            //Errore
            MessageWrapper response(ErrorMessage(-1, NoAction, COMMON_MESSAGES::E_NOT_LOGGED), ci);
            connection->sendResponseMessage(response);
            return;
        }

        try {
            database.deleteUser(msg.username);
        } catch (QException e) {
            std::cerr << "DH - " << e.what() << "\n";
            throw COMMON_MESSAGES::E_INTERNAL;
        } catch (...) {
            std::cerr << "DH - Unexpected error during NEW ACCOUNT\n";

            //Errore
            MessageWrapper response(ErrorMessage(msg.clientId, NoAction, COMMON_MESSAGES::E_DELETE_ACC), ci);
            connection->sendResponseMessage(response);
            return;
        }

        removeClientFromDocuments(msg.clientId, ci, documents, connection);

        SecurityManager::doLogout(ci);

        MessageWrapper response(InfoMessage(msg.clientId, COMMON_MESSAGES::I_ACC_DELETED), ci);
        connection->sendResponseMessage(response);

        MessageWrapper response2(AccountMessage(msg.clientId, "", "", Logout), ci);
        connection->sendResponseMessage(response2);
    } else if(msg.accountOperationType == Logout) {

        if(!securityCheck(msg.clientId, ci, connection))
            return;

        if(!SecurityManager::isAlreadyLogged(ci)) {
            //Errore
            MessageWrapper response(ErrorMessage(-1, NoAction, COMMON_MESSAGES::E_NOT_LOGGED), ci);
            connection->sendResponseMessage(response);
            return;
        }

        removeClientFromDocuments(msg.clientId, ci, documents, connection);

        SecurityManager::doLogout(ci);

        MessageWrapper response(AccountMessage(msg.clientId, "", "", Logout), ci);
        connection->sendResponseMessage(response);
    } else if(msg.accountOperationType == UpdateAccount) {

        if(!securityCheck(msg.clientId, ci, connection))
            return;

        if(msg.username.isEmpty() || msg.nickname.isEmpty() || msg.iconNumber >= ACCOUNT_ICON.size()) {
            //Errore
            MessageWrapper response(ErrorMessage(msg.clientId, NoAction, COMMON_MESSAGES::E_MISSING_CREDENTIALS), ci);
            connection->sendResponseMessage(response);
            return;
        }

        try {
            database.updateUserProfile(msg.username, msg.nickname, msg.iconNumber);
            if(!msg.password.isEmpty())
                database.updateUserPassword(msg.username, msg.password);
        } catch (QException e) {
            std::cerr << "DH - " << e.what() << "\n";
            throw COMMON_MESSAGES::E_INTERNAL;
        } catch (...) {
            std::cerr << "DH - Unexpected error during UPDATE ACCOUNT\n";

            //Errore
            MessageWrapper response(ErrorMessage(msg.clientId, NoAction, COMMON_MESSAGES::E_UPDATE_ACC), ci);
            connection->sendResponseMessage(response);
            return;
        }

        SecurityManager::updateClientInfo(msg.clientId, msg.nickname, msg.iconNumber);

        MessageWrapper response(msg, ci);
        connection->sendResponseMessage(response);

        std::vector<int> destinationClients;
        int flag = 0;

        {std::lock_guard<std::mutex> lg(docM);
            for(Document doc : documents) {
                if(doc.containsClient(msg.clientId)) {
                    destinationClients = doc.getActiveClientsId();
                    flag = 1;
                    break;
                }
            }
        }

        auto it = std::find(destinationClients.begin(), destinationClients.end(), msg.clientId);
        if(it != destinationClients.end()){
            destinationClients.erase(it);
        }

        if(flag && !destinationClients.empty()) {
            Client c = SecurityManager::findClientByClientId(msg.clientId);
            ActiveClientMessage newMessage(msg.clientId, {c});
            newMessage.activeClientOperation = UpdateClient;

            MessageWrapper response(newMessage, ci);
            connection->sendMessageToClients(response, destinationClients);
        }
    } else {
        //Errore
        MessageWrapper response(ErrorMessage(-1, NoAction, COMMON_MESSAGES::E_BAD_REQUEST), ci);
        connection->sendResponseMessage(response);
        return;
    }
}

QString DocumentHandler::toDBRepresentation(vector<InternalSymbol> symbols){
    QJsonArray jsonText;
    for(auto iter = symbols.begin(); iter!=symbols.end(); iter++){
        QJsonObject jsonSymbol = QJsonObject();

        QByteArray cbiu = "";
        iter->getBold() ? cbiu.append('1') : cbiu.append('0');
        iter->getItalic() ? cbiu.append('1') : cbiu.append('0');
        iter->getUnderline() ? cbiu.append('1') : cbiu.append('0');
        cbiu.append(iter->getColor());
        jsonSymbol.insert("cbiu", QString(cbiu));

        jsonSymbol.insert("family", QString(iter->getFamily()));
        jsonSymbol.insert("fSize", iter->getSize());
        jsonSymbol.insert("al", int(iter->getAlignment()));

        jsonSymbol.insert("letter", QString(iter->getC()));

        jsonSymbol.insert("bgcolor", iter->getBackgroundColor());
        jsonText.push_back(jsonSymbol);
    }

    QJsonDocument json(jsonText);
    return json.toJson();
}

vector<InternalSymbol> DocumentHandler::fromDBRepresentation(QString text){

    QJsonDocument json = QJsonDocument::fromJson(QByteArray(text.toUtf8()));
    QJsonArray jsonText = json.array();
    vector<InternalSymbol> symbols;
    for(QJsonValue symbol : jsonText){
        InternalSymbol s(symbol);
        symbols.push_back(s);
    }
    return symbols;
}

void DocumentHandler::openFile(ManageFileMessage msg, ConnectionInterface *ci, std::vector<Document> &documents, ConnectionManager *connection, DBInterface database) {
    int flag = 0;

    removeClientFromDocuments(msg.clientId, ci, documents, connection);

    {std::lock_guard<std::mutex> lg(docM);
        for(Document &doc: documents) {
            if(doc.getDocumentName() == msg.document) {
                //Add the client to the list
                SecurityManager::resetClientPosition(msg.clientId);
                Client c = SecurityManager::findClientByClientId(msg.clientId);

                EditMessage newMessage = EditMessage(msg.clientId);
                newMessage.document = doc.getDocumentName();
                //If file has already been loaded
                if(doc.isAlreadyOpened()) {
                    newMessage.externalActions = doc.getLastActions();
                    std::cerr <<  "DH - File was already opened, retrieved last message\n";
                } else {
                    QString text;

                    try {
                        text = database.loadDocumentContent(doc.getDocumentName());
                    } catch (QException e) {
                        std::cerr << "DH - " << e.what() << "\n";
                        throw COMMON_MESSAGES::E_INTERNAL;
                    } catch (...) {
                        std::cerr << "DH - Unexpected error during OPEN\n";

                        //Errore
                        MessageWrapper response(ErrorMessage(msg.clientId, NoAction, COMMON_MESSAGES::E_OPEN), ci);
                        connection->sendResponseMessage(response);
                        throw COMMON_MESSAGES::E_INTERNAL;
                    }

                    std::vector<InternalSymbol> textSymbols = fromDBRepresentation(text);

                    doc.loadModelFromString(textSymbols, newMessage.externalActions);

                    std::cerr <<  "DH - File was not already opened, model has been loaded\n";
                }

                doc.addClient(c);

                MessageWrapper response(ManageFileMessage(msg.clientId, Open, msg.document, msg.URI), ci);
                connection->sendResponseMessage(response);

                MessageWrapper response2(newMessage, ci);
                connection->sendResponseMessage(response2);

                flag = 1;
                break;
            }
        }
    }

    if(flag == 0) {
        //Errore
        MessageWrapper response(ErrorMessage(msg.clientId, NoAction, COMMON_MESSAGES::E_OPEN), ci);
        connection->sendResponseMessage(response);
        return;
    }
}

void DocumentHandler::removeClientFromDocuments(int clientId, ConnectionInterface *ci, std::vector<Document> &documents, ConnectionManager *connection) {
    std::vector<int> destinationClients;

    {std::lock_guard<std::mutex> lg(docM);
        for(Document &doc: documents) {
            if(doc.containsClient(clientId)) {
                if(doc.getActiveClients().empty()) {
                    doc.closeDocument();

                    try {
                        database.updateDocumentContent(doc.getDocumentName(), toDBRepresentation(doc.getInternalSymbols()));
                    } catch (QException e) {
                        std::cerr << "DH - " << e.what() << "\n";
                        throw COMMON_MESSAGES::E_INTERNAL;
                    } catch (...) {
                        std::cerr << "DH - Unexpected error during Document Saving\n";
                        throw COMMON_MESSAGES::E_INTERNAL;
                    }
                }

                doc.removeClient(clientId);
                destinationClients = doc.getActiveClientsId();
                break;
            }
        }
    }

    std::cerr << "DH - Sending active clients info to " << destinationClients.size() << " clients\n";

    ActiveClientMessage newMessage(clientId);
    newMessage.activeClientOperation = DeleteClient;
    newMessage.activeClients.push_back(SecurityManager::findClientByClientId(clientId));
    MessageWrapper response2(newMessage, ci);
    connection->sendMessageToClients(response2, destinationClients);
}

void DocumentHandler::periodicalDocumentSaving(){

    while(true){
        try{
            this_thread::sleep_for(std::chrono::seconds(SECONDS));
            {std::lock_guard<std::mutex> lg(docM);
                std::for_each(documents.begin(), documents.end(), [this](Document doc){
                    if(doc.isAlreadyOpened()) {
                        database.updateDocumentContent(doc.getDocumentName(), toDBRepresentation(doc.getInternalSymbols()));
                    }
                });
            }
            std::cerr << "DH - periodic save performed\n";
        }catch(...){
            std::cerr << "DH - PERIODIC THREAD EXCEPTION\n";
            //Should never happen, but at least Server does not crash
        }
    }
}

/*
 * From here on, only internal util functions
 */

void shareClientActivity(int clientId, std::vector<Client> activeClients, std::vector<int> destinationClients, ConnectionInterface *ci, ConnectionManager *connection) {

    auto it = std::find_if(activeClients.begin(), activeClients.end(), [clientId](Client el){
            return el.getClientId() == clientId;
});

    if(it != activeClients.end()){
        activeClients.erase(it);
    }

    ActiveClientMessage newMessage(clientId, activeClients);

    if(!activeClients.empty()) {
        newMessage.activeClientOperation = AddClient;
        MessageWrapper response(newMessage, ci);
        connection->sendResponseMessage(response);
    }

    auto it2 = std::find(destinationClients.begin(), destinationClients.end(), clientId);
    if(it2 != destinationClients.end()){
        destinationClients.erase(it2);
    }

    std::cerr << "DH - Sending active clients info to " << destinationClients.size() << " clients\n";

    if(!destinationClients.empty()) {
        newMessage.activeClients.clear();
        newMessage.activeClients.push_back(SecurityManager::findClientByClientId(clientId));
        MessageWrapper response(newMessage, ci);
        connection->sendMessageToClients(response, destinationClients);
    }
}

bool securityCheck(int clientId, ConnectionInterface *ci, ConnectionManager *connection) {
    //Security Check
    if(!SecurityManager::checkMessageOrigin(ci, clientId)) {
        //Errore
        MessageWrapper response(ErrorMessage(clientId, NoAction, COMMON_MESSAGES::E_NOT_LOGGED), ci);
        connection->sendResponseMessage(response);
        return false;
    }

    std::cerr <<  "DH - Security checks passed\n";

    return true;
}
