#include "securitymanager.h"

int SecurityManager::lastId = 100;
std::vector<SecurityManager::clientInfo> SecurityManager::clients;

int SecurityManager::isAlreadyLogged(ConnectionInterface *ci) {
    for(clientInfo c : clients) {
        if(c.ci == ci)
            return 1;
    }
    return 0;
}

//Use only at LOGIN time
int SecurityManager::isAlreadyLoggedWithUsername(QString username) {
    for(clientInfo c : clients) {
        if(c.client.getUsername() == username)
            return 1;
    }
    return 0;
}

int SecurityManager::doLogin(ConnectionInterface *ci, QString username, QString nickname, uint iconNumber, uint backgroundColor) {
    clientInfo newClient;
    newClient.ci = ci;
    newClient.client.setClientId(++lastId);
    newClient.client.setUsername(username);
    newClient.client.setNickname(nickname);
    newClient.client.setIconNumber(iconNumber);
    newClient.client.setBackgroundColor(backgroundColor);

    clients.push_back(newClient);

    return lastId;
}

void SecurityManager::doLogout(ConnectionInterface *ci) {
    auto it = std::find_if(clients.begin(), clients.end(), [ci](auto el){
        return el.ci == ci;
    });

    if(it != clients.end()) {
        clients.erase(it);
    } else {
        throw QString("exception");
    }
}

int SecurityManager::findClientIdByCi(ConnectionInterface *ci){
    for(clientInfo c : clients) {
        if(c.ci == ci)
            return c.client.getClientId();
    }

    throw QString("exception");
}

ConnectionInterface *SecurityManager::findConnectionInterfaceByClientId(int clientId){
    for(auto c : clients){
        if(clientId == c.client.getClientId()) {
            return c.ci;
        }
    }

    throw QString("exception");
}

Client SecurityManager::findClientByClientId(int clientId) {
    for(auto c : clients){
        if(clientId == c.client.getClientId()) {
            return c.client;
        }
    }

    throw QString("exception");
}

//Check that the incoming clientId comes from correct origin
bool SecurityManager::checkMessageOrigin(ConnectionInterface *ci, int clientId) {
    for(clientInfo c : clients) {
        if(c.ci == ci && c.client.getClientId() == clientId)
            return true;
    }
    return false;
}

void SecurityManager::updateClientInfo(int clientId, QString newNickname, uint newIconNumber) {
    for(clientInfo &c : clients) {
        if(c.client.getClientId() == clientId) {
            c.client.setNickname(newNickname);
            c.client.setIconNumber(newIconNumber);
            return;
        }
    }

    throw QString("exception");
}

void SecurityManager::resetClientPosition(int clientId) {
    for(clientInfo &c : clients) {
        if(c.client.getClientId() == clientId) {
            FracPos fp;
            fp.fracPos.push_back(INT_MIN);
            c.client.setCursorPosition(fp);
            return;
        }
    }

    throw QString("exception");
}
