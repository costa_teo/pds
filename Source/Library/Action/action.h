#ifndef ACTION_H
#define ACTION_H

#include "../Symbol/symbol.h"
#include <iostream>
#include <string>

using namespace std;

enum ActionType { Cancel, Insert, Change };

class InternalAction{
public:
    ActionType actionType;
    unsigned int position;
    InternalSymbol symbol;

    InternalAction(ActionType actionType,
                   unsigned int position,
                   InternalSymbol s): actionType(actionType), position(position), symbol(s){}

    ActionType getType(){
        return actionType;
    }

    string getStringType(){
        if(actionType == Insert){
            return string("insert");
        }else if(actionType == Cancel){
            return string("cancel");
        }else {
            return string("change");
        }
    }

    unsigned int getPosition(){
        return position;
    }

    InternalSymbol getSymbol(){
        return symbol;
    }

    void setPos(int pos){
        position = static_cast<unsigned int>(pos);
    }

};


class ExternalAction{
    Symbol s;
    ActionType actionType;
public:
    ExternalAction(Symbol s, ActionType actionType): s(s), actionType(actionType){}

    explicit ExternalAction(QJsonValue &obj) {
        s = Symbol(obj);

        if(obj.toObject().find("aType").value().toInt() == Cancel){
            actionType = Cancel;
        }
        else if(obj.toObject().find("aType").value().toInt() == Insert){
            actionType = Insert;
        }
        else if(obj.toObject().find("aType").value().toInt() == Change){
            actionType = Change;
        }
    }

    ActionType getActionType(){
        return actionType;
    }

    Symbol getSymbol(){
        return s;
    }

};

#endif // ACTION_H
