#ifndef FRACPOS_H
#define FRACPOS_H

#include <vector>
#include <limits>
#include <QJsonObject>
#include <QJsonArray>

using namespace std;

class FracPos {
public:
  vector<int> fracPos;

public:

  FracPos(){

  }

  FracPos(QJsonArray fracPosArray){
      for(QJsonValue num : fracPosArray){
          fracPos.push_back(num.toInt());
      }
  }

  FracPos(const FracPos &left, int client_id, const FracPos &right){
       int l, r;
       int lastLeft = 0;
       int lastRight = 0;
    for (unsigned int i = 0; i < max(left.fracPos.size(), right.fracPos.size())+1; i++) {
           l = (left.fracPos.size()  > i) ? left.fracPos[i] : 0;
           r = (right.fracPos.size() > i) ? right.fracPos[i] : 0;

           int pos = l;

           if(lastLeft<lastRight || l+1<r){
               pos = l+1;
           }
           fracPos.push_back(pos);
           if (pos != l)                   //finish
               break;

           lastLeft  = l;
           lastRight = r;
       }
       fracPos.push_back(client_id); //tie breaker!

  }

  FracPos(const FracPos &left, int client_id){
       int pos = left.fracPos[0]+1;
       fracPos.push_back(pos);
       fracPos.push_back(client_id);
  }
  FracPos(int client_id, const FracPos &right){//insert in 0, only right exists
      int r;

      for (unsigned int i = 0; i < right.fracPos.size(); i++) {

          r = right.fracPos[i];
          int pos = 0;
          if(r>std::numeric_limits<int>::min() ){//min int
              pos = r-1;
          }else{
              //TODO exception
          }
          fracPos.push_back(pos);
          if (pos != r)                   //finish
              break;
      }
          fracPos.push_back(client_id); //tie breaker!

  }

  FracPos(int client_id){//first char
      fracPos.push_back(1);
      fracPos.push_back(client_id);
  }


  bool operator<(const FracPos &that) const {
       bool result = this->fracPos < that.fracPos;
        return result;
  }

  QJsonArray toJson(){
      QJsonArray retu;
      for(int pos : fracPos){
          retu.push_back(pos);
      }
      return retu;
  }

  bool operator==(const FracPos& that) const{
      return this->fracPos == that.fracPos;
  }


  bool operator!=(const FracPos& that) const{
      return this->fracPos != that.fracPos;
  }


};

#endif // FRACPOS_H
