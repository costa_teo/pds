#ifndef MODEL_H
#define MODEL_H

#include <map>
#include <list>
#include "../Library/FracPos/fracpos.h"
#include "../Library/Action/action.h"
#include "../Client/client.h"
#include "../Symbol/symbol.h"


using namespace std;

class Model {
    QString documentName;
    QString URI;
    int client_id;  //Only for client use
    map<FracPos, Symbol> symbols;
    std::vector<Client> activeClients;

public:
    explicit Model(){
        client_id = -1;
    }

    Model(QString documentName, QString Uri, int client_id)
        : documentName(documentName),
          URI(Uri),
          client_id(client_id){}


    /**
     * @brief Model only for Server
     * @param ExternalAction is initialized, pass a list of symbols without fracpos initialization
     */
    Model(QString documentName, QString URI, vector<InternalSymbol> initialText, vector<ExternalAction> &ex):
        documentName(documentName),
        URI(URI){

        ex.clear();
        vector<Symbol> v;
        if(!initialText.empty()){
            auto it = initialText.begin();
            v.push_back(Symbol(*it, 0));
            it++;
            for(; it != initialText.end(); it++){
                Symbol s(v.back(), *it, 0);
                v.push_back(s);
            }

            for (auto s : v) {
                ex.push_back(ExternalAction(s, Insert));
            }
            makeAction(ex);
        }
    }

    vector<pair<FracPos, QString>> getPositionColor(){
        vector<pair<FracPos, QString>> retu;
        for(Client c : activeClients){
            retu.push_back(pair<FracPos, QString>(c.getCursorPosition(), c.getHTMLImage()));
        }

        return retu;
    }

    vector<FracPos> getClientsPositions(){
        vector<FracPos> retu;
        for(Client c : activeClients){
            retu.push_back(c.getCursorPosition());
        }

        return retu;
    }

    vector<ExternalAction> getActionsNewClient(){
        vector<ExternalAction> actionsToNewClients;
        for (auto s : symbols) {
            actionsToNewClients.push_back(ExternalAction(s.second, ActionType::Insert));
        }
        return actionsToNewClients;
    }

    QString getDocumentName(){
        return this->documentName;
    }

    void setDocumentName(QString name) {
        documentName = name;
    }

    int getClientId(){
        return client_id;
    }

    bool isValid(){
        return client_id == -1;
    }

    void setClientId(int clientId){
        this->client_id = clientId;
    }
    QString getURI() {
        return URI;
    }

    void setURI(QString newURI) {
        URI = newURI;
    }

    std::vector<Client> getActiveClients() {
        return activeClients;
    }

    void setActiveClients(std::vector<Client> &clients) {
        this->activeClients = clients;
    }

    void addActiveClient(Client newClient) {
        if(containsClient(newClient.getClientId()))
            return;

        activeClients.push_back(newClient);
    }

    void updateActiveClient(uint pos, Client newClient) {
        if(!containsClient(newClient.getClientId()))
            return;

        activeClients[pos] = newClient;
    }

    void removeClient(int client) {
        auto it = std::find_if(activeClients.begin(), activeClients.end(), [client](Client el){
            return el.getClientId() == client;
        });

        if(it != activeClients.end()) {
            activeClients.erase(it);
        }
    }

    bool containsClient(int client) {
        auto it = std::find_if(activeClients.begin(), activeClients.end(), [client](Client el){
            return el.getClientId() == client;
        });

        return it != activeClients.end();
    }

    void updateClient(int clientId, FracPos position) {
        for(Client &c : activeClients) {
            if(c.getClientId() == clientId) {
                c.setCursorPosition(position);
                break;
            }
        }
    }

    vector<Symbol> getSymbols(){
        vector<Symbol> retu;
        for(auto s : symbols)
            retu.push_back(s.second);
        return retu;
    }

    vector<InternalSymbol> getInternalSymbols(){
        vector<InternalSymbol> retu;
        for(auto s : symbols)
            retu.push_back(InternalSymbol(s.second));
        return retu;
    }

    vector<ExternalAction> makeAction(list<InternalAction> internalAction, QString backColor){
        list<Symbol> tmp;

        for (auto it = symbols.begin(); it != symbols.end(); it++) {
            tmp.push_back(it->second);
        }

        //end initialization of tmp

        vector<ExternalAction> externalActions;

        for (InternalAction a : internalAction) { //manage actions

            if (a.actionType == Insert) {
                if(tmp.size()==0){ //first characher
                    Symbol s(a.getSymbol(), client_id);
                    s.setBackGroundColor(backColor);
                    tmp.push_back(s);
                    externalActions.push_back(ExternalAction(s, Insert));
                }else if(a.position == 0){//first position
                    auto onTheRight = tmp.begin();
                    Symbol s(a.getSymbol(), client_id, *onTheRight);
                    s.setBackGroundColor(backColor);
                    tmp.insert(tmp.begin(),s);
                    externalActions.push_back(ExternalAction(s, Insert));
                }else if(a.position < tmp.size()){// in the middle
                    auto onTheLeft = tmp.begin();
                    for (unsigned int i = 1; i<a.position; i++)
                        onTheLeft++;
                    auto onTheRight = onTheLeft;
                    onTheRight++;
                    Symbol s(*onTheLeft, a.getSymbol(), client_id, *onTheRight);
                    s.setBackGroundColor(backColor);
                    tmp.insert(onTheRight,s);
                    externalActions.push_back(ExternalAction(s, Insert));

                }else if (a.position == tmp.size()){ //insert at the end
                    auto onTheLeft = tmp.back();
                    Symbol s(onTheLeft, a.getSymbol(), client_id);
                    s.setBackGroundColor(backColor);
                    tmp.push_back(s);
                    externalActions.push_back(ExternalAction(s, Insert));
                }



            }else if( a.actionType == Cancel && a.position < tmp.size()){
                auto toCancel = tmp.begin();
                for (unsigned int i = 0; i<a.position; i++) toCancel++;

                externalActions.push_back(ExternalAction(*toCancel, Cancel));
                tmp.erase(toCancel);

            }else if(a.actionType == Change && a.position < tmp.size()) {
                auto toChange = tmp.begin();
                for (unsigned int i = 0; i<a.position; i++) toChange++;

                (*toChange).setSymbol(a.symbol);
                externalActions.push_back(ExternalAction(*toChange, Change));
            }
        }
        makeAction(externalActions);
        return externalActions;
    }

    void makeAction(vector<ExternalAction> externalActions){
        for (auto action : externalActions) {
            Symbol s = action.getSymbol();
            if(action.getActionType() == Cancel){
                auto it = symbols.find(s.getFracPos());
                if(it != symbols.end()){
                    symbols.erase(it);
                }
            }
            else if(action.getActionType() == Insert){
                pair<FracPos, Symbol> p(s.getFracPos(), s);
                symbols.insert(p);
            }
            else if(action.getActionType() == Change){
                auto it = symbols.find(s.getFracPos());
                if(it != symbols.end()){
                    it->second.setSymbol(s);
                }
            }

        }

    }

};

#endif // MODEL_H
