#ifndef CONSTANTS_H
#define CONSTANTS_H

#include <iostream>
#include <vector>
#include <QString>
namespace CONSTANTS {
    const int MAX_USER_INPUT_LENGHT = 24;
}
static const std::vector<QString> COLORS{
    "#FFFFFF",  //WHITE
    "#FF6666",
    "#FFB266",
    "#FFFF66",
    "#B2FF66",
    "#66FF66",
    "#66FFB2",
    "#66FFFF",
    "#66B2FF",
    "#6666FF",
    "#B266FF",
    "#FF66FF",
    "#FF66B2",  //12th
    "#FF0000",
    "#FF8000",
    "#FFFF00",
    "#80FF00",
    "#00FF00",
    "#00FF80",
    "#00FFFF",
    "#0080FF",
    "#0000FF",
    "#7F00FF",
    "#FF00FF",
    "#FF007F",  //24th
};

static const std::vector<QString> ACCOUNT_ICON {
    ":/qml/images/icon/default.png",                //0th
    ":/qml/images/icon/alligatore.png",
    ":/qml/images/icon/ape.png",
    ":/qml/images/icon/balena.png",
    ":/qml/images/icon/birb.png",
    ":/qml/images/icon/bradipo.png",
    ":/qml/images/icon/bruco.png",
    ":/qml/images/icon/calabrone.png",
    ":/qml/images/icon/cammello.png",
    ":/qml/images/icon/cane.png",
    ":/qml/images/icon/canguro.png",                //10th
    ":/qml/images/icon/castoro.png",
    ":/qml/images/icon/cavalletta.png",
    ":/qml/images/icon/cavallo.png",
    ":/qml/images/icon/cervo.png",
    ":/qml/images/icon/chiocciola.png",
    ":/qml/images/icon/cigno.png",
    ":/qml/images/icon/coccinella.png",
    ":/qml/images/icon/colibri.png",
    ":/qml/images/icon/colomba.png",
    ":/qml/images/icon/coniglio.png",               //20th
    ":/qml/images/icon/delfino.png",
    ":/qml/images/icon/dinosauro.png",
    ":/qml/images/icon/drago.png",
    ":/qml/images/icon/elefante.png",
    ":/qml/images/icon/falco.png",
    ":/qml/images/icon/farfalla.png",
    ":/qml/images/icon/fenice.png",
    ":/qml/images/icon/fenicottero.png",
    ":/qml/images/icon/foca.png",
    ":/qml/images/icon/formica.png",                //30th
    ":/qml/images/icon/fratercula.png",
    ":/qml/images/icon/gabbiano.png",
    ":/qml/images/icon/gambero.png",
    ":/qml/images/icon/gatto.png",
    ":/qml/images/icon/giraffa.png",
    ":/qml/images/icon/gnu.png",
    ":/qml/images/icon/gorilla.png",
    ":/qml/images/icon/granchio.png",
    ":/qml/images/icon/gufo.png",
    ":/qml/images/icon/insetto.png",                //40th
    ":/qml/images/icon/ippocampo.png",
    ":/qml/images/icon/kiwi.png",
    ":/qml/images/icon/lama.png",
    ":/qml/images/icon/lemur.png",
    ":/qml/images/icon/leone.png",
    ":/qml/images/icon/leopardo.png",
    ":/qml/images/icon/libellula.png",
    ":/qml/images/icon/lupo.png",
    ":/qml/images/icon/maiale.png",
    ":/qml/images/icon/mucca.png",                  //50th
    ":/qml/images/icon/orca.png",
    ":/qml/images/icon/orso.png",
    ":/qml/images/icon/panda.png",
    ":/qml/images/icon/papera.png",
    ":/qml/images/icon/pappagallo.png",
    ":/qml/images/icon/pavone.png",
    ":/qml/images/icon/pecora.png",
    ":/qml/images/icon/pellicano.png",
    ":/qml/images/icon/pesce.png",
    ":/qml/images/icon/pescepagliaccio.png",        //60th
    ":/qml/images/icon/pipistrello.png",
    ":/qml/images/icon/polipo.png",
    ":/qml/images/icon/pollo.png",
    ":/qml/images/icon/ragno.png",
    ":/qml/images/icon/rana.png",
    ":/qml/images/icon/rinoceronte.png",
    ":/qml/images/icon/scoiattolo.png",
    ":/qml/images/icon/serpente.png",
    ":/qml/images/icon/stellamarina.png",
    ":/qml/images/icon/suricata.png",               //70th
    ":/qml/images/icon/tartaruga.png",
    ":/qml/images/icon/tasso.png",
    ":/qml/images/icon/topo.png",
    ":/qml/images/icon/toro.png",
    ":/qml/images/icon/tricheco.png",
    ":/qml/images/icon/trilobite.png",
    ":/qml/images/icon/unicorno.png",
    ":/qml/images/icon/verme.png",
    ":/qml/images/icon/volpe.png",
    ":/qml/images/icon/zebra.png",                  //80th
};

namespace COMMON_MESSAGES {
    //ERROR
    const QString E_OPEN = "Error during Open operation";
    const QString E_OPEN_URI = "Error during Open by URI operation";
    const QString E_CREATE = "Error during Create operation";
    const QString E_DELETE = "Error during Delete operation";
    const QString E_LIST = "Error during List operation";
    const QString E_LOGIN = "Error during Login operation";
    const QString E_NEW_ACC = "Error during New Account operation";
    const QString E_DELETE_ACC = "Error during Delete Account operation";
    const QString E_UPDATE_ACC = "Error during Update Account operation";
    const QString E_LOGOUT = "Error during Logout operation";
    const QString E_DENIED = "Access denied to requested resource";
    const QString E_NOT_FOUND = "Requested resource not found";
    const QString E_NOT_OPENED = "Requested resource is not already opened by Client";
    const QString E_BAD_REQUEST = "Unexpected request received by Server";
    const QString E_ALREADY_LOGGED = "User already logged in an other Client Application";
    const QString E_WRONG_CREDENTIALS = "Wrong credentials inserted";
    const QString E_MISSING_CREDENTIALS = "Some required credentials are missing";
    const QString E_NOT_LOGGED = "Client is not logged";
    const QString E_STILL_LOGGED = "Client must not be logged to perform required operation";
    const QString E_INVALID_USER = "Requested username has already been taken";
    const QString E_INVALID_DOCUMENT = "Requested document name has already been taken";
    const QString E_INVALID_URI = "Wrong URI";
    const QString E_INTERNAL = "Internal Server error. Try Again";

    //INFO
    const QString I_DELETED = "File correctly deleted";
    const QString I_NO_FILE = "No file available";
    const QString I_ACC_CREATED = "Account correctly created";
    const QString I_ACC_DELETED = "Account correctly deleted";

    //GUI MESSAGE
    const QString G_WELCOME = "<!DOCTYPE html><html><body><H1>Open an existing file or create a new one</H1></body></html>";
    const QString G_NO_TITLE = "No document";
    const QString G_NO_LOGGED = "Not logged";

    //DIALOG MESSAGES
    const QString D_DELETE_FILE_MESSAGE = "Are you sure to delete the document?";
    const QString D_CREATE_FILE_MESSAGE = "Document name";
    const QString D_SHARE_FILE_MESSAGE = "Copy the URI and share it!";
    const QString D_OPEN_URI_FILE_MESSAGE = "Insert URI";
    const QString D_OPEN_FILE_MESSAGE = "File list:";
    const QString D_CREATE_ACCOUNT_MESSAGE = "";
    const QString D_DELETE_ACCOUNT_MESSAGE_USER = "Insert username";
    const QString D_DELETE_ACCOUNT_MESSAGE_PASS = "Insert password";
    const QString D_CONNECTION_ERROR_MESSAGE  = "Unable to connect to the server";

    //DIALOG TITLES
    const QString D_DELETE_FILE_TITLE = "Delete document";
    const QString D_CREATE_FILE_TITLE = "New document";
    const QString D_SHARE_FILE_TITLE = "Share document";
    const QString D_OPEN_URI_FILE_TITLE = "Open document";
    const QString D_OPEN_FILE_TITLE = "Select document";
    const QString D_DELETE_ACCOUNT_TITLE = "Delete account";
    const QString D_CREATE_ACCOUNT_TITLE = "Create a new account";
    const QString D_LOGIN_TITLE  = "Login";
    const QString D_CONNECTION_ERROR_TITLE  = "Connection Error";
    const QString D_IMAGES_TITLE = "Images";

    //BUTTONS
    const QString B_CREATE = "New Account";
    const QString B_CANCEL = "Cancel";
    const QString B_OK = "Ok";
    const QString B_LOGIN = "Login";
    const QString B_RETRY = "Retry";
    const QString B_SELECT_ICON = "Select icon";
    const QString B_CHANGE_ICON = "Change icon";

    //LABLE
    const QString L_USERNAME = "Username";
    const QString L_NICKNAME = "Nickname";
    const QString L_PASSWORD = "Password";
    const QString L_PROFILE  = "User Profile";
    const QString L_CHANGE_NICKNAME = "Change Nickname";
    const QString L_CHANGE_PASSWORD = "Change Password";

    //QUESTIONS
    const QString Q_RETRY  = "Retry?";

}

#endif // CONSTANTS_H
