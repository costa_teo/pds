#ifndef SYMBOL_H
#define SYMBOL_H

#include "../FracPos/fracpos.h"
#include <QTextCharFormat>
#include <Qt>

class Symbol;

class InternalSymbol{
    QChar c;
    Qt::Alignment alignment;
    QString family;
    bool bold;
    bool italic;
    bool underline;
    int size;
    QString color;
    QString backgroundColor = "#ffffff";
public:

    InternalSymbol(Symbol);

    InternalSymbol(QChar c, QFont font, QString color, Qt::Alignment alignment) : c(c), alignment(alignment){
        bold = font.bold();
        italic = font.italic();
        underline = font.underline();
        size = font.pointSize();
        family = font.family();
        this->color = color;
    }

    InternalSymbol(QChar c, bool bold, bool italic, bool underline, int size, QString family, QString color, Qt::Alignment alignment) : c(c), alignment(alignment){
        this->bold = bold;
        this->italic = italic;
        this->underline = underline;
        this->size = size;
        this->family = family;
        this->color = color;
    }

    explicit InternalSymbol(QJsonValue &obj) {
        QJsonObject symbolObject = obj.toObject();

        std::string cbiu = symbolObject.find("cbiu").value().toString().toStdString();

        cbiu.at(0) == '1' ? bold = true : bold = false;
        cbiu.at(1) == '1' ? italic = true : italic = false;
        cbiu.at(2) == '1' ? underline = true : underline = false;

        size = symbolObject.find("fSize").value().toInt();
        family = symbolObject.find("family").value().toString();

        alignment = Qt::AlignLeft;
        if(symbolObject.find("al").value().toInt() == Qt::AlignLeft){
            alignment = Qt::AlignLeft;
        }
        if(symbolObject.find("al").value().toInt() == Qt::AlignHCenter){
            alignment = Qt::AlignHCenter;
        }
        else if(symbolObject.find("al").value().toInt() == Qt::AlignRight){
            alignment = Qt::AlignRight;
        }

        c = symbolObject.find("letter").value().toString()[0];

        color = QString::fromStdString(cbiu.substr(3, 7));

        backgroundColor = symbolObject.find("bgcolor").value().toString();
    }

    QChar getC(){
        return c;
    }

    void setBackgroundColor(QString backgroundColor){
       this->backgroundColor = backgroundColor;
    }

    QString getBackgroundColor(){
        return backgroundColor;
    }
    void setColor(QString color){
       this->color = color;
    }

    QString getColor(){
        return color;
    }

    void setFamily(QString family){
       this->family = family;
    }

    QString getFamily(){
        return family;
    }

    void setSize(int size){
       this->size = size;
    }

    int getSize(){
        return size;
    }

    void setBold(bool tf){
       bold = tf;
    }
    bool getBold(){
        return bold;
    }

    void setItalic(bool tf){
       italic = tf;
    }

    bool getItalic(){
        return italic;
    }

    void setUnderline(bool tf){
       underline = tf;
    }

    bool getUnderline(){
        return underline;
    }

    Qt::Alignment getAlignment(){
        return alignment;
    }

    void setAlignment(Qt::Alignment alignment){
        this->alignment = alignment;
    }

    bool operator==(const InternalSymbol that) const{
        if(this->c != that.c ||
                this->bold != that.bold ||
                this->italic != that.italic ||
                this->underline != that.underline ||
                this->size != that.size ||
                this->family.compare(that.family) != 0 ||
                this->color.compare(that.color) != 0 ||
                this->alignment != that.alignment ){
            return false;
        }
        return true;
    }

    bool operator!=(const InternalSymbol& that) const{
        if(this->c != that.c ||
                this->bold != that.bold ||
                this->italic != that.italic ||
                this->underline != that.underline ||
                this->size != that.size ||
                this->family.compare(that.family) != 0 ||
                this->color.compare(that.color) != 0 ||
                this->alignment != that.alignment ){
            return true;
        }
        return false;
    }

};

class Symbol {
    QChar c;
    Qt::Alignment alignment;
    QString family;
    bool bold;
    bool italic;
    bool underline;
    int size;
    QString color;
    QString backgroundColor = "#ffffff";

protected:
    FracPos position;
public:

    explicit Symbol(const Symbol &left, InternalSymbol is, int client_id, const Symbol &right) :c(is.getC()),
        alignment(is.getAlignment()),
        position(left.position, client_id, right.position) {
        bold = is.getBold();
        italic = is.getItalic();
        underline = is.getUnderline();
        size = is.getSize();
        family = is.getFamily();
        color = is.getColor();
        backgroundColor = is.getBackgroundColor();
    }


    explicit Symbol(InternalSymbol is, int client_id, Symbol right) :
        c(is.getC()),
        alignment(is.getAlignment()),
        position(client_id, right.position){
        bold = is.getBold();
        italic = is.getItalic();
        underline = is.getUnderline();
        size = is.getSize();
        family = is.getFamily();
        color = is.getColor();
        backgroundColor = is.getBackgroundColor();
    }

    explicit Symbol(Symbol left, InternalSymbol is, int client_id) :
        c(is.getC()),
        alignment(is.getAlignment()),
        position(left.position, client_id){
        bold = is.getBold();
        italic = is.getItalic();
        underline = is.getUnderline();
        size = is.getSize();
        family = is.getFamily();
        color = is.getColor();
        backgroundColor = is.getBackgroundColor();
    }


    explicit Symbol(InternalSymbol is, int client_id) :
        c(is.getC()),
        alignment(is.getAlignment()),
        position(client_id){
        bold = is.getBold();
        italic = is.getItalic();
        underline = is.getUnderline();
        size = is.getSize();
        family = is.getFamily();
        color = is.getColor();
        backgroundColor = is.getBackgroundColor();
    }

    explicit Symbol(QJsonValue &obj) {
        QJsonArray fracPos = obj.toObject().find("pos").value().toArray();
        position = FracPos(fracPos);

        std::string cbiu = obj.toObject().find("cbiu").value().toString().toStdString();

        QString string = obj.toObject().find("letter").value().toString();
        c = string.at(0);

        cbiu.at(0) == '1' ? (bold = true) : (bold = false);
        cbiu.at(1) == '1' ? (italic = true) : (italic = false);
        cbiu.at(2) == '1' ? (underline = true) : (underline = false);

        size = obj.toObject().find("fSize").value().toInt();
        family = obj.toObject().find("family").value().toString();

        if(obj.toObject().find("al").value().toInt() == Qt::AlignLeft){
            alignment = Qt::AlignLeft;
        }
        if(obj.toObject().find("al").value().toInt() == Qt::AlignHCenter){
            alignment = Qt::AlignHCenter;
        }
        else if(obj.toObject().find("al").value().toInt() == Qt::AlignRight){
            alignment = Qt::AlignRight;
        }

        color = QString::fromStdString(cbiu.substr(3, 7));
        backgroundColor = obj.toObject().find("bgcolor").value().toString();
    }

    void setSymbol(InternalSymbol i){
        this->c = i.getC();
        this->bold = i.getBold();
        this->italic = i.getItalic();
        this->underline = i.getUnderline();
        this->alignment = i.getAlignment();
        this->size = i.getSize();
        this->family = i.getFamily();
        this->color = i.getColor();
        backgroundColor = i.getBackgroundColor();
    }

    void setSymbol(Symbol i){
        this->c = i.getC();
        this->bold = i.getBold();
        this->italic = i.getItalic();
        this->underline = i.getUnderline();
        this->alignment = i.getAlignment();
        this->size = i.getSize();
        this->family = i.getFamily();
        this->color = i.getColor();
    }
    void setColor(QString color){
       this->color = color;
    }
    QString getColor(){
        return color;
    }
    void setBackGroundColor(QString color) {
        this->backgroundColor = color;
    }
    QString getBackGroundColor() {
        return backgroundColor;
    }
    void setFamily(QString family){
       this->family = family;
    }
    QString getFamily(){
        return family;
    }
    void setSize(int size){
       this->size = size;
    }
    int getSize(){
        return size;
    }
    void setBold(bool tf){
       bold = tf;
    }
    bool getBold(){
        return bold;
    }
    void setItalic(bool tf){
       italic = tf;
    }
    bool getItalic(){
        return italic;
    }
    void setUnderline(bool tf){
       underline = tf;
    }
    bool getUnderline(){
        return underline;
    }
    void setC(QChar c){
        this->c=c;
    }

    FracPos getFracPos(){
        return position;
    }
    void setFracPos(FracPos& position){
        this->position = position;
    }
    QChar getC(){
        return c;
    }

    Qt::Alignment getAlignment(){
        return alignment;
    }
    void setAlignment(Qt::Alignment alignment){
        this->alignment = alignment;
    }

    bool operator<(Symbol &that){
     return this->position < that.position;
    }

    Symbol(){}

    explicit Symbol(QChar c, int client_id, Symbol right, QFont f, QString color, Qt::Alignment a) : c(c),
        alignment(a),
        position(client_id, right.position){
        this->bold = f.bold();
        this->italic = f.italic();
        this->underline = f.underline();
        this->size = f.pointSize();
        this->family = f.family();
        this->color = color;
    }

    explicit Symbol(Symbol left, QChar c, int client_id, QFont f, QString color, Qt::Alignment a) : c(c),
        alignment(a),
        position(left.position, client_id){
        this->bold = f.bold();
        this->italic = f.italic();
        this->underline = f.underline();
        this->size = f.pointSize();
        this->family = f.family();
        this->color = color;
    }

    explicit Symbol(QChar c, FracPos fracPos, QFont f, QString color, Qt::Alignment a) : c(c),
        alignment(a),
        position(fracPos){
        this->bold = f.bold();
        this->italic = f.italic();
        this->underline = f.underline();
        this->size = f.pointSize();
        this->family = f.family();
        this->color = color;
    }

    explicit Symbol(QChar c, int client_id, QFont f, QString color, Qt::Alignment a) : c(c),
        alignment(a),
        position(client_id){
        this->bold = f.bold();
        this->italic = f.italic();
        this->underline = f.underline();
        this->size = f.pointSize();
        this->family = f.family();
        this->color = color;
    }
//--------------------------------------------------------------
    explicit Symbol(const Symbol &left, QChar c, int client_id, const Symbol &right, bool bold, bool italic, bool underline, int size, QString family, QString color, Qt::Alignment a) :
        c(c),
        alignment(a),
        position(left.position, client_id, right.position) {
        this->bold = bold;
        this->italic = italic;
        this->underline = underline;
        this->size = size;
        this->family = family;
        this->color = color;
    }


    explicit Symbol(QChar c, int client_id, Symbol right, bool bold, bool italic, bool underline, int size, QString family, QString color, Qt::Alignment a) : c(c),
        alignment(a),
        position(client_id, right.position){
        this->bold = bold;
        this->italic = italic;
        this->underline = underline;
        this->size = size;
        this->family = family;
        this->color = color;
    }

    explicit Symbol(Symbol left, QChar c, int client_id, bool bold, bool italic, bool underline, int size, QString family, QString color, Qt::Alignment a) : c(c),
        alignment(a),
        position(left.position, client_id){
        this->bold = bold;
        this->italic = italic;
        this->underline = underline;
        this->size = size;
        this->family = family;
        this->color = color;
    }

    explicit Symbol(QChar c, FracPos fracPos, bool bold, bool italic, bool underline, int size, QString family, QString color, Qt::Alignment a) : c(c),
        alignment(a),
        position(fracPos){
        this->bold = bold;
        this->italic = italic;
        this->underline = underline;
        this->size = size;
        this->family = family;
        this->color = color;
    }

    explicit Symbol(QChar c, int client_id, bool bold, bool italic, bool underline, int size, QString family, QString color, Qt::Alignment a) : c(c),
        alignment(a),
        position(client_id){
        this->bold = bold;
        this->italic = italic;
        this->underline = underline;
        this->size = size;
        this->family = family;
        this->color = color;
    }


    bool areSameFontAndChar(const Symbol &that){
        if(this->c == that.c &&
                this->bold == that.bold &&
                this->italic == that.italic &&
                this->underline == that.underline &&
                this->size == that.size &&
                this->family.compare(that.family) == 0 &&
                this->color.compare(that.color) == 0 &&
                this->alignment == that.alignment)
            return true;
        return  false;
    }

    //Symbol &operator=(const Symbol &that)=delete;

};

#endif // SYMBOL_H
