#include "symbol.h"

InternalSymbol::InternalSymbol(Symbol s):
    c(s.getC()),
    alignment(s.getAlignment()){
    this->bold = s.getBold();
    this->italic = s.getItalic();
    this->underline = s.getUnderline();
    this->size = s.getSize();
    this->family = s.getFamily();
    this->color = s.getColor();
    this->backgroundColor = s.getBackGroundColor();
}
