#ifndef LISTDOCUMENTMESSAGE_H
#define LISTDOCUMENTMESSAGE_H

#include "message.h"
#include <vector>

class ListDocumentMessage : public Message
{
public:
    std::vector<QString> documents;

    explicit ListDocumentMessage() : Message(T_ListDocumentMessage) {}

    explicit ListDocumentMessage(int id) : Message(id, T_ListDocumentMessage) {}

    explicit ListDocumentMessage(const std::vector<QString> &documentsToInsert) : Message(T_ListDocumentMessage) {
        for(QString document : documentsToInsert)
            documents.push_back(document);
    }

    explicit ListDocumentMessage(QJsonObject &obj) : Message(T_ListDocumentMessage) {

        clientId = obj.find("clientId").value().toString().toInt();

        for(QJsonValue document : obj.find("documents").value().toArray()){
            documents.push_back(document.toString());
        }
    }

    QJsonDocument toJsonMessage(){
        QJsonObject jsonMessage = QJsonObject();

        jsonMessage.insert("clientId", QString::number(this->clientId));
        jsonMessage.insert("messageType", int(this->messageType));
        QJsonArray documents;
        for(QString string : this->documents)
            documents.push_back(string);

        jsonMessage.insert("documents", documents);

        QJsonDocument json(jsonMessage);
        return json;
    }

    void log() const {
        std::cerr << "Message details: id = " << clientId << " - Document number = " << documents.size() << "\n";
    }
};

#endif // LISTDOCUMENTMESSAGE_H
