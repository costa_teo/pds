#ifndef ACTIVECLIENTMESSAGE_H
#define ACTIVECLIENTMESSAGE_H

#include "message.h"
#include "../Library/Client/client.h"
#include <vector>

enum ActiveClientOperation { AddClient, DeleteClient, UpdateClient };

class ActiveClientMessage: public Message
{
public:
    std::vector<Client> activeClients;
    ActiveClientOperation activeClientOperation;

    explicit ActiveClientMessage(): Message(T_ActiveClientMessage){}

    explicit ActiveClientMessage(int id): Message(id, T_ActiveClientMessage){}

    explicit ActiveClientMessage(int id, std::vector<Client> activeClients):
        Message(id, T_ActiveClientMessage),
        activeClients(activeClients){}

    explicit ActiveClientMessage(QJsonObject &obj): Message(T_ActiveClientMessage){

        clientId = obj.find("clientId").value().toString().toInt();

        if(obj.find("activeClientOperation").value().toInt() == AddClient){
            activeClientOperation = AddClient;
        }
        else if(obj.find("activeClientOperation").value().toInt() == DeleteClient){
            activeClientOperation = DeleteClient;
        }
        else if(obj.find("activeClientOperation").value().toInt() == UpdateClient){
            activeClientOperation = UpdateClient;
        }

        Client c;
        for(QJsonValue client : obj.find("activeClients").value().toArray()){
            QJsonObject clientObject = client.toObject();
            c.setClientId(clientObject.find("clientId").value().toInt());
            c.setUsername(clientObject.find("username").value().toString());
            c.setCursorPosition(FracPos(clientObject.find("pos").value().toArray()));
            c.setBackgroundColor(static_cast<uint>(clientObject.find("img").value().toInt()));
            c.setNickname(clientObject.find("nick").value().toString());
            c.setIconNumber(static_cast<uint>(clientObject.find("icNum").value().toInt()));
            activeClients.push_back(c);
        }

    }


    QJsonDocument toJsonMessage(){
        QJsonObject jsonMessage = QJsonObject();

        jsonMessage.insert("clientId", QString::number(this->clientId));
        jsonMessage.insert("messageType", int(this->messageType));

        jsonMessage.insert("activeClientOperation", int(this->activeClientOperation));

        QJsonArray clients;
        for(auto client : this->activeClients){
            QJsonObject jsonClient = QJsonObject();
            jsonClient.insert("clientId", client.getClientId());
            jsonClient.insert("username", client.getUsername());
            QJsonArray fracPos;
            for(int pos : client.getCursorPosition().fracPos){
                fracPos.push_back(pos);
            }
            jsonClient.insert("pos", fracPos);

            jsonClient.insert("nick", client.getNickname());
            jsonClient.insert("icNum", static_cast<int>(client.getIconNumber()));

            jsonClient.insert("img", static_cast<int>(client.getBackgroundColor()));
            clients.push_back(jsonClient);
        }
        jsonMessage.insert("activeClients", clients);

        QJsonDocument json(jsonMessage);
        return json;
    }

    void log() const {
        std::cerr << "Message details: id = " << clientId << " - activeClient number = " << activeClients.size() <<  " - Op Type = " << activeClientOperation << "\n";
    }
};

#endif // ACTIVECLIENTMESSAGE_H
