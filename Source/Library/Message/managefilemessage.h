#ifndef MANAGEFILEMESSAGE_H
#define MANAGEFILEMESSAGE_H

#include "message.h"
#include <string>

enum OperationType { Open, OpenByURI, Close, Create, Delete};       //TODO eliminare CLOSE

class ManageFileMessage : public Message
{
public:
    QString document;
    QString URI;
    OperationType operationType;

    explicit ManageFileMessage() : Message(T_ManageFileMessage){}

    explicit ManageFileMessage(int id) : Message(id, T_ManageFileMessage){}

    explicit ManageFileMessage(int id, const OperationType& operationType, const QString& document, const QString& URI) :
        Message(id, T_ManageFileMessage),
        document(document),
        URI(URI),
        operationType(operationType){}

    explicit ManageFileMessage(QJsonObject &obj) : Message(T_ManageFileMessage) {

        clientId = obj.find("clientId").value().toString().toInt();
        document = obj.find("document")->toString();
        URI = obj.find("URI")->toString();

        if(obj.find("operationType").value().toInt() == Open){
            operationType = Open;
        }
        else if(obj.find("operationType").value().toInt() == Close){
            operationType = Close;
        }
        else if(obj.find("operationType").value().toInt() == Create){
            operationType = Create;
        }
        else if(obj.find("operationType").value().toInt() == Delete){
            operationType = Delete;
        }
        else if(obj.find("operationType").value().toInt() == OpenByURI){
            operationType = OpenByURI;
        }
    }

    QJsonDocument toJsonMessage(){
        QJsonObject jsonMessage = QJsonObject();

        jsonMessage.insert("clientId", QString::number(this->clientId));
        jsonMessage.insert("messageType", int(this->messageType));

        QJsonValue operationType = int(this->operationType);
        jsonMessage.insert("operationType", operationType);

        QJsonValue document = this->document;
        jsonMessage.insert("document", document);

        QJsonValue URI = this->URI;
        jsonMessage.insert("URI", URI);

        QJsonDocument json(jsonMessage);
        return json;
    }

    void log() const {
        std::cerr << "Message details: id = " << clientId << " - name = " << document.toStdString() << " - URI = " << URI.toStdString() << " - Op Type = " << operationType << "\n";
    }
};

#endif // MANAGEFILEMESSAGE_H
