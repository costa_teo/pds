#ifndef MESSAGE_H
#define MESSAGE_H

#include <iostream>
#include <QJsonObject>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonDocument>
#include <QJsonArray>

enum MessageType { T_ManageFileMessage, T_ListDocumentMessage, T_ErrorMessage, T_EditMessage, T_AccountMessage, T_InfoMessage, T_ActiveClientMessage};

class Message
{
public:
    const MessageType messageType;
    int clientId = -1;

    Message(MessageType messageType) : messageType(messageType){}

    Message(int clientId, MessageType messageType) : messageType(messageType), clientId(clientId){}

    virtual void log() const =0;
    virtual QJsonDocument toJsonMessage() =0;
    virtual ~Message(){}
};

#endif // MESSAGE_H
