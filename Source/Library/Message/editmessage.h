#ifndef EDITMESSAGE_H
#define EDITMESSAGE_H

#include "../Action/action.h"
#include "message.h"
#include <vector>

class EditMessage : public Message
{
public:
    QString document;
    std::vector<ExternalAction> externalActions;
    std::vector<pair<int, FracPos>> clientsCursors;

    explicit EditMessage():  Message(T_EditMessage){}

    explicit EditMessage(int id): Message(id, T_EditMessage){}

    explicit EditMessage(int id,
                         QString documentName,
                         std::vector<ExternalAction> externalActions,
                         std::vector<pair<int, FracPos>> clientsCursors):
        Message(id, T_EditMessage),
        document(documentName), externalActions(externalActions),
        clientsCursors(clientsCursors){}


    explicit EditMessage(QJsonObject &obj) : Message(T_EditMessage) {
        clientId = obj.find("clientId").value().toString().toInt();
        document = obj.find("doc").value().toString();

        QJsonArray externalActionsJSON = obj.find("exA").value().toArray();
        for(QJsonValue extAction : externalActionsJSON){
            ExternalAction e(extAction);
            externalActions.push_back(e);
        }

        QJsonArray cursorPositions = obj.find("cCurs").value().toArray();
        for (QJsonValue cPos : cursorPositions){
            pair<int, FracPos> p;
            p.first = cPos.toObject().find("int").value().toInt();
            p.second = FracPos(cPos.toObject().find("fPos").value().toArray());
            this->clientsCursors.push_back(p);
        }
    }

    QJsonDocument toJsonMessage(){
        QJsonObject jsonMessage = QJsonObject();
        jsonMessage.insert("clientId", QString::number(this->clientId));
        jsonMessage.insert("messageType", int(this->messageType));

        QJsonValue document = this->document;
        jsonMessage.insert("doc", document);

        QJsonArray externalActions;
        for(ExternalAction extAction : this->externalActions){
            QJsonObject jsonSymbol = QJsonObject();

            QJsonArray fracPos = extAction.getSymbol().getFracPos().toJson();
            jsonSymbol.insert("pos", fracPos);

            QByteArray cbiu = "";
            extAction.getSymbol().getBold() ? cbiu.append('1') : cbiu.append('0');
            extAction.getSymbol().getItalic() ? cbiu.append('1') : cbiu.append('0');
            extAction.getSymbol().getUnderline() ? cbiu.append('1') : cbiu.append('0');
            cbiu.append(extAction.getSymbol().getColor());
            jsonSymbol.insert("cbiu", QString(cbiu));

            jsonSymbol.insert("letter", QString(extAction.getSymbol().getC()));

            jsonSymbol.insert("family", QString(extAction.getSymbol().getFamily()));
            jsonSymbol.insert("fSize", extAction.getSymbol().getSize());
            jsonSymbol.insert("al", int(extAction.getSymbol().getAlignment()));
            jsonSymbol.insert("bgcolor", extAction.getSymbol().getBackGroundColor());

            jsonSymbol.insert("aType", int(extAction.getActionType()));

            externalActions.push_back(jsonSymbol);
        }
        jsonMessage.insert("exA", externalActions);

        QJsonArray JsonCursors;
        for(auto pair : this->clientsCursors){
            QJsonObject pairJson = QJsonObject();
            pairJson.insert("int", pair.first);
            QJsonArray fracPos = pair.second.toJson();
            pairJson.insert("fPos", fracPos);
            JsonCursors.push_back(pairJson);
        }

        jsonMessage.insert("cCurs", JsonCursors);

        QJsonDocument json(jsonMessage);
        return json;
    }

    void log() const {
        std::cerr << "Message details: id = " << clientId << " - name = " << document.toStdString() << " - External Actions number = " << externalActions.size() << " - Cursors numbers = " << clientsCursors.size() <<  "\n";
    }
};



#endif // EDITMESSAGE_H
