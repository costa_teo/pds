#ifndef INFOMESSAGE_H
#define INFOMESSAGE_H

#include "message.h"

class InfoMessage: public Message
{
public:
    QString message;

    explicit InfoMessage() : Message(T_InfoMessage){}

    explicit InfoMessage(int id, QString message): Message(id, T_InfoMessage), message(message){}

    explicit InfoMessage(QJsonObject &obj) : Message(T_InfoMessage) {

        clientId = obj.find("clientId").value().toString().toInt();
        message = obj.find("message").value().toString();
    }

    QJsonDocument toJsonMessage(){
        QJsonObject jsonMessage = QJsonObject();

        jsonMessage.insert("clientId", QString::number(this->clientId));
        jsonMessage.insert("messageType", int(this->messageType));

        jsonMessage.insert("message", this->message);

        QJsonDocument json(jsonMessage);
        return json;
    }

    void log() const {
        std::cerr << "Message details: id = " << clientId << " - message = " << message.toStdString() << "\n";
    }
};

#endif // INFOMESSAGE_H
