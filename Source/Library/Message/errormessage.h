#ifndef ERRORMESSAGE_H
#define ERRORMESSAGE_H

#include "message.h"

enum ErrorType { NoAction, NetworkError };

class ErrorMessage : public Message
{
public:
    QString message;
    ErrorType errorType;

    explicit ErrorMessage() : Message(T_ErrorMessage){}

    explicit ErrorMessage(int id) : Message(id, T_ErrorMessage){}

    explicit ErrorMessage(int id, ErrorType et, QString message): Message(id, T_ErrorMessage), message(message), errorType(et)  {}

    explicit ErrorMessage(QJsonObject &obj) : Message(T_ErrorMessage) {

        clientId = obj.find("clientId").value().toString().toInt();
        message = obj.find("message").value().toString();


        switch (obj.find("type").value().toInt()) {
        case NoAction:
            errorType = NoAction;
            break;

        case NetworkError:
            errorType = NetworkError;
            break;

        }
    }

    QJsonDocument toJsonMessage(){
        QJsonObject jsonMessage = QJsonObject();

        jsonMessage.insert("clientId", QString::number(this->clientId));
        jsonMessage.insert("messageType", int(this->messageType));

        QJsonValue message = this->message;
        jsonMessage.insert("message", message);

        jsonMessage.insert("type", int(this->errorType));

        QJsonDocument json(jsonMessage);
        return json;
    }

    void log() const {
        std::cerr << "Message details: id = " << clientId << " - message = " << message.toStdString() << " - Op Type = " << errorType << "\n";
    }
};

#endif // ERRORMESSAGE_H
