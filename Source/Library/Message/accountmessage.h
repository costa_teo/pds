#ifndef ACCOUNTMESSAGE_H
#define ACCOUNTMESSAGE_H

#include "message.h"

enum AccountOperationType {Login, NewAccount, DeleteAccount, Logout, UpdateAccount };

class AccountMessage : public Message
{
public:    
    QString username;
    QString nickname;
    QString password;
    uint backgroundColor;
    uint iconNumber;
    AccountOperationType accountOperationType;

    explicit AccountMessage() : Message(T_AccountMessage){}

    explicit AccountMessage(int id) : Message(id, T_AccountMessage){}

    explicit AccountMessage(int id, QString username, QString password, AccountOperationType accountOperationType) :
        Message(id, T_AccountMessage),
        username(username), password(password), accountOperationType(accountOperationType){}

    explicit AccountMessage(int id, QString username, QString nickname, QString password, uint iconNumber, uint backgroundColor, AccountOperationType accountOperationType) :
        Message(id, T_AccountMessage),
        username(username),
        nickname(nickname),
        password(password),
        backgroundColor(backgroundColor),
        iconNumber(iconNumber),
        accountOperationType(accountOperationType){}

    explicit AccountMessage(QJsonObject &obj) : Message(T_AccountMessage) {

        clientId = obj.find("clientId").value().toString().toInt();
        username = obj.find("username").value().toString();
        nickname = obj.find("nickname").value().toString();
        password = obj.find("password").value().toString();

        backgroundColor = static_cast<uint>(obj.find("color").value().toInt());
        iconNumber = static_cast<uint>(obj.find("iconNumber").value().toInt());

        if(obj.find("accountOperationType").value().toInt() == Login){
            accountOperationType = Login;
        }
        else if(obj.find("accountOperationType").value().toInt() == Logout){
            accountOperationType = Logout;
        }
        else if(obj.find("accountOperationType").value().toInt() == NewAccount){
            accountOperationType = NewAccount;
        }
        else if(obj.find("accountOperationType").value().toInt() == DeleteAccount){
            accountOperationType = DeleteAccount;
        }
        else if(obj.find("accountOperationType").value().toInt() == UpdateAccount){
            accountOperationType = UpdateAccount;
        }
    }

    void log() const {
        std::cerr << "Message details: id = " << clientId << " - username = " << username.toStdString() << " - nickname = " << nickname.toStdString() << " - password = " << password.toStdString() << " - account color id = " << backgroundColor << " - icon number = " << iconNumber << " - Op Type = " << accountOperationType << "\n";
    }

    QJsonDocument toJsonMessage(){
        QJsonObject jsonMessage = QJsonObject();

        jsonMessage.insert("clientId", QString::number(this->clientId));
        jsonMessage.insert("messageType", int(this->messageType));

        jsonMessage.insert("username", this->username);
        jsonMessage.insert("nickname", this->nickname);
        jsonMessage.insert("password", this->password);

        jsonMessage.insert("color", static_cast<int>(this->backgroundColor));

        jsonMessage.insert("iconNumber", static_cast<int>(this->iconNumber));

        QJsonValue accountOperationType = int(this->accountOperationType);
        jsonMessage.insert("accountOperationType", accountOperationType);

        QJsonDocument json(jsonMessage);
        return json;
    }
};

#endif // ACCOUNTMESSAGE_H
