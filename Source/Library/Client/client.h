#ifndef CLIENT_H
#define CLIENT_H

#include <QString>
#include "../Library/FracPos/fracpos.h"
#include "../Library/Constants/constants.h"

class Client{

    int clientId;
    QString username;
    QString nickname;
    uint backgroundColor;
    uint iconNumber;
    FracPos cursorPosition;

public:
    explicit Client(){
        clientId = -1;
        backgroundColor = 0;
        iconNumber = 0;
    }

    Client(int clientId, QString username, QString nickname, uint iconNumber, uint backgroundColor = 0) :
        clientId(clientId),
        username(username),
        nickname(nickname),
        backgroundColor(backgroundColor),
        iconNumber(iconNumber){}

    QString getNickname(){
        return nickname;
    }

    void setNickname(QString nickname){
        this->nickname = nickname;
    }

    uint getIconNumber(){
        return iconNumber;
    }

    void setIconNumber(uint iconNumber){
        this->iconNumber = iconNumber;
    }

    int getClientId(){
        return clientId;
    }

    void setClientId(int clientId){
        this->clientId = clientId;
    }

    QString getUsername(){
        return username;
    }

    void setUsername(QString username){
        this->username = username;
    }

    uint getBackgroundColor() {
        return backgroundColor;
    }

    void setBackgroundColor(uint backgroundColor) {
        this->backgroundColor = backgroundColor;
    }

    FracPos getCursorPosition() {
        return cursorPosition;
    }

    void setCursorPosition(FracPos position) {
        this->cursorPosition = position;
    }

    QString getHTMLImage(){
        QString img = "<img src=\"qrc:qml/images/cursor/";

        if(backgroundColor < COLORS.size()) {
            img.append(QString::number(backgroundColor)).append(".png\" />");
        } else {
            img.append("default.png\" />");
        }

        return img;
    }
};

#endif // CLIENT_H
